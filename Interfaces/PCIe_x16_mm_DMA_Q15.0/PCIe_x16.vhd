-- Altera Example design tested by Jubin and Shuiab on 24th June, 2015

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PCIe_test_top is
port(
A10_CLK_PCIE_P									: in std_logic_vector  ( 1 downto 0);
pcie_8ch_msb_pcie_npor_pin_perst				: in std_logic;  	-- default '1'
pcie_8ch_lsb_pcie_npor_pin_perst				: in std_logic;		-- default '1'
PLX_A10_PCIE_TX_P								: out std_logic_vector (15 downto 0);
PLX_A10_PCIE_RX_P								: in  std_logic_vector (15 downto 0);

--- TEST INPUTS
PCIe_LTSSM										: in std_logic_vector ( 4 downto 0);
PCIe_rate										: in std_logic_vector ( 1 downto 0);
PCIe_txdetectrx									: in std_logic;
PCIe_txswing									: in std_logic

);
end entity PCIe_test_top;

architecture top_connection of PCIe_test_top is

signal s_pcie_test_in :  std_logic_vector(31 downto 0) := x"00000000";

signal pcie_8ch_msb_npor : std_logic;
signal pcie_8ch_lsb_npor : std_logic;

	component ep_g3x8_avmm256 is
		port (
			pcie_a10_hip_0_hip_serial_rx_in0       : in  std_logic                     := 'X';             -- rx_in0
			pcie_a10_hip_0_hip_serial_rx_in1       : in  std_logic                     := 'X';             -- rx_in1
			pcie_a10_hip_0_hip_serial_rx_in2       : in  std_logic                     := 'X';             -- rx_in2
			pcie_a10_hip_0_hip_serial_rx_in3       : in  std_logic                     := 'X';             -- rx_in3
			pcie_a10_hip_0_hip_serial_rx_in4       : in  std_logic                     := 'X';             -- rx_in4
			pcie_a10_hip_0_hip_serial_rx_in5       : in  std_logic                     := 'X';             -- rx_in5
			pcie_a10_hip_0_hip_serial_rx_in6       : in  std_logic                     := 'X';             -- rx_in6
			pcie_a10_hip_0_hip_serial_rx_in7       : in  std_logic                     := 'X';             -- rx_in7
			pcie_a10_hip_0_hip_serial_tx_out0      : out std_logic;                                        -- tx_out0
			pcie_a10_hip_0_hip_serial_tx_out1      : out std_logic;                                        -- tx_out1
			pcie_a10_hip_0_hip_serial_tx_out2      : out std_logic;                                        -- tx_out2
			pcie_a10_hip_0_hip_serial_tx_out3      : out std_logic;                                        -- tx_out3
			pcie_a10_hip_0_hip_serial_tx_out4      : out std_logic;                                        -- tx_out4
			pcie_a10_hip_0_hip_serial_tx_out5      : out std_logic;                                        -- tx_out5
			pcie_a10_hip_0_hip_serial_tx_out6      : out std_logic;                                        -- tx_out6
			pcie_a10_hip_0_hip_serial_tx_out7      : out std_logic;                                        -- tx_out7
			pcie_a10_hip_0_hip_ctrl_test_in        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- test_in
			pcie_a10_hip_0_hip_ctrl_simu_mode_pipe : in  std_logic                     := 'X';             -- simu_mode_pipe
			pcie_a10_hip_0_npor_npor               : in  std_logic                     := 'X';             -- npor
			pcie_a10_hip_0_npor_pin_perst          : in  std_logic                     := 'X';             -- pin_perst
			refclk_clk                             : in  std_logic                     := 'X'              -- clk
		);
	end component ep_g3x8_avmm256;

begin

pcie_8ch_msb_npor <= pcie_8ch_msb_pcie_npor_pin_perst;--or application reset (Active LOW)
pcie_8ch_lsb_npor <= pcie_8ch_lsb_pcie_npor_pin_perst;--or application reset (Active LOW)

ep_g3x8_avmm256_lsb : component ep_g3x8_avmm256
		port map (
			pcie_a10_hip_0_hip_serial_rx_in0       => PLX_A10_PCIE_RX_P( 0),       -- pcie_a10_hip_0_hip_serial.rx_in0
			pcie_a10_hip_0_hip_serial_rx_in1       => PLX_A10_PCIE_RX_P( 1),       --                          .rx_in1
			pcie_a10_hip_0_hip_serial_rx_in2       => PLX_A10_PCIE_RX_P( 2),       --                          .rx_in2
			pcie_a10_hip_0_hip_serial_rx_in3       => PLX_A10_PCIE_RX_P( 3),       --                          .rx_in3
			pcie_a10_hip_0_hip_serial_rx_in4       => PLX_A10_PCIE_RX_P( 4),       --                          .rx_in4
			pcie_a10_hip_0_hip_serial_rx_in5       => PLX_A10_PCIE_RX_P( 5),       --                          .rx_in5
			pcie_a10_hip_0_hip_serial_rx_in6       => PLX_A10_PCIE_RX_P( 6),       --                          .rx_in6
			pcie_a10_hip_0_hip_serial_rx_in7       => PLX_A10_PCIE_RX_P( 7),       --                          .rx_in7
			pcie_a10_hip_0_hip_serial_tx_out0      => PLX_A10_PCIE_TX_P( 0),       --                          .tx_out0
			pcie_a10_hip_0_hip_serial_tx_out1      => PLX_A10_PCIE_TX_P( 1),       --                          .tx_out1
			pcie_a10_hip_0_hip_serial_tx_out2      => PLX_A10_PCIE_TX_P( 2),       --                          .tx_out2
			pcie_a10_hip_0_hip_serial_tx_out3      => PLX_A10_PCIE_TX_P( 3),       --                          .tx_out3
			pcie_a10_hip_0_hip_serial_tx_out4      => PLX_A10_PCIE_TX_P( 4),       --                          .tx_out4
			pcie_a10_hip_0_hip_serial_tx_out5      => PLX_A10_PCIE_TX_P( 5),       --                          .tx_out5
			pcie_a10_hip_0_hip_serial_tx_out6      => PLX_A10_PCIE_TX_P( 6),       --                          .tx_out6
			pcie_a10_hip_0_hip_serial_tx_out7      => PLX_A10_PCIE_TX_P( 7),       --                          .tx_out7
			pcie_a10_hip_0_hip_ctrl_test_in        => s_pcie_test_in,       		  --   pcie_a10_hip_0_hip_ctrl.test_in
			pcie_a10_hip_0_hip_ctrl_simu_mode_pipe => '0', --                          .simu_mode_pipe
			pcie_a10_hip_0_npor_npor               => pcie_8ch_lsb_npor,               --       pcie_a10_hip_0_npor.npor
			pcie_a10_hip_0_npor_pin_perst          => pcie_8ch_lsb_pcie_npor_pin_perst,          --                          .pin_perst
			refclk_clk                             => A10_CLK_PCIE_P(0)                              --                    refclk.clk
		);

ep_g3x8_avmm256_msb : component ep_g3x8_avmm256
		port map (
			pcie_a10_hip_0_hip_serial_rx_in0       => PLX_A10_PCIE_RX_P( 8),       -- pcie_a10_hip_0_hip_serial.rx_in0
			pcie_a10_hip_0_hip_serial_rx_in1       => PLX_A10_PCIE_RX_P( 9),       --                          .rx_in1
			pcie_a10_hip_0_hip_serial_rx_in2       => PLX_A10_PCIE_RX_P(10),       --                          .rx_in2
			pcie_a10_hip_0_hip_serial_rx_in3       => PLX_A10_PCIE_RX_P(11),       --                          .rx_in3
			pcie_a10_hip_0_hip_serial_rx_in4       => PLX_A10_PCIE_RX_P(12),       --                          .rx_in4
			pcie_a10_hip_0_hip_serial_rx_in5       => PLX_A10_PCIE_RX_P(13),       --                          .rx_in5
			pcie_a10_hip_0_hip_serial_rx_in6       => PLX_A10_PCIE_RX_P(14),       --                          .rx_in6
			pcie_a10_hip_0_hip_serial_rx_in7       => PLX_A10_PCIE_RX_P(15),       --                          .rx_in7
			pcie_a10_hip_0_hip_serial_tx_out0      => PLX_A10_PCIE_TX_P( 8),       --                          .tx_out0
			pcie_a10_hip_0_hip_serial_tx_out1      => PLX_A10_PCIE_TX_P( 9),       --                          .tx_out1
			pcie_a10_hip_0_hip_serial_tx_out2      => PLX_A10_PCIE_TX_P(10),       --                          .tx_out2
			pcie_a10_hip_0_hip_serial_tx_out3      => PLX_A10_PCIE_TX_P(11),       --                          .tx_out3
			pcie_a10_hip_0_hip_serial_tx_out4      => PLX_A10_PCIE_TX_P(12),       --                          .tx_out4
			pcie_a10_hip_0_hip_serial_tx_out5      => PLX_A10_PCIE_TX_P(13),       --                          .tx_out5
			pcie_a10_hip_0_hip_serial_tx_out6      => PLX_A10_PCIE_TX_P(14),       --                          .tx_out6
			pcie_a10_hip_0_hip_serial_tx_out7      => PLX_A10_PCIE_TX_P(15),       --                          .tx_out7
			pcie_a10_hip_0_hip_ctrl_test_in        => s_pcie_test_in,       		  --   pcie_a10_hip_0_hip_ctrl.test_in
			pcie_a10_hip_0_hip_ctrl_simu_mode_pipe => '0', --                          .simu_mode_pipe
			pcie_a10_hip_0_npor_npor               => pcie_8ch_msb_npor,               --       pcie_a10_hip_0_npor.npor
			pcie_a10_hip_0_npor_pin_perst          => pcie_8ch_msb_pcie_npor_pin_perst,          --                          .pin_perst
			refclk_clk                             => A10_CLK_PCIE_P(1)                              --                    refclk.clk
		);

end architecture top_connection;