--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros.marin@ieee.org)
--                        (Original design by P. Vichoudis (CERN) & M. Barros Marin)                          
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           Altera Arria 10 - MGT latency-optimized TX_WORDCLK monitoring
--                                                                                                 
-- Language:              VHDL'93                                                              
--                                                                                                   
-- Target Device:         Altera Arria 10                                                   
-- Tool version:          Quartus II 15.0                                                                  
--                                                                                                   
-- Version:               3.5                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--                                                                  
--                        06/04/2014   3.0       M. Barros Marin   First .vhd module definition.
--
--                        04/09/2014   3.5       M. Barros Marin   Processing stage process synchronous to TX_FRAMECLK & asynchronous output reset
--                        12/05/2015   3.5       J. Mitra          Modified for Arria 10
--
-- Additional Comments:  
--
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!                                                                                           !!
-- !! * The different parameters of the GBT Bank are set through:                               !!  
-- !!   (Note!! These parameters are vendor specific)                                           !!                    
-- !!                                                                                           !!
-- !!   - The MGT control ports of the GBT Bank module (these ports are listed in the records   !!
-- !!     of the file "<vendor>_<device>_gbt_bank_package.vhd").                                !! 
-- !!     (e.g. xlx_v6_gbt_bank_package.vhd)                                                    !!
-- !!                                                                                           !!  
-- !!   - By modifying the content of the file "<vendor>_<device>_gbt_bank_user_setup.vhd".     !!
-- !!     (e.g. xlx_v6_gbt_bank_user_setup.vhd)                                                 !! 
-- !!                                                                                           !! 
-- !! * The "<vendor>_<device>_gbt_bank_user_setup.vhd" is the only file of the GBT Bank that   !!
-- !!   may be modified by the user. The rest of the files MUST be used as is.                  !!
-- !!                                                                                           !!  
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--                                                                                                   
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.vendor_specific_gbt_bank_package.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity alt_a10_mgt_latopt_txwordclkmon is
   port ( 

      --=======--
      -- Reset --
      --=======--
      
      RESET_I                                   : in  std_logic; 
      
      --===============--
      -- Clocks scheme --
      --===============--   
      
      -- Sampled clock:
      -----------------
      
      SAMPLED_CLK_I                             : in  std_logic;      
      
      -- MGT_REFCLK:                     
      --------------            
      
      MGT_REFCLK_I                              : in  std_logic;
      
      -- TX_WORCLK:
      -------------
      
      TX_WORDCLK_I                              : in  std_logic;
      
      -- TX_FRAMECLK:
      --------------
      
      TX_FRAMECLK_I                             : in  std_logic;
      
      --=================--
      -- Monitor control --
      --=================--
      
      MGT_TX_READY_I                            : in std_logic;
      
      THRESHOLD_UP_I                            : in  std_logic_vector(7 downto 0);
      STATS_O                                   : out std_logic_vector(7 downto 0);
      THRESHOLD_LOW_I                           : in  std_logic_vector(7 downto 0);
      PHASE_OK_O                                : out std_logic;
      RESET_ITERATIONS_O                        : out std_logic_vector(15 downto 0);
      DONE_O                                    : out std_logic;
      
      --==============--
      -- MGT TX reset --
      --==============--

      MGT_RESET_EN_I                            : in  std_logic;
      MGT_RESET_O                               : out std_logic
      
   );
end alt_a10_mgt_latopt_txwordclkmon;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of alt_a10_mgt_latopt_txwordclkmon is
   
   --================================ Signal Declarations ================================--
   
   --==================--
   -- Comparison stage --
   --==================--
   
   signal sampledClk_from_mgtRefClkReg          : std_logic;
   signal sampledClk_from_txWordClkReg          : std_logic;
   
   signal test_from_xorGate                     : std_logic;
   
   --==================--
   -- Processing stage --
   --==================--
   
   signal test_from_xorGate_r                   : std_logic;
   signal statsTimer                            : unsigned(15 downto 0);
   signal done_from_xorGateStats                : std_logic;
   signal monitoringStats                       : unsigned(15 downto 0);  
   
   signal txReady_r2                            : std_logic;
   signal txReady_r                             : std_logic;
   signal donefromXorGateStats_r2               : std_logic;
   signal donefromXorGateStats_r                : std_logic;
   signal mgtResetEn_r2                         : std_logic;
   signal mgtResetEn_r                          : std_logic;
   
   signal state                                 : txWordClkMonitoringFsmLatOpt_T;
   signal timer                                 : unsigned(15 downto 0);
   signal statsReset_from_processingStage       : std_logic; 
   signal resetIterations                       : unsigned(15 downto 0); 
   
   signal reset_from_processingStage            : std_logic;
   signal mgtReset_r                            : std_logic;
   
   --=====================================================================================--   
   
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic =====================================--

   --==================--
   -- Comparison stage --
   --==================--
   
   -- MGT_REFCLK register:
   -----------------------
   
   mgtRefClkReg: process(RESET_I, MGT_REFCLK_I)
   begin
      if RESET_I = '1' then
         sampledClk_from_mgtRefClkReg           <= '0';
      elsif rising_edge(MGT_REFCLK_I) then
         sampledClk_from_mgtRefClkReg           <= SAMPLED_CLK_I;
      end if;
   end process;

   -- TX_WORCLK register:
   ----------------------
   
   txWordClkReg: process(RESET_I, TX_WORDCLK_I)
   begin
      if RESET_I = '1' then
         sampledClk_from_txWordClkReg           <= '0';
      elsif rising_edge(TX_WORDCLK_I) then
         sampledClk_from_txWordClkReg           <= SAMPLED_CLK_I;
      end if;
   end process;  
   
   -- XOR gate:
   ------------
   
   test_from_xorGate                            <= sampledClk_from_mgtRefClkReg xor sampledClk_from_txWordClkReg;

   --==================--
   -- Processing stage --
   --==================--
   
   xorGateStats: process(statsReset_from_processingStage, MGT_REFCLK_I)
   begin
      if statsReset_from_processingStage = '1' then
         test_from_xorGate_r                    <= '0';
         statsTimer                             <= (others => '0');
         done_from_xorGateStats                 <= '0';
         monitoringStats                        <= (others => '0');
         STATS_O                                <= (others => '0');
      elsif rising_edge(MGT_REFCLK_I) then
         
         -- Registered output:
         ---------------------
         
         test_from_xorGate_r                    <= test_from_xorGate;         
         
         -- Counter:
         -----------
        
         if statsTimer = STATS_TIMEOUT-1 then
            done_from_xorGateStats              <= '1';
         else
            if test_from_xorGate_r = '1' then
               monitoringStats                  <= monitoringStats + 1;             
            end if;
            statsTimer                          <= statsTimer + 1;   
         end if;
         
         STATS_O                                <= std_logic_vector(monitoringStats(15 downto 8));
         
      end if;
   end process;
   
   processingStage: process(RESET_I, TX_FRAMECLK_I)
   begin
      if RESET_I = '1' then        
         txReady_r2                             <= '0';
         txReady_r                              <= '0';        
         donefromXorGateStats_r2                <= '0';
         donefromXorGateStats_r                 <= '0';        
         mgtResetEn_r2                          <= '0';
         mgtResetEn_r                           <= '0';
         ---------------------------------------         
         state                                  <= s0_idle;
         timer                                  <= (others => '0');                  
         statsReset_from_processingStage        <= '1';
         resetIterations                        <= (others => '0');         
         RESET_ITERATIONS_O                     <= (others => '0');
         reset_from_processingStage             <= '1';
         PHASE_OK_O                             <= '0';
         DONE_O                                 <= '0';
      elsif rising_edge(TX_FRAMECLK_I) then      
         
         -- Synchronizers:
         -----------------
         
         txReady_r2                             <= txReady_r;            
         txReady_r                              <= MGT_TX_READY_I;
         
         donefromXorGateStats_r2                <= donefromXorGateStats_r;
         donefromXorGateStats_r                 <= done_from_xorGateStats;
         
         mgtResetEn_r2                          <= mgtResetEn_r;
         mgtResetEn_r                           <= MGT_RESET_EN_I;
         
         -- Finite Sate Machine (FSM):
         -----------------------------
         
         case state is
            when s0_idle =>
                reset_from_processingStage      <= '1';
                statsReset_from_processingStage <= '1';
                if timer = MGT_RESET_DLY-1 then
                  reset_from_processingStage    <= '0';
                  state                         <= s1_txMgtReady;
                  timer                         <= (others => '0');
                else
                  timer                         <= timer + 1;
                end if;
            when s1_txMgtReady =>    
               if txReady_r2 = '1' then
                  state                         <= s2_dly;
               end if;
            when s2_dly =>   
               if timer = TXWORDCLK_INITIAL_DLY-1 then
                  state                         <= s3_stats;
                  timer                         <= (others => '0');
               else                  
                  timer                         <= timer + 1;
               end if;
            when s3_stats =>         
               statsReset_from_processingStage  <= '0';
               if donefromXorGateStats_r2 = '1' then
                  state                         <= s4_waitRstEn;
               end if;
            when s4_waitRstEn =>      
               if mgtResetEn_r2 = '1' then 
                  DONE_O                        <= '0';    
                  state                         <= s5_thresholds;
               else
                  DONE_O                        <= '1';                     
               end if;               
            when s5_thresholds =>
               if     (monitoringStats(15 downto 8) < unsigned(THRESHOLD_UP_I))            
                  and (monitoringStats(15 downto 8) > unsigned(THRESHOLD_LOW_I))
               then
                  state                         <= s7_phaseOk;
               else
                  state                         <= s6_resetMgtTx;
               end if;               
            when s6_resetMgtTx =>
               state                            <= s0_idle;
               resetIterations                  <= resetIterations + 1;
            when s7_phaseOk => 
               PHASE_OK_O                       <= '1';
               DONE_O                           <= '1';               
         end case;
         
         RESET_ITERATIONS_O                     <= std_logic_vector(resetIterations);
         
      end if;
   end process;
   
   -- Comment: Although the reset chain of the LATOPT GBT Bank must be synchronous with TX_FRAMECLK so as to achieve fixed and deterministic latency,
   --          the first reset must be asynchronous in order to have 0deg or 180deg in the phase of TX_WORDCLK after power up (if the reset was fully
   --          synchronous with TX_FRAMECLK, after power up the value of the phase may be 0deg or 180deg but it would not change if resetting the GBT Bank.
   
   sampleClkSync: process(RESET_I, SAMPLED_CLK_I)
   begin
      if RESET_I = '1' then
         MGT_RESET_O                            <= '1';
         mgtReset_r                             <= '1';
      elsif rising_edge(SAMPLED_CLK_I) then
         MGT_RESET_O                            <= mgtReset_r;
         mgtReset_r                             <= reset_from_processingStage;
      end if;
   end process;
   
   --=====================================================================================--   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--