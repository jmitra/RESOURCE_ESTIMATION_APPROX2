--=================================================================================================--
--##################################   Package Information   ######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros.marin@ieee.org)
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           Altera Arria 10 - Multi Gigabit Transceivers latency-optimized
--                                                                                                 
-- Language:              VHDL'93                                                                 
--                                                                                                   
-- Target Device:         Altera Arria 10                                                      
-- Tool version:          Quartus II 15.0                                                              
--                                                                                                   
-- Revision:              3.6                                                                      
--
-- Description:           
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        27/03/2014   3.0       M. Barros Marin   First .vhd module definition           
--
--                        05/10/2014   3.5       M. Barros Marin   - Minor modification
--                                                                 - Updated to Quartus II 14.0           
--
--                        09/02/2015   3.6       M. Barros Marin   - Modified Tx_WORDCLK monitor
--                                                                 - Modified Rx bitslip control
--                                                                 - Minor modification
--                        12/05/2015   3.6       J. Mitra          - Modified for Arria 10
--
--
-- Additional Comments:                                                                               
--
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!                                                                                           !!
-- !! * The different parameters of the GBT Bank are set through:                               !!  
-- !!   (Note!! These parameters are vendor specific)                                           !!                    
-- !!                                                                                           !!
-- !!   - The MGT control ports of the GBT Bank module (these ports are listed in the records   !!
-- !!     of the file "<vendor>_<device>_gbt_bank_package.vhd").                                !! 
-- !!     (e.g. xlx_v6_gbt_bank_package.vhd)                                                    !!
-- !!                                                                                           !!  
-- !!   - By modifying the content of the file "<vendor>_<device>_gbt_bank_user_setup.vhd".     !!
-- !!     (e.g. xlx_v6_gbt_bank_user_setup.vhd)                                                 !! 
-- !!                                                                                           !! 
-- !! * The "<vendor>_<device>_gbt_bank_user_setup.vhd" is the only file of the GBT Bank that   !!
-- !!   may be modified by the user. The rest of the files MUST be used as is.                  !!
-- !!                                                                                           !!  
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--                                                                                                   
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Altera devices library:
library altera; 
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;   
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;



--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity mgt_latopt is
   generic (
      GBT_BANK_ID                                  : integer := 1
   ); 
   port (      

      --===============--  
      -- Clocks scheme --  
      --===============--  

      MGT_CLKS_I                                   : in  gbtBankMgtClks_i_R;
      MGT_CLKS_O                                   : out gbtBankMgtClks_o_R;        

      --=========--  
      -- MGT I/O --  
      --=========--  

      MGT_I                                        : in  mgt_i_R;
      MGT_O                                        : out mgt_o_R;

      --=============-- 
      -- GBT Control -- 
      --=============-- 

      GBTTX_MGTTX_RDY_O                            : out std_logic_vector       (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
      
      GBTRX_MGTRX_RDY_O                            : out std_logic_vector       (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
      GBTRX_RXWORDCLK_READY_O                      : out std_logic_vector       (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
      GBTRX_HEADER_LOCKED_I                        : in  std_logic_vector       (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
      GBTRX_BITSLIP_NBR_I                          : in  rxBitSlipNbr_mxnbit_A  (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);      
            
      --=======-- 
      -- Words -- 
      --=======-- 
 
      GBTTX_WORD_I                                 : in  word_mxnbit_A          (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);     
      GBTRX_WORD_O                                 : out word_mxnbit_A          (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS) 
   
   );
end mgt_latopt;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of mgt_latopt is 

   --================================ Component Declarations ================================--

	component alt_a10_gx_latopt_x1 is
		port (
			reconfig_write          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- write
			reconfig_read           : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- read
			reconfig_address        : in  std_logic_vector(9 downto 0)  := (others => 'X'); -- address
			reconfig_writedata      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			reconfig_readdata       : out std_logic_vector(31 downto 0);                    -- readdata
			reconfig_waitrequest    : out std_logic_vector(0 downto 0);                     -- waitrequest
			reconfig_clk            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
			reconfig_reset          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- reset
			rx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_analogreset
			rx_cal_busy             : out std_logic_vector(0 downto 0);                     -- rx_cal_busy
			rx_cdr_refclk0          : in  std_logic                     := 'X';             -- clk
			rx_clkout               : out std_logic_vector(0 downto 0);                     -- clk
			rx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
			rx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_digitalreset
			rx_is_lockedtodata      : out std_logic_vector(0 downto 0);                     -- rx_is_lockedtodata
			rx_is_lockedtoref       : out std_logic_vector(0 downto 0);                     -- rx_is_lockedtoref
			rx_parallel_data        : out std_logic_vector(39 downto 0);                    -- rx_parallel_data
			rx_pma_clkslip          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_pma_clkslip
			rx_polinv               : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_polinv
			rx_serial_data          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_serial_data
			rx_seriallpbken         : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_seriallpbken
			tx_analogreset          : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- tx_analogreset
			tx_bonding_clocks       : in  std_logic_vector(5 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy             : out std_logic_vector(0 downto 0);                     -- tx_cal_busy
			tx_clkout               : out std_logic_vector(0 downto 0);                     -- clk
			tx_coreclkin            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- clk
			tx_digitalreset         : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- tx_digitalreset
			tx_parallel_data        : in  std_logic_vector(39 downto 0) := (others => 'X'); -- tx_parallel_data
			tx_polinv               : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- tx_polinv
			tx_serial_data          : out std_logic_vector(0 downto 0);                     -- tx_serial_data
			unused_rx_parallel_data : out std_logic_vector(87 downto 0);                    -- unused_rx_parallel_data
			unused_tx_parallel_data : in  std_logic_vector(87 downto 0) := (others => 'X')  -- unused_tx_parallel_data
		);
	end component alt_a10_gx_latopt_x1;

	component alt_a10_gx_latopt_x2 is
		port (
			reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- write
			reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- read
			reconfig_address        : in  std_logic_vector(10 downto 0)  := (others => 'X'); -- address
			reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => 'X'); -- writedata
			reconfig_readdata       : out std_logic_vector(31 downto 0);                     -- readdata
			reconfig_waitrequest    : out std_logic_vector(0 downto 0);                      -- waitrequest
			reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- clk
			reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- reset
			rx_analogreset          : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- rx_analogreset
			rx_cal_busy             : out std_logic_vector(1 downto 0);                      -- rx_cal_busy
			rx_cdr_refclk0          : in  std_logic                      := 'X';             -- clk
			rx_clkout               : out std_logic_vector(1 downto 0);                      -- clk
			rx_coreclkin            : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- clk
			rx_digitalreset         : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- rx_digitalreset
			rx_is_lockedtodata      : out std_logic_vector(1 downto 0);                      -- rx_is_lockedtodata
			rx_is_lockedtoref       : out std_logic_vector(1 downto 0);                      -- rx_is_lockedtoref
			rx_parallel_data        : out std_logic_vector(79 downto 0);                     -- rx_parallel_data
			rx_pma_clkslip          : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- rx_pma_clkslip
			rx_polinv               : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- rx_polinv
			rx_serial_data          : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- rx_serial_data
			rx_seriallpbken         : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- rx_seriallpbken
			tx_analogreset          : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- tx_analogreset
			tx_bonding_clocks       : in  std_logic_vector(11 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy             : out std_logic_vector(1 downto 0);                      -- tx_cal_busy
			tx_clkout               : out std_logic_vector(1 downto 0);                      -- clk
			tx_coreclkin            : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- clk
			tx_digitalreset         : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- tx_digitalreset
			tx_parallel_data        : in  std_logic_vector(79 downto 0)  := (others => 'X'); -- tx_parallel_data
			tx_polinv               : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- tx_polinv
			tx_serial_data          : out std_logic_vector(1 downto 0);                      -- tx_serial_data
			unused_rx_parallel_data : out std_logic_vector(175 downto 0);                    -- unused_rx_parallel_data
			unused_tx_parallel_data : in  std_logic_vector(175 downto 0) := (others => 'X')  -- unused_tx_parallel_data
		);
	end component alt_a10_gx_latopt_x2;

	component alt_a10_gx_latopt_x3 is
		port (
			reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- write
			reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- read
			reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => 'X'); -- address
			reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => 'X'); -- writedata
			reconfig_readdata       : out std_logic_vector(31 downto 0);                     -- readdata
			reconfig_waitrequest    : out std_logic_vector(0 downto 0);                      -- waitrequest
			reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- clk
			reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- reset
			rx_analogreset          : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- rx_analogreset
			rx_cal_busy             : out std_logic_vector(2 downto 0);                      -- rx_cal_busy
			rx_cdr_refclk0          : in  std_logic                      := 'X';             -- clk
			rx_clkout               : out std_logic_vector(2 downto 0);                      -- clk
			rx_coreclkin            : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- clk
			rx_digitalreset         : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- rx_digitalreset
			rx_is_lockedtodata      : out std_logic_vector(2 downto 0);                      -- rx_is_lockedtodata
			rx_is_lockedtoref       : out std_logic_vector(2 downto 0);                      -- rx_is_lockedtoref
			rx_parallel_data        : out std_logic_vector(119 downto 0);                    -- rx_parallel_data
			rx_pma_clkslip          : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- rx_pma_clkslip
			rx_polinv               : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- rx_polinv
			rx_serial_data          : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- rx_serial_data
			rx_seriallpbken         : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- rx_seriallpbken
			tx_analogreset          : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- tx_analogreset
			tx_bonding_clocks       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy             : out std_logic_vector(2 downto 0);                      -- tx_cal_busy
			tx_clkout               : out std_logic_vector(2 downto 0);                      -- clk
			tx_coreclkin            : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- clk
			tx_digitalreset         : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- tx_digitalreset
			tx_parallel_data        : in  std_logic_vector(119 downto 0) := (others => 'X'); -- tx_parallel_data
			tx_polinv               : in  std_logic_vector(2 downto 0)   := (others => 'X'); -- tx_polinv
			tx_serial_data          : out std_logic_vector(2 downto 0);                      -- tx_serial_data
			unused_rx_parallel_data : out std_logic_vector(263 downto 0);                    -- unused_rx_parallel_data
			unused_tx_parallel_data : in  std_logic_vector(263 downto 0) := (others => 'X')  -- unused_tx_parallel_data
		);
	end component alt_a10_gx_latopt_x3;

	component alt_a10_gx_latopt_x4 is
		port (
			reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- write
			reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- read
			reconfig_address        : in  std_logic_vector(11 downto 0)  := (others => 'X'); -- address
			reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => 'X'); -- writedata
			reconfig_readdata       : out std_logic_vector(31 downto 0);                     -- readdata
			reconfig_waitrequest    : out std_logic_vector(0 downto 0);                      -- waitrequest
			reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- clk
			reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- reset
			rx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- rx_analogreset
			rx_cal_busy             : out std_logic_vector(3 downto 0);                      -- rx_cal_busy
			rx_cdr_refclk0          : in  std_logic                      := 'X';             -- clk
			rx_clkout               : out std_logic_vector(3 downto 0);                      -- clk
			rx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- clk
			rx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- rx_digitalreset
			rx_is_lockedtodata      : out std_logic_vector(3 downto 0);                      -- rx_is_lockedtodata
			rx_is_lockedtoref       : out std_logic_vector(3 downto 0);                      -- rx_is_lockedtoref
			rx_parallel_data        : out std_logic_vector(159 downto 0);                    -- rx_parallel_data
			rx_pma_clkslip          : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- rx_pma_clkslip
			rx_polinv               : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- rx_polinv
			rx_serial_data          : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- rx_serial_data
			rx_seriallpbken         : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- rx_seriallpbken
			tx_analogreset          : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- tx_analogreset
			tx_bonding_clocks       : in  std_logic_vector(23 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy             : out std_logic_vector(3 downto 0);                      -- tx_cal_busy
			tx_clkout               : out std_logic_vector(3 downto 0);                      -- clk
			tx_coreclkin            : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- clk
			tx_digitalreset         : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- tx_digitalreset
			tx_parallel_data        : in  std_logic_vector(159 downto 0) := (others => 'X'); -- tx_parallel_data
			tx_polinv               : in  std_logic_vector(3 downto 0)   := (others => 'X'); -- tx_polinv
			tx_serial_data          : out std_logic_vector(3 downto 0);                      -- tx_serial_data
			unused_rx_parallel_data : out std_logic_vector(351 downto 0);                    -- unused_rx_parallel_data
			unused_tx_parallel_data : in  std_logic_vector(351 downto 0) := (others => 'X')  -- unused_tx_parallel_data
		);
	end component alt_a10_gx_latopt_x4;

	component alt_a10_gx_latopt_x5 is
		port (
			reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- write
			reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- read
			reconfig_address        : in  std_logic_vector(12 downto 0)  := (others => 'X'); -- address
			reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => 'X'); -- writedata
			reconfig_readdata       : out std_logic_vector(31 downto 0);                     -- readdata
			reconfig_waitrequest    : out std_logic_vector(0 downto 0);                      -- waitrequest
			reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- clk
			reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- reset
			rx_analogreset          : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- rx_analogreset
			rx_cal_busy             : out std_logic_vector(4 downto 0);                      -- rx_cal_busy
			rx_cdr_refclk0          : in  std_logic                      := 'X';             -- clk
			rx_clkout               : out std_logic_vector(4 downto 0);                      -- clk
			rx_coreclkin            : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- clk
			rx_digitalreset         : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- rx_digitalreset
			rx_is_lockedtodata      : out std_logic_vector(4 downto 0);                      -- rx_is_lockedtodata
			rx_is_lockedtoref       : out std_logic_vector(4 downto 0);                      -- rx_is_lockedtoref
			rx_parallel_data        : out std_logic_vector(199 downto 0);                    -- rx_parallel_data
			rx_pma_clkslip          : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- rx_pma_clkslip
			rx_polinv               : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- rx_polinv
			rx_serial_data          : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- rx_serial_data
			rx_seriallpbken         : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- rx_seriallpbken
			tx_analogreset          : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- tx_analogreset
			tx_bonding_clocks       : in  std_logic_vector(29 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy             : out std_logic_vector(4 downto 0);                      -- tx_cal_busy
			tx_clkout               : out std_logic_vector(4 downto 0);                      -- clk
			tx_coreclkin            : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- clk
			tx_digitalreset         : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- tx_digitalreset
			tx_parallel_data        : in  std_logic_vector(199 downto 0) := (others => 'X'); -- tx_parallel_data
			tx_polinv               : in  std_logic_vector(4 downto 0)   := (others => 'X'); -- tx_polinv
			tx_serial_data          : out std_logic_vector(4 downto 0);                      -- tx_serial_data
			unused_rx_parallel_data : out std_logic_vector(439 downto 0);                    -- unused_rx_parallel_data
			unused_tx_parallel_data : in  std_logic_vector(439 downto 0) := (others => 'X')  -- unused_tx_parallel_data
		);
	end component alt_a10_gx_latopt_x5;

	component alt_a10_gx_latopt_x6 is
		port (
			reconfig_write          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- write
			reconfig_read           : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- read
			reconfig_address        : in  std_logic_vector(12 downto 0)  := (others => 'X'); -- address
			reconfig_writedata      : in  std_logic_vector(31 downto 0)  := (others => 'X'); -- writedata
			reconfig_readdata       : out std_logic_vector(31 downto 0);                     -- readdata
			reconfig_waitrequest    : out std_logic_vector(0 downto 0);                      -- waitrequest
			reconfig_clk            : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- clk
			reconfig_reset          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- reset
			rx_analogreset          : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- rx_analogreset
			rx_cal_busy             : out std_logic_vector(5 downto 0);                      -- rx_cal_busy
			rx_cdr_refclk0          : in  std_logic                      := 'X';             -- clk
			rx_clkout               : out std_logic_vector(5 downto 0);                      -- clk
			rx_coreclkin            : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- clk
			rx_digitalreset         : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- rx_digitalreset
			rx_is_lockedtodata      : out std_logic_vector(5 downto 0);                      -- rx_is_lockedtodata
			rx_is_lockedtoref       : out std_logic_vector(5 downto 0);                      -- rx_is_lockedtoref
			rx_parallel_data        : out std_logic_vector(239 downto 0);                    -- rx_parallel_data
			rx_pma_clkslip          : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- rx_pma_clkslip
			rx_polinv               : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- rx_polinv
			rx_serial_data          : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- rx_serial_data
			rx_seriallpbken         : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- rx_seriallpbken
			tx_analogreset          : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- tx_analogreset
			tx_bonding_clocks       : in  std_logic_vector(35 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy             : out std_logic_vector(5 downto 0);                      -- tx_cal_busy
			tx_clkout               : out std_logic_vector(5 downto 0);                      -- clk
			tx_coreclkin            : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- clk
			tx_digitalreset         : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- tx_digitalreset
			tx_parallel_data        : in  std_logic_vector(239 downto 0) := (others => 'X'); -- tx_parallel_data
			tx_polinv               : in  std_logic_vector(5 downto 0)   := (others => 'X'); -- tx_polinv
			tx_serial_data          : out std_logic_vector(5 downto 0);                      -- tx_serial_data
			unused_rx_parallel_data : out std_logic_vector(527 downto 0);                    -- unused_rx_parallel_data
			unused_tx_parallel_data : in  std_logic_vector(527 downto 0) := (others => 'X')  -- unused_tx_parallel_data
		);
	end component alt_a10_gx_latopt_x6;


   --================================ Signal Declarations ===================================--
   
   --======================--
   -- GX reset controllers --
   --======================--  
   
   signal txAnalogReset_from_gxRstCtrl             : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal txDigitalReset_from_gxRstCtrl            : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal txReady_from_gxRstCtrl                   : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   ------------------------------------------------   
   signal rxAnalogreset_from_gxRstCtrl             : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal rxDigitalreset_from_gxRstCtrl            : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal rxReady_from_gxRstCtrl                   : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);

   --=======================--
   -- TX_WORDCLK monitoring --
   --=======================--
   
   signal done_from_txWordClkMon                   : std_logic;

   --====================--
   -- RX phase alignment --
   --====================--
   
   signal nbr_to_rxBitSlipControl                  : rxBitSlipNbr_mxnbit_A      (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal run_to_rxBitSlipControl                  : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal rxBitSlip_from_rxBitSlipControl          : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal rxBitSlip_to_gxLatOpt                    : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);   
   signal resetGxRx_from_rxBitSlipControl          : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);   
   signal done_from_rxBitSlipControl               : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);  
   
   --================================================--
   -- Multi-Gigabit Transceivers (latency-optimized) --
   --================================================--      

   signal rxIsLockedToData_from_gxLatOpt           : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);   
   signal txCalBusy_from_gxLatOpt                  : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS);
   signal rxCalBusy_from_gxLatOpt                  : std_logic_vector           (1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS); 
   
   --=====================================================================================--   
   signal unused_tx_parallel_data_int              : std_logic_vector     (527 downto 0):=(others=>'0');
   signal unused_rx_parallel_data_int              : std_logic_vector     (527 downto 0);
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================-- 
   --=============--
   -- Assignments --
   --=============--
   
   commonAssign_gen: for i in 1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS generate
   
      MGT_O.mgtLink(i).txCal_busy                  <= txCalBusy_from_gxLatOpt(i);
      MGT_O.mgtLink(i).rxCal_busy                  <= rxCalBusy_from_gxLatOpt(i);
      MGT_O.mgtLink(i).ready                       <= txReady_from_gxRstCtrl(i) and rxReady_from_gxRstCtrl(i);
      MGT_O.mgtLink(i).tx_ready                    <= txReady_from_gxRstCtrl(i);
      MGT_O.mgtLink(i).rx_ready                    <= rxReady_from_gxRstCtrl(i);
      GBTTX_MGTTX_RDY_O(i)                         <= txReady_from_gxRstCtrl(i);         
      GBTRX_MGTRX_RDY_O(i)                         <= rxReady_from_gxRstCtrl(i);                 
      MGT_O.mgtLink(i).rxIsLocked_toData           <= rxIsLockedToData_from_gxLatOpt(i);
   
   end generate;
   
   --======================--
   -- GX reset controllers --
   --======================--
   
   gxRstCtrl_gen: for i in 1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS generate
   
      gxRstCtrl: entity work.alt_a10_mgt_resetctrl       
         port map (
            CLK_I                                  => MGT_CLKS_I.tx_frameClk, 
            ---------------------------------------            
            TX_RESET_I                             => MGT_I.mgtLink(i).tx_reset,    
            --RX_RESET_I                             => MGT_I.mgtLink(i).rx_reset or resetGxRx_from_rxBitSlipControl(i),    
            RX_RESET_I                             => MGT_I.mgtLink(i).rx_reset , -- Modified by Jubin    
            ---------------------------------------             
            TX_ANALOGRESET_O                       => txAnalogReset_from_gxRstCtrl(i),
            TX_DIGITALRESET_O                      => txDigitalReset_from_gxRstCtrl(i),                  
            TX_READY_O                             => txReady_from_gxRstCtrl(i),                         
            PLL_LOCKED_I                           => MGT_I.mgtCommon.extGxTxPll_locked,                       
            TX_CAL_BUSY_I                          => txCalBusy_from_gxLatOpt(i),                          
            ---------------------------------------             
            RX_ANALOGRESET_O                       => rxAnalogreset_from_gxRstCtrl(i),
            RX_DIGITALRESET_O                      => rxDigitalreset_from_gxRstCtrl(i),                          
            RX_READY_O                             => rxReady_from_gxRstCtrl(i),                         
            RX_IS_LOCKEDTODATA_I                   => rxIsLockedToData_from_gxLatOpt(i),                     
            RX_CAL_BUSY_I                          => rxCalBusy_from_gxLatOpt(i)                                 
         );
   
   end generate;

   --=======================--
   -- TX_WORDCLK monitoring --
   --=======================--
   --Turned off by Jubin as there were no problem of tx latopt in arria10
   -- txWordClkMon_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).TX_OPTIMIZATION = LATENCY_OPTIMIZED generate
      
      -- -- Comment: * The phase of TX_WORDCLK may be 0deg or 180deg with respect to the phase of the MGT_REFCLK.
      
               -- -- * The module "txWordClkMon" monitors the phase of TX_WORDCLK and if it is not the one chosen by 
                 -- -- the user, resets the GX TX until having the correct phase.
            
      -- txWordClkMon: entity work.alt_a10_mgt_latopt_txwordclkmon
         -- port map (
            -- RESET_I                                => MGT_I.mgtCommon.txWrdClkMon_reset,
            -------------------------------------                     
            -- SAMPLED_CLK_I                          => MGT_CLKS_I.txWrdClkMon_sampledClk,
            -- MGT_REFCLK_I                           => MGT_CLKS_I.txWrdClkMon_mgtTxRefClk,           
            -- TX_WORDCLK_I                           => MGT_CLKS_I.tx_wordClk(1),
            -- TX_FRAMECLK_I                          => MGT_CLKS_I.tx_frameClk,
            -------------------------------------         
            -- MGT_TX_READY_I                         => txReady_from_gxRstCtrl(1),
            -- THRESHOLD_UP_I                         => MGT_I.mgtCommon.txWrdClkMon_thresholdUp,
            -- STATS_O                                => MGT_O.mgtCommon.txWrdClkMon_stats,
            -- THRESHOLD_LOW_I                        => MGT_I.mgtCommon.txWrdClkMon_thresholdLow,           
            -- PHASE_OK_O                             => MGT_O.mgtCommon.txWrdClkMon_phaseOk,
            -- RESET_ITERATIONS_O                     => MGT_O.mgtCommon.txWrdClkMon_rstIteration,
            -- DONE_O                                 => done_from_txWordClkMon,
            -------------------------------------         
            -- MGT_RESET_EN_I                         => MGT_I.mgtCommon.txWrdClkMon_gbtLinkRstEn,
            -- MGT_RESET_O                            => MGT_O.mgtCommon.txWrdClkMon_gbtLinkReset
         -- );
      
   -- end generate;

   -- txWordClkMon_no_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).TX_OPTIMIZATION = STANDARD generate

      MGT_O.mgtCommon.txWrdClkMon_stats            <= (others => '0');
      MGT_O.mgtCommon.txWrdClkMon_phaseOk          <= '0';
      
      done_from_txWordClkMon                       <= '1';
      MGT_O.mgtCommon.txWrdClkMon_rstIteration     <= (others => '0');
      ---------------------------------------------
      MGT_O.mgtCommon.txWrdClkMon_gbtLinkReset     <= '0';
      
   -- end generate;
      
   --====================--
   -- RX phase alignment --
   --====================--
   
   -- Comment: Note!! The standard version of the GX does not align the phase of the  
   --                 RX_RECCLK (RX_WORDCLK) with respect to the TX_OUTCLK (TX_WORDCLK).
   -- Comment by Jubin for Rx latency to  use standard component only as no error seen in simulation
   rxPhaseAlign_numLinks_gen: for i in 1 to GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS generate  

      rxPhaseAlign_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).RX_OPTIMIZATION = LATENCY_OPTIMIZED generate
      
         -- Bitslip control module:
         ----------------------
         
         rxBitSlipControl: entity work.mgt_latopt_bitslipctrl 								--commented by Jubin
            port map (
               RX_RESET_I                          => MGT_I.mgtLink(i).rx_reset,
               RX_WORDCLK_I                        => MGT_CLKS_I.rx_wordClk(i),
               NUMBITSLIPS_I                       => nbr_to_rxBitSlipControl(i),
               ENABLE_I                            => run_to_rxBitSlipControl(i),
               MGT_RX_ODD_RESET_EN_I               => MGT_I.mgtLink(i).rxBitSlip_oddRstEn,
               BITSLIP_O                           => rxBitSlip_from_rxBitSlipControl(i),
               RESET_MGT_RX_O                      => resetGxRx_from_rxBitSlipControl(i),
               RESET_MGT_RX_ITERATIONS_O           => MGT_O.mgtLink(i).rxBitSlip_oddRstNbr,
               DONE_O                              => done_from_rxBitSlipControl(i)
            );
         
         MGT_O.mgtLink(i).rxWordClkReady           <= done_from_rxBitSlipControl(i);
         MGT_O.mgtLink(i).rxWordClkReady           <= done_from_rxBitSlipControl(i);

         -- Manual or auto bitslip control selection logic:
         ----------------------------------------------
         
         -- Comment: * MGT_I(i).rxBitSlip_enable must be '1' to enable the GX RX phase alignment.
         
                  -- * Manual control: MGT_I(i).rxBitSlip_ctrl = '1'
                    -- Auto control  : MGT_I(i).rxBitSlip_ctrl = '0'
         
                  -- * In manual control, the user provides the number of bitslips (rxBitSlip_nbr)
                    -- as well as triggers the GX RX phase alignment (rxBitSlip_run).
         
         rxBitSlip_to_gxLatOpt(i)     <= rxBitSlip_from_rxBitSlipControl(i) when     MGT_I.mgtLink(i).rxBitSlip_enable = '1'  
                                       -----------------------------------------------------------------------
                                      else '0'; 
         run_to_rxBitSlipControl(i) <= MGT_I.mgtLink(i).rxBitSlip_run       when     MGT_I.mgtLink(i).rxBitSlip_enable = '1' 
                                                                                 and MGT_I.mgtLink(i).rxBitSlip_ctrl   = '1'
                                       ---------------------------------------------------------------------
                                       else GBTRX_HEADER_LOCKED_I(i)        when     MGT_I.mgtLink(i).rxBitSlip_enable = '1'
                                                                                 and MGT_I.mgtLink(i).rxBitSlip_ctrl   = '0'
                                       ---------------------------------------------------------------------
                                       else '0';
                           
         nbr_to_rxBitSlipControl(i) <= MGT_I.mgtLink(i).rxBitSlip_nbr       when     MGT_I.mgtLink(i).rxBitSlip_enable = '1'
                                                                                 and MGT_I.mgtLink(i).rxBitSlip_ctrl   = '1'
                                        ---------------------------------------------------------------------                                
                                        else GBTRX_BITSLIP_NBR_I(i)         when     MGT_I.mgtLink(i).rxBitSlip_enable = '1'
                                                                                 and MGT_I.mgtLink(i).rxBitSlip_ctrl   = '0'
                                        -----------------------------------------------------------------------                                
                                        else (others => '0');   
      
      end generate;
      
      rxPhaseAlign_no_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).RX_OPTIMIZATION = STANDARD generate
      
         -- Bitslip control module:
         --------------------------
        
         MGT_O.mgtLink(i).rxWordClkReady           <= rxReady_from_gxRstCtrl(i);
         GBTRX_RXWORDCLK_READY_O(i)                <= rxReady_from_gxRstCtrl(i);
         
         -- Manual or auto bitslip control selection logic:
         --------------------------------------------------
      
         rxBitSlip_to_gxLatOpt(i)                  <= '0';
      
      end generate;
      
   end generate;
   
   --================================================--
   -- Multi-Gigabit Transceivers (latency-optimized) --
   --================================================-- 
   
   -- MGT latency-optimized x1:
   ----------------------------
   
   gxLatOpt_x1_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS = 1 generate
      
      gxLatOpt_x1: alt_a10_gx_latopt_x1
         port map (
			rx_analogreset(0)          => rxAnalogReset_from_gxRstCtrl(1),
			rx_cal_busy(0)             => rxCalBusy_from_gxLatOpt(1),
			rx_cdr_refclk0             => MGT_CLKS_I.mgtRxRefClk,
			rx_clkout(0)           	   => MGT_CLKS_O.rx_wordClk(1),
			rx_coreclkin(0)            => MGT_CLKS_I.rx_wordClk(1),
			rx_digitalreset(0)         => rxDigitalReset_from_gxRstCtrl(1),
			rx_is_lockedtodata(0)      => rxIsLockedToData_from_gxLatOpt(1),
			rx_is_lockedtoref(0)       => MGT_O.mgtLink(1).rxIsLocked_toRef,
			rx_parallel_data      	   => GBTRX_WORD_O(1),
			rx_pma_clkslip(0)          => rxBitSlip_to_gxLatOpt(1),
			rx_polinv(0)               => MGT_I.mgtLink(1).rx_polarity,
			rx_serial_data(0)          => MGT_I.mgtLink(1).rxSerialData,
			rx_seriallpbken(0)         => MGT_I.mgtLink(1).loopBack,
			tx_analogreset(0)          => txAnalogReset_from_gxRstCtrl(1),
			tx_bonding_clocks	       => MGT_CLKS_I.extGxTxPll_clk, --in  std_logic_vector(5 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy(0)             => txCalBusy_from_gxLatOpt(1),
			tx_clkout(0)               => MGT_CLKS_O.tx_wordClk(1),
			tx_coreclkin(0)            => MGT_CLKS_I.tx_wordClk(1),
			tx_digitalreset(0)         => txDigitalReset_from_gxRstCtrl(1),
			tx_parallel_data      	   => GBTTX_WORD_I(1),
			tx_polinv(0)               => MGT_I.mgtLink(1).tx_polarity,
			tx_serial_data(0)          => MGT_O.mgtLink(1).txSerialData,
			unused_tx_parallel_data    => unused_tx_parallel_data_int(87 downto 0),
			unused_rx_parallel_data    => unused_rx_parallel_data_int(87 downto 0),
			reconfig_clk               => MGT_I.mgtLink(1).reconfig_clk,
			reconfig_reset             => MGT_I.mgtLink(1).reconfig_reset,
			reconfig_write             => MGT_I.mgtLink(1).reconfig_write,
			reconfig_read              => MGT_I.mgtLink(1).reconfig_read,
			reconfig_address		   => MGT_I.mgtLink(1).reconfig_address(9 downto 0),
			reconfig_writedata         => MGT_I.mgtLink(1).reconfig_writedata,
			reconfig_readdata          => MGT_O.mgtLink(1).reconfig_readdata,
			reconfig_waitrequest       => MGT_O.mgtLink(1).reconfig_waitrequest		
         );
   end generate;
   
   -- MGT latency-optimized x2:
   ----------------------------
   
   gxLatOpt_x2_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS = 2 generate
      
      gxLatOpt_x2: alt_a10_gx_latopt_x2
         port map (
			rx_analogreset(0)       				=> rxAnalogReset_from_gxRstCtrl(1),
			rx_analogreset(1)       				=> rxAnalogReset_from_gxRstCtrl(2),
			rx_cal_busy(0)          				=> rxCalBusy_from_gxLatOpt(1),
			rx_cal_busy(1)          				=> rxCalBusy_from_gxLatOpt(2),
			rx_cdr_refclk0         			 		=> MGT_CLKS_I.mgtRxRefClk,
			rx_clkout(0)            				=> MGT_CLKS_O.rx_wordClk(1), 
			rx_clkout(1)            				=> MGT_CLKS_O.rx_wordClk(2), 
			rx_coreclkin(0)         				=> MGT_CLKS_I.rx_wordClk(1),
			rx_coreclkin(1)         				=> MGT_CLKS_I.rx_wordClk(2),
			rx_digitalreset(0)      				=> rxDigitalReset_from_gxRstCtrl(1),
			rx_digitalreset(1)     					=> rxDigitalReset_from_gxRstCtrl(2),
			rx_is_lockedtodata(0)   				=> rxIsLockedToData_from_gxLatOpt(1),
			rx_is_lockedtodata(1)   				=> rxIsLockedToData_from_gxLatOpt(2),
			rx_is_lockedtoref(0)    				=> MGT_O.mgtLink(1).rxIsLocked_toRef,
			rx_is_lockedtoref(1)    				=> MGT_O.mgtLink(2).rxIsLocked_toRef,
			rx_parallel_data(39 downto  0)          => GBTRX_WORD_O(1),  
			rx_parallel_data(79 downto  40)         => GBTRX_WORD_O(2),  
			rx_pma_clkslip(0)         				=> rxBitSlip_to_gxLatOpt(1),
			rx_pma_clkslip(1)         				=> rxBitSlip_to_gxLatOpt(2),
			rx_polinv(0)               				=> MGT_I.mgtLink(1).rx_polarity,	
			rx_polinv(1)               				=> MGT_I.mgtLink(2).rx_polarity,	
			rx_serial_data(0)          				=> MGT_I.mgtLink(1).rxSerialData,
			rx_serial_data(1)          				=> MGT_I.mgtLink(2).rxSerialData,
			rx_seriallpbken(0)   			      	=> MGT_I.mgtLink(1).loopBack,
			rx_seriallpbken(1)   			      	=> MGT_I.mgtLink(2).loopBack,
			tx_analogreset(0)						=> txAnalogReset_from_gxRstCtrl(1),          
			tx_analogreset(1)						=> txAnalogReset_from_gxRstCtrl(2),          
			tx_bonding_clocks(5 downto 0)			=> MGT_CLKS_I.extGxTxPll_clk,--: in  std_logic_vector(11 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy(0) 			            	=> txCalBusy_from_gxLatOpt(1),
			tx_cal_busy(1) 			            	=> txCalBusy_from_gxLatOpt(2),
			tx_clkout(0)               				=> MGT_CLKS_O.tx_wordClk(1),
			tx_clkout(1)               				=> MGT_CLKS_O.tx_wordClk(2),
			tx_coreclkin(0)            				=> MGT_CLKS_I.tx_wordClk(1),
			tx_coreclkin(1)           				=> MGT_CLKS_I.tx_wordClk(2),
			tx_digitalreset(0)         				=> txDigitalReset_from_gxRstCtrl(1),
			tx_digitalreset(1)         				=> txDigitalReset_from_gxRstCtrl(2),
			tx_parallel_data(39 downto  0)	        => GBTTX_WORD_I(1),
			tx_parallel_data(79 downto  40)	        => GBTTX_WORD_I(2),
			tx_polinv(0)               				=> MGT_I.mgtLink(1).tx_polarity,
			tx_polinv(1)               				=> MGT_I.mgtLink(2).tx_polarity,
			tx_serial_data(0)          				=> MGT_O.mgtLink(1).txSerialData,
			tx_serial_data(1)          				=> MGT_O.mgtLink(2).txSerialData,
			unused_tx_parallel_data    => unused_tx_parallel_data_int(175 downto 0),
			unused_rx_parallel_data    => unused_rx_parallel_data_int(175 downto 0),
			reconfig_clk               => MGT_I.mgtLink(1).reconfig_clk,
			reconfig_reset             => MGT_I.mgtLink(1).reconfig_reset,
			reconfig_write             => MGT_I.mgtLink(1).reconfig_write,
			reconfig_read              => MGT_I.mgtLink(1).reconfig_read,
			reconfig_address           => MGT_I.mgtLink(1).reconfig_address(10 downto 0),
			reconfig_writedata         => MGT_I.mgtLink(1).reconfig_writedata,
			reconfig_readdata          => MGT_O.mgtLink(1).reconfig_readdata,
			reconfig_waitrequest       => MGT_O.mgtLink(1).reconfig_waitrequest
         );
         
      end generate;   
   
   -- MGT latency-optimized x3:
   ----------------------------
   
   gxLatOpt_x3_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS = 3 generate
      
      gxLatOpt_x3: alt_a10_gx_latopt_x3
	  port map (
			rx_analogreset(0)       				=> rxAnalogReset_from_gxRstCtrl(1),
			rx_analogreset(1)       				=> rxAnalogReset_from_gxRstCtrl(2),
			rx_analogreset(2)       				=> rxAnalogReset_from_gxRstCtrl(3),
			rx_cal_busy(0)          				=> rxCalBusy_from_gxLatOpt(1),
			rx_cal_busy(1)          				=> rxCalBusy_from_gxLatOpt(2),
			rx_cal_busy(2)          				=> rxCalBusy_from_gxLatOpt(3),
			rx_cdr_refclk0         			 		=> MGT_CLKS_I.mgtRxRefClk,
			rx_clkout(0)            				=> MGT_CLKS_O.rx_wordClk(1), 
			rx_clkout(1)            				=> MGT_CLKS_O.rx_wordClk(2), 
			rx_clkout(2)            				=> MGT_CLKS_O.rx_wordClk(3), 
			rx_coreclkin(0)         				=> MGT_CLKS_I.rx_wordClk(1),
			rx_coreclkin(1)         				=> MGT_CLKS_I.rx_wordClk(2),
			rx_coreclkin(2)         				=> MGT_CLKS_I.rx_wordClk(3),
			rx_digitalreset(0)      				=> rxDigitalReset_from_gxRstCtrl(1),
			rx_digitalreset(1)     					=> rxDigitalReset_from_gxRstCtrl(2),
			rx_digitalreset(2)     					=> rxDigitalReset_from_gxRstCtrl(3),
			rx_is_lockedtodata(0)   				=> rxIsLockedToData_from_gxLatOpt(1),
			rx_is_lockedtodata(1)   				=> rxIsLockedToData_from_gxLatOpt(2),
			rx_is_lockedtodata(2)   				=> rxIsLockedToData_from_gxLatOpt(3),
			rx_is_lockedtoref(0)    				=> MGT_O.mgtLink(1).rxIsLocked_toRef,
			rx_is_lockedtoref(1)    				=> MGT_O.mgtLink(2).rxIsLocked_toRef,
			rx_is_lockedtoref(2)    				=> MGT_O.mgtLink(3).rxIsLocked_toRef,
			rx_parallel_data(39 downto  0)          => GBTRX_WORD_O(1),  
			rx_parallel_data(79 downto  40)         => GBTRX_WORD_O(2),  
			rx_parallel_data(119 downto 80)         => GBTRX_WORD_O(3),  
			rx_pma_clkslip(0)         				=> rxBitSlip_to_gxLatOpt(1),
			rx_pma_clkslip(1)         				=> rxBitSlip_to_gxLatOpt(2),
			rx_pma_clkslip(2)         				=> rxBitSlip_to_gxLatOpt(3),
			rx_polinv(0)               				=> MGT_I.mgtLink(1).rx_polarity,	
			rx_polinv(1)               				=> MGT_I.mgtLink(2).rx_polarity,	
			rx_polinv(2)               				=> MGT_I.mgtLink(3).rx_polarity,	
			rx_serial_data(0)          				=> MGT_I.mgtLink(1).rxSerialData,
			rx_serial_data(1)          				=> MGT_I.mgtLink(2).rxSerialData,
			rx_serial_data(2)          				=> MGT_I.mgtLink(3).rxSerialData,
			rx_seriallpbken(0)   			      	=> MGT_I.mgtLink(1).loopBack,
			rx_seriallpbken(1)   			      	=> MGT_I.mgtLink(2).loopBack,
			rx_seriallpbken(2)   			      	=> MGT_I.mgtLink(3).loopBack,
			tx_analogreset(0)						=> txAnalogReset_from_gxRstCtrl(1),          
			tx_analogreset(1)						=> txAnalogReset_from_gxRstCtrl(2),          
			tx_analogreset(2)						=> txAnalogReset_from_gxRstCtrl(3),          
			tx_bonding_clocks( 5 downto  0)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(11 downto  6)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(17 downto 12)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy(0) 			            	=> txCalBusy_from_gxLatOpt(1),
			tx_cal_busy(1) 			            	=> txCalBusy_from_gxLatOpt(2),
			tx_cal_busy(2) 			            	=> txCalBusy_from_gxLatOpt(3),
			tx_clkout(0)               				=> MGT_CLKS_O.tx_wordClk(1),
			tx_clkout(1)               				=> MGT_CLKS_O.tx_wordClk(2),
			tx_clkout(2)               				=> MGT_CLKS_O.tx_wordClk(3),
			tx_coreclkin(0)            				=> MGT_CLKS_I.tx_wordClk(1),
			tx_coreclkin(1)           				=> MGT_CLKS_I.tx_wordClk(2),
			tx_coreclkin(2)           				=> MGT_CLKS_I.tx_wordClk(3),
			tx_digitalreset(0)         				=> txDigitalReset_from_gxRstCtrl(1),
			tx_digitalreset(1)         				=> txDigitalReset_from_gxRstCtrl(2),
			tx_digitalreset(2)         				=> txDigitalReset_from_gxRstCtrl(3),
			tx_parallel_data(39 downto  0)	        => GBTTX_WORD_I(1),
			tx_parallel_data(79 downto  40)	        => GBTTX_WORD_I(2),
			tx_parallel_data(119 downto 80)	        => GBTTX_WORD_I(2),
			tx_polinv(0)               				=> MGT_I.mgtLink(1).tx_polarity,
			tx_polinv(1)               				=> MGT_I.mgtLink(2).tx_polarity,
			tx_polinv(2)               				=> MGT_I.mgtLink(3).tx_polarity,
			tx_serial_data(0)          				=> MGT_O.mgtLink(1).txSerialData,
			tx_serial_data(1)          				=> MGT_O.mgtLink(2).txSerialData,
			tx_serial_data(2)          				=> MGT_O.mgtLink(3).txSerialData,
			unused_tx_parallel_data    => unused_tx_parallel_data_int(263 downto 0),
			unused_rx_parallel_data    => unused_rx_parallel_data_int(263 downto 0),
			reconfig_clk               => MGT_I.mgtLink(1).reconfig_clk,
			reconfig_reset             => MGT_I.mgtLink(1).reconfig_reset,
			reconfig_write             => MGT_I.mgtLink(1).reconfig_write,
			reconfig_read              => MGT_I.mgtLink(1).reconfig_read,
			reconfig_address           => MGT_I.mgtLink(1).reconfig_address(11 downto 0),
			reconfig_writedata         => MGT_I.mgtLink(1).reconfig_writedata,
			reconfig_readdata          => MGT_O.mgtLink(1).reconfig_readdata,
			reconfig_waitrequest       => MGT_O.mgtLink(1).reconfig_waitrequest
         );         
   end generate;

   -- MGT latency-optimized x4:
   ----------------------------
   
   gxLatOpt_x4_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS = 4 generate
      
      gxLatOpt_x4: alt_a10_gx_latopt_x4
	  port map (
			rx_analogreset(0)       				=> rxAnalogReset_from_gxRstCtrl(1),
			rx_analogreset(1)       				=> rxAnalogReset_from_gxRstCtrl(2),
			rx_analogreset(2)       				=> rxAnalogReset_from_gxRstCtrl(3),
			rx_analogreset(3)       				=> rxAnalogReset_from_gxRstCtrl(4),
			rx_cal_busy(0)          				=> rxCalBusy_from_gxLatOpt(1),
			rx_cal_busy(1)          				=> rxCalBusy_from_gxLatOpt(2),
			rx_cal_busy(2)          				=> rxCalBusy_from_gxLatOpt(3),
			rx_cal_busy(3)          				=> rxCalBusy_from_gxLatOpt(4),
			rx_cdr_refclk0         			 		=> MGT_CLKS_I.mgtRxRefClk,
			rx_clkout(0)            				=> MGT_CLKS_O.rx_wordClk(1), 
			rx_clkout(1)            				=> MGT_CLKS_O.rx_wordClk(2), 
			rx_clkout(2)            				=> MGT_CLKS_O.rx_wordClk(3), 
			rx_clkout(3)            				=> MGT_CLKS_O.rx_wordClk(4), 
			rx_coreclkin(0)         				=> MGT_CLKS_I.rx_wordClk(1),
			rx_coreclkin(1)         				=> MGT_CLKS_I.rx_wordClk(2),
			rx_coreclkin(2)         				=> MGT_CLKS_I.rx_wordClk(3),
			rx_coreclkin(3)         				=> MGT_CLKS_I.rx_wordClk(4),
			rx_digitalreset(0)      				=> rxDigitalReset_from_gxRstCtrl(1),
			rx_digitalreset(1)     					=> rxDigitalReset_from_gxRstCtrl(2),
			rx_digitalreset(2)     					=> rxDigitalReset_from_gxRstCtrl(3),
			rx_digitalreset(3)     					=> rxDigitalReset_from_gxRstCtrl(4),
			rx_is_lockedtodata(0)   				=> rxIsLockedToData_from_gxLatOpt(1),
			rx_is_lockedtodata(1)   				=> rxIsLockedToData_from_gxLatOpt(2),
			rx_is_lockedtodata(2)   				=> rxIsLockedToData_from_gxLatOpt(3),
			rx_is_lockedtodata(3)   				=> rxIsLockedToData_from_gxLatOpt(4),
			rx_is_lockedtoref(0)    				=> MGT_O.mgtLink(1).rxIsLocked_toRef,
			rx_is_lockedtoref(1)    				=> MGT_O.mgtLink(2).rxIsLocked_toRef,
			rx_is_lockedtoref(2)    				=> MGT_O.mgtLink(3).rxIsLocked_toRef,
			rx_is_lockedtoref(3)    				=> MGT_O.mgtLink(4).rxIsLocked_toRef,
			rx_parallel_data(39 downto  0)          => GBTRX_WORD_O(1),  
			rx_parallel_data(79 downto  40)         => GBTRX_WORD_O(2),  
			rx_parallel_data(119 downto 80)         => GBTRX_WORD_O(3),  
			rx_parallel_data(159 downto 120)        => GBTRX_WORD_O(4),  
			rx_pma_clkslip(0)         				=> rxBitSlip_to_gxLatOpt(1),
			rx_pma_clkslip(1)         				=> rxBitSlip_to_gxLatOpt(2),
			rx_pma_clkslip(2)         				=> rxBitSlip_to_gxLatOpt(3),
			rx_pma_clkslip(3)         				=> rxBitSlip_to_gxLatOpt(4),
			rx_polinv(0)               				=> MGT_I.mgtLink(1).rx_polarity,	
			rx_polinv(1)               				=> MGT_I.mgtLink(2).rx_polarity,	
			rx_polinv(2)               				=> MGT_I.mgtLink(3).rx_polarity,	
			rx_polinv(3)               				=> MGT_I.mgtLink(4).rx_polarity,	
			rx_serial_data(0)          				=> MGT_I.mgtLink(1).rxSerialData,
			rx_serial_data(1)          				=> MGT_I.mgtLink(2).rxSerialData,
			rx_serial_data(2)          				=> MGT_I.mgtLink(3).rxSerialData,
			rx_serial_data(3)          				=> MGT_I.mgtLink(4).rxSerialData,
			rx_seriallpbken(0)   			      	=> MGT_I.mgtLink(1).loopBack,
			rx_seriallpbken(1)   			      	=> MGT_I.mgtLink(2).loopBack,
			rx_seriallpbken(2)   			      	=> MGT_I.mgtLink(3).loopBack,
			rx_seriallpbken(3)   			      	=> MGT_I.mgtLink(4).loopBack,
			tx_analogreset(0)						=> txAnalogReset_from_gxRstCtrl(1),          
			tx_analogreset(1)						=> txAnalogReset_from_gxRstCtrl(2),          
			tx_analogreset(2)						=> txAnalogReset_from_gxRstCtrl(3),          
			tx_analogreset(3)						=> txAnalogReset_from_gxRstCtrl(4),          
			tx_bonding_clocks( 5 downto  0)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(11 downto  6)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(17 downto 12)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(23 downto 18)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy(0) 			            	=> txCalBusy_from_gxLatOpt(1),
			tx_cal_busy(1) 			            	=> txCalBusy_from_gxLatOpt(2),
			tx_cal_busy(2) 			            	=> txCalBusy_from_gxLatOpt(3),
			tx_cal_busy(3) 			            	=> txCalBusy_from_gxLatOpt(4),
			tx_clkout(0)               				=> MGT_CLKS_O.tx_wordClk(1),
			tx_clkout(1)               				=> MGT_CLKS_O.tx_wordClk(2),
			tx_clkout(2)               				=> MGT_CLKS_O.tx_wordClk(3),
			tx_clkout(3)               				=> MGT_CLKS_O.tx_wordClk(4),
			tx_coreclkin(0)            				=> MGT_CLKS_I.tx_wordClk(1),
			tx_coreclkin(1)           				=> MGT_CLKS_I.tx_wordClk(2),
			tx_coreclkin(2)           				=> MGT_CLKS_I.tx_wordClk(3),
			tx_coreclkin(3)           				=> MGT_CLKS_I.tx_wordClk(4),
			tx_digitalreset(0)         				=> txDigitalReset_from_gxRstCtrl(1),
			tx_digitalreset(1)         				=> txDigitalReset_from_gxRstCtrl(2),
			tx_digitalreset(2)         				=> txDigitalReset_from_gxRstCtrl(3),
			tx_digitalreset(3)         				=> txDigitalReset_from_gxRstCtrl(4),
			tx_parallel_data(39 downto  0)	        => GBTTX_WORD_I(1),
			tx_parallel_data(79 downto  40)	        => GBTTX_WORD_I(2),
			tx_parallel_data(119 downto 80)	        => GBTTX_WORD_I(3),
			tx_parallel_data(159 downto 120)	    => GBTTX_WORD_I(4),
			tx_polinv(0)               				=> MGT_I.mgtLink(1).tx_polarity,
			tx_polinv(1)               				=> MGT_I.mgtLink(2).tx_polarity,
			tx_polinv(2)               				=> MGT_I.mgtLink(3).tx_polarity,
			tx_polinv(3)               				=> MGT_I.mgtLink(4).tx_polarity,
			tx_serial_data(0)          				=> MGT_O.mgtLink(1).txSerialData,
			tx_serial_data(1)          				=> MGT_O.mgtLink(2).txSerialData,
			tx_serial_data(2)          				=> MGT_O.mgtLink(3).txSerialData,
			tx_serial_data(3)          				=> MGT_O.mgtLink(4).txSerialData,
			unused_tx_parallel_data    => unused_tx_parallel_data_int(351 downto 0),
			unused_rx_parallel_data    => unused_rx_parallel_data_int(351 downto 0),
			reconfig_clk               => MGT_I.mgtLink(1).reconfig_clk,
			reconfig_reset             => MGT_I.mgtLink(1).reconfig_reset,
			reconfig_write             => MGT_I.mgtLink(1).reconfig_write,
			reconfig_read              => MGT_I.mgtLink(1).reconfig_read,
			reconfig_address           => MGT_I.mgtLink(1).reconfig_address(11 downto 0),
			reconfig_writedata         => MGT_I.mgtLink(1).reconfig_writedata,
			reconfig_readdata          => MGT_O.mgtLink(1).reconfig_readdata,
			reconfig_waitrequest       => MGT_O.mgtLink(1).reconfig_waitrequest
         );         
   end generate;
   
   -- MGT latency-optimized x5:
   ----------------------------
   
   gxLatOpt_x5_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS = 5 generate
      
      gxLatOpt_x5: alt_a10_gx_latopt_x5
	  port map (
			rx_analogreset(0)       				=> rxAnalogReset_from_gxRstCtrl(1),
			rx_analogreset(1)       				=> rxAnalogReset_from_gxRstCtrl(2),
			rx_analogreset(2)       				=> rxAnalogReset_from_gxRstCtrl(3),
			rx_analogreset(3)       				=> rxAnalogReset_from_gxRstCtrl(4),
			rx_analogreset(4)       				=> rxAnalogReset_from_gxRstCtrl(5),
			rx_cal_busy(0)          				=> rxCalBusy_from_gxLatOpt(1),
			rx_cal_busy(1)          				=> rxCalBusy_from_gxLatOpt(2),
			rx_cal_busy(2)          				=> rxCalBusy_from_gxLatOpt(3),
			rx_cal_busy(3)          				=> rxCalBusy_from_gxLatOpt(4),
			rx_cal_busy(4)          				=> rxCalBusy_from_gxLatOpt(5),
			rx_cdr_refclk0         			 		=> MGT_CLKS_I.mgtRxRefClk,
			rx_clkout(0)            				=> MGT_CLKS_O.rx_wordClk(1), 
			rx_clkout(1)            				=> MGT_CLKS_O.rx_wordClk(2), 
			rx_clkout(2)            				=> MGT_CLKS_O.rx_wordClk(3), 
			rx_clkout(3)            				=> MGT_CLKS_O.rx_wordClk(4), 
			rx_clkout(4)            				=> MGT_CLKS_O.rx_wordClk(5), 
			rx_coreclkin(0)         				=> MGT_CLKS_I.rx_wordClk(1),
			rx_coreclkin(1)         				=> MGT_CLKS_I.rx_wordClk(2),
			rx_coreclkin(2)         				=> MGT_CLKS_I.rx_wordClk(3),
			rx_coreclkin(3)         				=> MGT_CLKS_I.rx_wordClk(4),
			rx_coreclkin(4)         				=> MGT_CLKS_I.rx_wordClk(5),
			rx_digitalreset(0)      				=> rxDigitalReset_from_gxRstCtrl(1),
			rx_digitalreset(1)     					=> rxDigitalReset_from_gxRstCtrl(2),
			rx_digitalreset(2)     					=> rxDigitalReset_from_gxRstCtrl(3),
			rx_digitalreset(3)     					=> rxDigitalReset_from_gxRstCtrl(4),
			rx_digitalreset(4)     					=> rxDigitalReset_from_gxRstCtrl(5),
			rx_is_lockedtodata(0)   				=> rxIsLockedToData_from_gxLatOpt(1),
			rx_is_lockedtodata(1)   				=> rxIsLockedToData_from_gxLatOpt(2),
			rx_is_lockedtodata(2)   				=> rxIsLockedToData_from_gxLatOpt(3),
			rx_is_lockedtodata(3)   				=> rxIsLockedToData_from_gxLatOpt(4),
			rx_is_lockedtodata(4)   				=> rxIsLockedToData_from_gxLatOpt(5),
			rx_is_lockedtoref(0)    				=> MGT_O.mgtLink(1).rxIsLocked_toRef,
			rx_is_lockedtoref(1)    				=> MGT_O.mgtLink(2).rxIsLocked_toRef,
			rx_is_lockedtoref(2)    				=> MGT_O.mgtLink(3).rxIsLocked_toRef,
			rx_is_lockedtoref(3)    				=> MGT_O.mgtLink(4).rxIsLocked_toRef,
			rx_is_lockedtoref(4)    				=> MGT_O.mgtLink(5).rxIsLocked_toRef,
			rx_parallel_data(39 downto  0)          => GBTRX_WORD_O(1),  
			rx_parallel_data(79 downto  40)         => GBTRX_WORD_O(2),  
			rx_parallel_data(119 downto 80)         => GBTRX_WORD_O(3),  
			rx_parallel_data(159 downto 120)        => GBTRX_WORD_O(4),  
			rx_parallel_data(199 downto 160)        => GBTRX_WORD_O(5),  
			rx_pma_clkslip(0)         				=> rxBitSlip_to_gxLatOpt(1),
			rx_pma_clkslip(1)         				=> rxBitSlip_to_gxLatOpt(2),
			rx_pma_clkslip(2)         				=> rxBitSlip_to_gxLatOpt(3),
			rx_pma_clkslip(3)         				=> rxBitSlip_to_gxLatOpt(4),
			rx_pma_clkslip(4)         				=> rxBitSlip_to_gxLatOpt(5),
			rx_polinv(0)               				=> MGT_I.mgtLink(1).rx_polarity,	
			rx_polinv(1)               				=> MGT_I.mgtLink(2).rx_polarity,	
			rx_polinv(2)               				=> MGT_I.mgtLink(3).rx_polarity,	
			rx_polinv(3)               				=> MGT_I.mgtLink(4).rx_polarity,	
			rx_polinv(4)               				=> MGT_I.mgtLink(5).rx_polarity,	
			rx_serial_data(0)          				=> MGT_I.mgtLink(1).rxSerialData,
			rx_serial_data(1)          				=> MGT_I.mgtLink(2).rxSerialData,
			rx_serial_data(2)          				=> MGT_I.mgtLink(3).rxSerialData,
			rx_serial_data(3)          				=> MGT_I.mgtLink(4).rxSerialData,
			rx_serial_data(4)          				=> MGT_I.mgtLink(5).rxSerialData,
			rx_seriallpbken(0)   			      	=> MGT_I.mgtLink(1).loopBack,
			rx_seriallpbken(1)   			      	=> MGT_I.mgtLink(2).loopBack,
			rx_seriallpbken(2)   			      	=> MGT_I.mgtLink(3).loopBack,
			rx_seriallpbken(3)   			      	=> MGT_I.mgtLink(4).loopBack,
			rx_seriallpbken(4)   			      	=> MGT_I.mgtLink(5).loopBack,
			tx_analogreset(0)						=> txAnalogReset_from_gxRstCtrl(1),          
			tx_analogreset(1)						=> txAnalogReset_from_gxRstCtrl(2),          
			tx_analogreset(2)						=> txAnalogReset_from_gxRstCtrl(3),          
			tx_analogreset(3)						=> txAnalogReset_from_gxRstCtrl(4),          
			tx_analogreset(4)						=> txAnalogReset_from_gxRstCtrl(5),          
			tx_bonding_clocks( 5 downto  0)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(11 downto  6)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(17 downto 12)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(23 downto 18)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(29 downto 24)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy(0) 			            	=> txCalBusy_from_gxLatOpt(1),
			tx_cal_busy(1) 			            	=> txCalBusy_from_gxLatOpt(2),
			tx_cal_busy(2) 			            	=> txCalBusy_from_gxLatOpt(3),
			tx_cal_busy(3) 			            	=> txCalBusy_from_gxLatOpt(4),
			tx_cal_busy(4) 			            	=> txCalBusy_from_gxLatOpt(5),
			tx_clkout(0)               				=> MGT_CLKS_O.tx_wordClk(1),
			tx_clkout(1)               				=> MGT_CLKS_O.tx_wordClk(2),
			tx_clkout(2)               				=> MGT_CLKS_O.tx_wordClk(3),
			tx_clkout(3)               				=> MGT_CLKS_O.tx_wordClk(4),
			tx_clkout(4)               				=> MGT_CLKS_O.tx_wordClk(5),
			tx_coreclkin(0)            				=> MGT_CLKS_I.tx_wordClk(1),
			tx_coreclkin(1)           				=> MGT_CLKS_I.tx_wordClk(2),
			tx_coreclkin(2)           				=> MGT_CLKS_I.tx_wordClk(3),
			tx_coreclkin(3)           				=> MGT_CLKS_I.tx_wordClk(4),
			tx_coreclkin(4)           				=> MGT_CLKS_I.tx_wordClk(5),
			tx_digitalreset(0)         				=> txDigitalReset_from_gxRstCtrl(1),
			tx_digitalreset(1)         				=> txDigitalReset_from_gxRstCtrl(2),
			tx_digitalreset(2)         				=> txDigitalReset_from_gxRstCtrl(3),
			tx_digitalreset(3)         				=> txDigitalReset_from_gxRstCtrl(4),
			tx_digitalreset(4)         				=> txDigitalReset_from_gxRstCtrl(5),
			tx_parallel_data(39 downto  0)	        => GBTTX_WORD_I(1),
			tx_parallel_data(79 downto  40)	        => GBTTX_WORD_I(2),
			tx_parallel_data(119 downto 80)	        => GBTTX_WORD_I(3),
			tx_parallel_data(159 downto 120)	    => GBTTX_WORD_I(4),
			tx_parallel_data(199 downto 160)	    => GBTTX_WORD_I(5),
			tx_polinv(0)               				=> MGT_I.mgtLink(1).tx_polarity,
			tx_polinv(1)               				=> MGT_I.mgtLink(2).tx_polarity,
			tx_polinv(2)               				=> MGT_I.mgtLink(3).tx_polarity,
			tx_polinv(3)               				=> MGT_I.mgtLink(4).tx_polarity,
			tx_polinv(4)               				=> MGT_I.mgtLink(5).tx_polarity,
			tx_serial_data(0)          				=> MGT_O.mgtLink(1).txSerialData,
			tx_serial_data(1)          				=> MGT_O.mgtLink(2).txSerialData,
			tx_serial_data(2)          				=> MGT_O.mgtLink(3).txSerialData,
			tx_serial_data(3)          				=> MGT_O.mgtLink(4).txSerialData,
			tx_serial_data(4)          				=> MGT_O.mgtLink(5).txSerialData,
			unused_tx_parallel_data    => unused_tx_parallel_data_int(439 downto 0),
			unused_rx_parallel_data    => unused_rx_parallel_data_int(439 downto 0),
			reconfig_clk               => MGT_I.mgtLink(1).reconfig_clk,
			reconfig_reset             => MGT_I.mgtLink(1).reconfig_reset,
			reconfig_write             => MGT_I.mgtLink(1).reconfig_write,
			reconfig_read              => MGT_I.mgtLink(1).reconfig_read,
			reconfig_address           => MGT_I.mgtLink(1).reconfig_address(12 downto 0),
			reconfig_writedata         => MGT_I.mgtLink(1).reconfig_writedata,
			reconfig_readdata          => MGT_O.mgtLink(1).reconfig_readdata,
			reconfig_waitrequest       => MGT_O.mgtLink(1).reconfig_waitrequest
         );         
   end generate;

   -- MGT latency-optimized x6:
   ----------------------------
   
   gxLatOpt_x6_gen: if GBT_BANKS_USER_SETUP(GBT_BANK_ID).NUM_LINKS = 6 generate
      
      gxLatOpt_x6: alt_a10_gx_latopt_x6
	  port map (
			rx_analogreset(0)       				=> rxAnalogReset_from_gxRstCtrl(1),
			rx_analogreset(1)       				=> rxAnalogReset_from_gxRstCtrl(2),
			rx_analogreset(2)       				=> rxAnalogReset_from_gxRstCtrl(3),
			rx_analogreset(3)       				=> rxAnalogReset_from_gxRstCtrl(4),
			rx_analogreset(4)       				=> rxAnalogReset_from_gxRstCtrl(5),
			rx_analogreset(5)       				=> rxAnalogReset_from_gxRstCtrl(6),
			rx_cal_busy(0)          				=> rxCalBusy_from_gxLatOpt(1),
			rx_cal_busy(1)          				=> rxCalBusy_from_gxLatOpt(2),
			rx_cal_busy(2)          				=> rxCalBusy_from_gxLatOpt(3),
			rx_cal_busy(3)          				=> rxCalBusy_from_gxLatOpt(4),
			rx_cal_busy(4)          				=> rxCalBusy_from_gxLatOpt(5),
			rx_cal_busy(5)          				=> rxCalBusy_from_gxLatOpt(6),
			rx_cdr_refclk0         			 		=> MGT_CLKS_I.mgtRxRefClk,
			rx_clkout(0)            				=> MGT_CLKS_O.rx_wordClk(1), 
			rx_clkout(1)            				=> MGT_CLKS_O.rx_wordClk(2), 
			rx_clkout(2)            				=> MGT_CLKS_O.rx_wordClk(3), 
			rx_clkout(3)            				=> MGT_CLKS_O.rx_wordClk(4), 
			rx_clkout(4)            				=> MGT_CLKS_O.rx_wordClk(5), 
			rx_clkout(5)            				=> MGT_CLKS_O.rx_wordClk(6), 
			rx_coreclkin(0)         				=> MGT_CLKS_I.rx_wordClk(1),
			rx_coreclkin(1)         				=> MGT_CLKS_I.rx_wordClk(2),
			rx_coreclkin(2)         				=> MGT_CLKS_I.rx_wordClk(3),
			rx_coreclkin(3)         				=> MGT_CLKS_I.rx_wordClk(4),
			rx_coreclkin(4)         				=> MGT_CLKS_I.rx_wordClk(5),
			rx_coreclkin(5)         				=> MGT_CLKS_I.rx_wordClk(6),
			rx_digitalreset(0)      				=> rxDigitalReset_from_gxRstCtrl(1),
			rx_digitalreset(1)     					=> rxDigitalReset_from_gxRstCtrl(2),
			rx_digitalreset(2)     					=> rxDigitalReset_from_gxRstCtrl(3),
			rx_digitalreset(3)     					=> rxDigitalReset_from_gxRstCtrl(4),
			rx_digitalreset(4)     					=> rxDigitalReset_from_gxRstCtrl(5),
			rx_digitalreset(5)     					=> rxDigitalReset_from_gxRstCtrl(6),
			rx_is_lockedtodata(0)   				=> rxIsLockedToData_from_gxLatOpt(1),
			rx_is_lockedtodata(1)   				=> rxIsLockedToData_from_gxLatOpt(2),
			rx_is_lockedtodata(2)   				=> rxIsLockedToData_from_gxLatOpt(3),
			rx_is_lockedtodata(3)   				=> rxIsLockedToData_from_gxLatOpt(4),
			rx_is_lockedtodata(4)   				=> rxIsLockedToData_from_gxLatOpt(5),
			rx_is_lockedtodata(5)   				=> rxIsLockedToData_from_gxLatOpt(6),
			rx_is_lockedtoref(0)    				=> MGT_O.mgtLink(1).rxIsLocked_toRef,
			rx_is_lockedtoref(1)    				=> MGT_O.mgtLink(2).rxIsLocked_toRef,
			rx_is_lockedtoref(2)    				=> MGT_O.mgtLink(3).rxIsLocked_toRef,
			rx_is_lockedtoref(3)    				=> MGT_O.mgtLink(4).rxIsLocked_toRef,
			rx_is_lockedtoref(4)    				=> MGT_O.mgtLink(5).rxIsLocked_toRef,
			rx_is_lockedtoref(5)    				=> MGT_O.mgtLink(6).rxIsLocked_toRef,
			rx_parallel_data(39 downto  0)          => GBTRX_WORD_O(1),  
			rx_parallel_data(79 downto  40)         => GBTRX_WORD_O(2),  
			rx_parallel_data(119 downto 80)         => GBTRX_WORD_O(3),  
			rx_parallel_data(159 downto 120)        => GBTRX_WORD_O(4),  
			rx_parallel_data(199 downto 160)        => GBTRX_WORD_O(5),  
			rx_parallel_data(239 downto 200)        => GBTRX_WORD_O(6),  
			rx_pma_clkslip(0)         				=> rxBitSlip_to_gxLatOpt(1),
			rx_pma_clkslip(1)         				=> rxBitSlip_to_gxLatOpt(2),
			rx_pma_clkslip(2)         				=> rxBitSlip_to_gxLatOpt(3),
			rx_pma_clkslip(3)         				=> rxBitSlip_to_gxLatOpt(4),
			rx_pma_clkslip(4)         				=> rxBitSlip_to_gxLatOpt(5),
			rx_pma_clkslip(5)         				=> rxBitSlip_to_gxLatOpt(6),
			rx_polinv(0)               				=> MGT_I.mgtLink(1).rx_polarity,	
			rx_polinv(1)               				=> MGT_I.mgtLink(2).rx_polarity,	
			rx_polinv(2)               				=> MGT_I.mgtLink(3).rx_polarity,	
			rx_polinv(3)               				=> MGT_I.mgtLink(4).rx_polarity,	
			rx_polinv(4)               				=> MGT_I.mgtLink(5).rx_polarity,	
			rx_polinv(5)               				=> MGT_I.mgtLink(6).rx_polarity,	
			rx_serial_data(0)          				=> MGT_I.mgtLink(1).rxSerialData,
			rx_serial_data(1)          				=> MGT_I.mgtLink(2).rxSerialData,
			rx_serial_data(2)          				=> MGT_I.mgtLink(3).rxSerialData,
			rx_serial_data(3)          				=> MGT_I.mgtLink(4).rxSerialData,
			rx_serial_data(4)          				=> MGT_I.mgtLink(5).rxSerialData,
			rx_serial_data(5)          				=> MGT_I.mgtLink(6).rxSerialData,
			rx_seriallpbken(0)   			      	=> MGT_I.mgtLink(1).loopBack,
			rx_seriallpbken(1)   			      	=> MGT_I.mgtLink(2).loopBack,
			rx_seriallpbken(2)   			      	=> MGT_I.mgtLink(3).loopBack,
			rx_seriallpbken(3)   			      	=> MGT_I.mgtLink(4).loopBack,
			rx_seriallpbken(4)   			      	=> MGT_I.mgtLink(5).loopBack,
			rx_seriallpbken(5)   			      	=> MGT_I.mgtLink(6).loopBack,
			tx_analogreset(0)						=> txAnalogReset_from_gxRstCtrl(1),          
			tx_analogreset(1)						=> txAnalogReset_from_gxRstCtrl(2),          
			tx_analogreset(2)						=> txAnalogReset_from_gxRstCtrl(3),          
			tx_analogreset(3)						=> txAnalogReset_from_gxRstCtrl(4),          
			tx_analogreset(4)						=> txAnalogReset_from_gxRstCtrl(5),          
			tx_analogreset(5)						=> txAnalogReset_from_gxRstCtrl(6),          
			tx_bonding_clocks( 5 downto  0)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(11 downto  6)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(17 downto 12)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(23 downto 18)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(29 downto 24)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_bonding_clocks(35 downto 30)			=> MGT_CLKS_I.extGxTxPll_clk,--       : in  std_logic_vector(17 downto 0)  := (others => 'X'); -- clk
			tx_cal_busy(0) 			            	=> txCalBusy_from_gxLatOpt(1),
			tx_cal_busy(1) 			            	=> txCalBusy_from_gxLatOpt(2),
			tx_cal_busy(2) 			            	=> txCalBusy_from_gxLatOpt(3),
			tx_cal_busy(3) 			            	=> txCalBusy_from_gxLatOpt(4),
			tx_cal_busy(4) 			            	=> txCalBusy_from_gxLatOpt(5),
			tx_cal_busy(5) 			            	=> txCalBusy_from_gxLatOpt(6),
			tx_clkout(0)               				=> MGT_CLKS_O.tx_wordClk(1),
			tx_clkout(1)               				=> MGT_CLKS_O.tx_wordClk(2),
			tx_clkout(2)               				=> MGT_CLKS_O.tx_wordClk(3),
			tx_clkout(3)               				=> MGT_CLKS_O.tx_wordClk(4),
			tx_clkout(4)               				=> MGT_CLKS_O.tx_wordClk(5),
			tx_clkout(5)               				=> MGT_CLKS_O.tx_wordClk(6),
			tx_coreclkin(0)            				=> MGT_CLKS_I.tx_wordClk(1),
			tx_coreclkin(1)           				=> MGT_CLKS_I.tx_wordClk(2),
			tx_coreclkin(2)           				=> MGT_CLKS_I.tx_wordClk(3),
			tx_coreclkin(3)           				=> MGT_CLKS_I.tx_wordClk(4),
			tx_coreclkin(4)           				=> MGT_CLKS_I.tx_wordClk(5),
			tx_coreclkin(5)           				=> MGT_CLKS_I.tx_wordClk(6),
			tx_digitalreset(0)         				=> txDigitalReset_from_gxRstCtrl(1),
			tx_digitalreset(1)         				=> txDigitalReset_from_gxRstCtrl(2),
			tx_digitalreset(2)         				=> txDigitalReset_from_gxRstCtrl(3),
			tx_digitalreset(3)         				=> txDigitalReset_from_gxRstCtrl(4),
			tx_digitalreset(4)         				=> txDigitalReset_from_gxRstCtrl(5),
			tx_digitalreset(5)         				=> txDigitalReset_from_gxRstCtrl(6),
			tx_parallel_data(39 downto  0)	        => GBTTX_WORD_I(1),
			tx_parallel_data(79 downto  40)	        => GBTTX_WORD_I(2),
			tx_parallel_data(119 downto 80)	        => GBTTX_WORD_I(3),
			tx_parallel_data(159 downto 120)	    => GBTTX_WORD_I(4),
			tx_parallel_data(199 downto 160)	    => GBTTX_WORD_I(5),
			tx_parallel_data(239 downto 200)	    => GBTTX_WORD_I(6),
			tx_polinv(0)               				=> MGT_I.mgtLink(1).tx_polarity,
			tx_polinv(1)               				=> MGT_I.mgtLink(2).tx_polarity,
			tx_polinv(2)               				=> MGT_I.mgtLink(3).tx_polarity,
			tx_polinv(3)               				=> MGT_I.mgtLink(4).tx_polarity,
			tx_polinv(4)               				=> MGT_I.mgtLink(5).tx_polarity,
			tx_polinv(5)               				=> MGT_I.mgtLink(6).tx_polarity,
			tx_serial_data(0)          				=> MGT_O.mgtLink(1).txSerialData,
			tx_serial_data(1)          				=> MGT_O.mgtLink(2).txSerialData,
			tx_serial_data(2)          				=> MGT_O.mgtLink(3).txSerialData,
			tx_serial_data(3)          				=> MGT_O.mgtLink(4).txSerialData,
			tx_serial_data(4)          				=> MGT_O.mgtLink(5).txSerialData,
			tx_serial_data(5)          				=> MGT_O.mgtLink(6).txSerialData,
			unused_tx_parallel_data    => unused_tx_parallel_data_int(527 downto 0),
			unused_rx_parallel_data    => unused_rx_parallel_data_int(527 downto 0),
			reconfig_clk               => MGT_I.mgtLink(1).reconfig_clk,
			reconfig_reset             => MGT_I.mgtLink(1).reconfig_reset,
			reconfig_write             => MGT_I.mgtLink(1).reconfig_write,
			reconfig_read              => MGT_I.mgtLink(1).reconfig_read,
			reconfig_address          => MGT_I.mgtLink(1).reconfig_address(12 downto 0) ,
			reconfig_writedata         => MGT_I.mgtLink(1).reconfig_writedata,
			reconfig_readdata          => MGT_O.mgtLink(1).reconfig_readdata,
			reconfig_waitrequest       => MGT_O.mgtLink(1).reconfig_waitrequest
         );         
   end generate;   
   --=====================================================================================--   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
