--=================================================================================================--
--##################################   Package Information   ######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Jubin MITRA (jmitra@cern.ch), Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros.marin@ieee.org)
--
-- Project Name:          GBT-FPGA                                                                
-- Package Name:          Altera Arria 10 - GBT Banks user setup                                        
--                                                                                                 
-- Language:              VHDL'93                                                            
--                                                                                                   
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.0                                                                
--                                                                                                   
-- Revision:              3.0                                                                      
--
-- Description:           The user can setup the different parameters of the GBT Banks by modifying
--                        this file.
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        16/03/2014   3.0       M. Barros Marin   - First .vhd package definition           
--                        26/05/2015   3.0       J.	Mitra	   	   - Modified for Arria 10           
--
-- Additional Comments:                                                                               
--
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!                                                                                           !!
-- !! * The different parameters of the GBT Bank are set through:                               !!  
-- !!   (Note!! These parameters are vendor specific)                                           !!                    
-- !!                                                                                           !!
-- !!   - The MGT control ports of the GBT Bank module (these ports are listed in the records   !!
-- !!     of the file "<vendor>_<device>_gbt_bank_package.vhd").                                !! 
-- !!     (e.g. xlx_v6_gbt_bank_package.vhd)                                                    !!
-- !!                                                                                           !!  
-- !!   - By modifying the content of the file "<vendor>_<device>_gbt_bank_user_setup.vhd".     !!
-- !!     (e.g. xlx_v6_gbt_bank_user_setup.vhd)                                                 !! 
-- !!                                                                                           !! 
-- !! * The "<vendor>_<device>_gbt_bank_user_setup.vhd" is the only file of the GBT Bank that   !!
-- !!   may be modified by the user. The rest of the files MUST be used as is.                  !!
-- !!                              
-- !! ** This file is modified for automation in QSYS                                           !!
-- !!     ******************************************************************************        !!
-- !!     *******************###       CALCULATION        ###***************************        !!
-- !!     ******************************************************************************        !!

-- !! * <No of Links(3 bits) - '1' > <Tx Coding (GBT Frame, Widebus) (1 bit)> <Rx Coding (GBT Frame, Widebus) (1 bit)> <Tx Latency (std,lat)  (1 bit)> <Rx Latency (std,lat)  (1 bit)> + '1' !!  

-- !!     ******************************************************************************        !!
-- !!     ******************************************************************************        !!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--                                                                                                   
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.vendor_specific_gbt_bank_package.all;

--=================================================================================================--
--##################################   Package Declaration   ######################################--
--=================================================================================================--

package gbt_banks_user_setup is
   
   --================================= GBT Banks parameters ==============================--   
   
   --=====================--
   -- Number of GBT Banks --
   --=====================--
   
   -- Comment:   * On Arria 10 it is possible to implement up to SIX links per GBT Bank.
   --
   --            * If more links than allowed per GBT Bank are needed, then it is 
   --              necessary to instantiate more GBT Banks.        
   
   constant NUM_GBT_BANKS                       : integer := 96;
   
   --=================--
   -- GBT Banks setup --
   --=================--
   
   -- Comment: For more information about the record "GBT_BANKS_USER_SETUP" see the file:
   --          "../gbt_bank/altera_sv/alt_sv_gbt_link_package.vhd"   
   
   constant GBT_BANKS_USER_SETUP : gbt_bank_user_setup_R_A(1 to NUM_GBT_BANKS) := (
      
	--=============================================Link 2 ==============================================================--
	---------------     
	-- GBT Bank 1:
	---------------     
      
      1 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 2:
	---------------     
      
      2 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 3:
	---------------     
      
      3 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 4:
	---------------     
      
      4 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 5:
	---------------     
      
      5 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 6:
	---------------     
      
      6 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 7:
	---------------     
      
      7 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 8:
	---------------     
      
      8 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 9:
	---------------     
      
      9 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 10:
	---------------     
      
      10 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 11:
	---------------     
      
      11 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 12:
	---------------     
      
      12 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 13:
	---------------     
      
      13 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 14:
	---------------     
      
      14 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 15:
	---------------     
      
      15 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 16:
	---------------     
      
      16 => (NUM_LINKS                           => 1,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 17:
	---------------     
      
      17 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 18:
	---------------     
      
      18 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 19:
	---------------     
      
      19 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 20:
	---------------     
      
      20 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 21:
	---------------     
      
      21 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 22:
	---------------     
      
      22 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 23:
	---------------     
      
      23 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 24:
	---------------     
      
      24 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 25:
	---------------     
      
      25 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 26:
	---------------     
      
      26 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 27:
	---------------     
      
      27 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 28:
	---------------     
      
      28 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 29:
	---------------     
      
      29 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 30:
	---------------     
      
      30 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 31:
	---------------     
      
      31 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 32:
	---------------     
      
      32 => (NUM_LINKS                           => 2,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 33:
	---------------     
      
      33 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 34:
	---------------     
      
      34 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 35:
	---------------     
      
      35 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 36:
	---------------     
      
      36 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 37:
	---------------     
      
      37 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 38:
	---------------     
      
      38 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 39:
	---------------     
      
      39 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 40:
	---------------     
      
      40 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 41:
	---------------     
      
      41 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 42:
	---------------     
      
      42 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 43:
	---------------     
      
      43 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 44:
	---------------     
      
      44 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 45:
	---------------     
      
      45 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 46:
	---------------     
      
      46 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 47:
	---------------     
      
      47 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 48:
	---------------     
      
      48 => (NUM_LINKS                           => 3,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 49:
	---------------     
      
      49 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 50:
	---------------     
      
      50 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 51:
	---------------     
      
      51 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 52:
	---------------     
      
      52 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 53:
	---------------     
      
      53 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 54:
	---------------     
      
      54 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 55:
	---------------     
      
      55 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 56:
	---------------     
      
      56 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 57:
	---------------     
      
      57 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 58:
	---------------     
      
      58 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 59:
	---------------     
      
      59 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 60:
	---------------     
      
      60 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 61:
	---------------     
      
      61 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 62:
	---------------     
      
      62 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 63:
	---------------     
      
      63 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 64:
	---------------     
      
      64 => (NUM_LINKS                           => 4,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 65:
	---------------     
      
      65 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 66:
	---------------     
      
      66 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 67:
	---------------     
      
      67 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 68:
	---------------     
      
      68 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 69:
	---------------     
      
      69 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 70:
	---------------     
      
      70 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 71:
	---------------     
      
      71 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 72:
	---------------     
      
      72 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 73:
	---------------     
      
      73 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 74:
	---------------     
      
      74 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 75:
	---------------     
      
      75 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 76:
	---------------     
      
      76 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 77:
	---------------     
      
      77 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 78:
	---------------     
      
      78 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 79:
	---------------     
      
      79 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 80:
	---------------     
      
      80 => (NUM_LINKS                           => 5,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 81:
	---------------     
      
      81 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 82:
	---------------     
      
      82 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 83:
	---------------     
      
      83 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 84:
	---------------     
      
      84 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 85:
	---------------     
      
      85 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 86:
	---------------     
      
      86 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 87:
	---------------     
      
      87 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 88:
	---------------     
      
      88 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 89:
	---------------     
      
      89 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 90:
	---------------     
      
      90 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 91:
	---------------     
      
      91 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 92:
	---------------     
      
      92 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => STANDARD,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 93:
	---------------     
      
      93 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 94:
	---------------     
      
      94 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => GBT_FRAME,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 95:
	---------------     
      
      95 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => GBT_FRAME),		--          * (GBT_FRAME or WIDE_BUS)

	---------------     
	-- GBT Bank 96:
	---------------     
      
      96 => (NUM_LINKS                           => 6,				-- Comment: * 1 to 6                
            --------------------------------------------------------------
            TX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            RX_OPTIMIZATION                     => LATENCY_OPTIMIZED,		--          * (STANDARD or LATENCY_OPTIMIZED)                  
            --------------------------------------------------------------
            TX_ENCODING                         => WIDE_BUS,		--          * (GBT_FRAME or WIDE_BUS)          
            RX_ENCODING                         => WIDE_BUS)		--          * (GBT_FRAME or WIDE_BUS)
	
	--===================================================================================================================--



   );

   --=====================================================================================--      
end gbt_banks_user_setup;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--