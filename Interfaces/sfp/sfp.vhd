library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity sfp is 
port(
	A10_REFCLK_TFC_P      : in  STD_LOGIC;
	A10_SFP_TFC_TX_P      : out STD_LOGIC;
	A10_SFP_TFC_RX_P      : in  STD_LOGIC;
	A10_SFP_TFC_TX_ENABLE : out  STD_LOGIC;
	
	A10_SI5315_BWSEL      : out STD_LOGIC_VECTOR(1 downto 0);  -- SI5315
    A10_SI5315_DBL2_BY    : out STD_LOGIC;
    A10_SI5315_FREQSEL    : out STD_LOGIC_VECTOR(3 downto 0);
    A10_SI5315_FRQTBL     : out STD_LOGIC;
    A10_SI5315_RESET_n    : out STD_LOGIC;
    A10_SI5315_SFOUT      : out STD_LOGIC_VECTOR(1 downto 0)
);	
end entity sfp;

architecture pcie40_top of sfp is

 component ttk is
        port (
            clk_clk                                           : in  std_logic                     := 'X';             -- clk
            refclk_clk                                        : in  std_logic                     := 'X';             -- clk
            refclk_reset_reset_n                              : in  std_logic                     := 'X';             -- reset_n
            reset_reset_n                                     : in  std_logic                     := 'X';             -- reset_n
            timing_adapter_checker_in_data                    : in  std_logic_vector(39 downto 0) := (others => 'X'); -- data
            timing_adapter_generator_out_data                 : out std_logic_vector(39 downto 0);                    -- data
            xcvr_atx_pll_a10_mcgb_rst_mcgb_rst                : in  std_logic                     := 'X';             -- mcgb_rst
            xcvr_native_a10_rx_parallel_data_rx_parallel_data : out std_logic_vector(39 downto 0);                    -- rx_parallel_data
            xcvr_native_a10_rx_serial_data_rx_serial_data     : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- rx_serial_data
            xcvr_native_a10_tx_parallel_data_tx_parallel_data : in  std_logic_vector(39 downto 0) := (others => 'X'); -- tx_parallel_data
            xcvr_native_a10_tx_serial_data_tx_serial_data     : out std_logic_vector(0 downto 0);                     -- tx_serial_data
            xcvr_native_a10_tx_cal_busy_tx_cal_busy           : out std_logic_vector(0 downto 0);                     -- tx_cal_busy
            xcvr_atx_pll_a10_pll_cal_busy_pll_cal_busy        : out std_logic;                                        -- pll_cal_busy
            xcvr_reset_control_tx_cal_busy_tx_cal_busy        : in  std_logic_vector(0 downto 0)  := (others => 'X')  -- tx_cal_busy
        );
 end component ttk;	
 
	signal timing_adapter_checker_in_data					  : std_logic_vector(39 downto 0);
	signal timing_adapter_generator_out_data				  : std_logic_vector(39 downto 0);

	signal      reset_n         : std_logic:='0';
	signal		reset_n_cnt		: std_logic_vector(7 downto 0):=(others=>'0');
	
	signal      tx_cal_busy      : std_logic_vector(0 downto 0);
	signal      pll_cal_busy     : std_logic;
	signal      joint_tx_cal_busy: std_logic_vector(0 downto 0);
begin

	A10_SFP_TFC_TX_ENABLE <= '1';
	
	reset_counter: process(A10_REFCLK_TFC_P)
	begin
		if rising_edge(A10_REFCLK_TFC_P) then
			reset_n_cnt <= reset_n_cnt + '1';
			if (reset_n_cnt >= x"F0") then
              reset_n <= '1';
            end if;
		end if;		
	end process;
	
	joint_tx_cal_busy(0) <= tx_cal_busy(0) or pll_cal_busy;
	
    u0 : component ttk
        port map (
            clk_clk                                           => A10_REFCLK_TFC_P,                                          				 --                              clk.clk
            refclk_clk                                        => A10_REFCLK_TFC_P,                                      		 --                           refclk.clk
            refclk_reset_reset_n                              => reset_n,                             							 --                     refclk_reset.reset_n
            reset_reset_n                                     => reset_n,                                    					 --                            reset.reset_n
            timing_adapter_checker_in_data                    => timing_adapter_checker_in_data,           				         --        timing_adapter_checker_in.data
            timing_adapter_generator_out_data                 => timing_adapter_generator_out_data,       				         --     timing_adapter_generator_out.data
            xcvr_atx_pll_a10_mcgb_rst_mcgb_rst                => reset_n,               										 --        xcvr_atx_pll_a10_mcgb_rst.mcgb_rst
            xcvr_native_a10_rx_parallel_data_rx_parallel_data => timing_adapter_checker_in_data,								 -- xcvr_native_a10_rx_parallel_data.rx_parallel_data
            xcvr_native_a10_rx_serial_data_rx_serial_data(0)  => A10_SFP_TFC_RX_P, 												 --   xcvr_native_a10_rx_serial_data.rx_serial_data
            xcvr_native_a10_tx_parallel_data_tx_parallel_data => timing_adapter_generator_out_data,								 -- xcvr_native_a10_tx_parallel_data.tx_parallel_data
            xcvr_native_a10_tx_serial_data_tx_serial_data(0)  => A10_SFP_TFC_TX_P,											     --   xcvr_native_a10_tx_serial_data.tx_serial_data
			xcvr_native_a10_tx_cal_busy_tx_cal_busy           => tx_cal_busy,       						   					 --      xcvr_native_a10_tx_cal_busy.tx_cal_busy
            xcvr_atx_pll_a10_pll_cal_busy_pll_cal_busy        => pll_cal_busy,       											 --    xcvr_atx_pll_a10_pll_cal_busy.pll_cal_busy
            xcvr_reset_control_tx_cal_busy_tx_cal_busy        => joint_tx_cal_busy        										 --   xcvr_reset_control_tx_cal_busy.tx_cal_busy
        );




-----------------------------------------------------------------------------------------------------------
--	SI5315 configuration
-----------------------------------------------------------------------------------------------------------		
	A10_SI5315_BWSEL           <= "11";
	A10_SI5315_DBL2_BY         <= '0';
	A10_SI5315_FREQSEL         <= "ZZ10";-- MMHL : 644 MHz
	A10_SI5315_FRQTBL          <= '1';
	A10_SI5315_RESET_n         <= '1';
	A10_SI5315_SFOUT           <= "1Z";

end architecture pcie40_top;
