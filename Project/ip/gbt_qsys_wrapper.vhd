--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               VECC (INDIA)                                                         
-- Engineer:              Jubin MITRA (jubin.mitra@cern.ch) (jm61288@gmail.com)
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           Altera Arria 10 - GBT QSYS Wrapper                                         
--                                                                                                 
-- Language:              VHDL'08                                                                  
--                                                                                                   
-- Target Device:         Altera Arria 10                                                        
-- Tool version:          Quartus II 15.0                                                               
--                                                                                                   
-- Version:               1.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        22/06/2015   1.0       Jubin Mitra	   - GBT QSYS Wrapper for Arria 10 of GBTv3.1.1 migrated design of Stratix V
--            
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Altera devices library:
library altera; 
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;   
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;



--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity gbt_qsys_wrapper is 
   generic(
			NUMBER_OF_LINKS                                       : integer := 6;
			TX_CODING                                             : integer := 0;
			RX_CODING                                             : integer := 0;
			TX_LATENCY                                            : integer := 1;
			RX_LATENCY                                            : integer := 0
   );
   port ( 
      --====================================================--
	  -- QSYS port defination style
	  --<interface type prefix>_<interface name>_<signal type>
	  --====================================================--
	  
      --======================--
      --   AVALON MM Slave    --
      --======================-- 
		rsi_monitor_reset_reset                                        : in std_logic;
		csi_monitor_clk_clk                                            : in std_logic;
		avs_monitor_read                                               : in std_logic;
		avs_monitor_readdata                                           : out std_logic_vector(31 downto 0);
		avs_monitor_write                                              : in std_logic;
		avs_monitor_writedata                                          : in std_logic_vector(31 downto 0);
		avs_monitor_address                                            : in std_logic_vector( 7 downto 0);
      --===============================--
      -- AVALON Stream Source (output) --
      --======================-========--		
		rsi_LINK_reset              	                          	   : in std_logic;
		
		aso_LINK1_O_valid                                              : out std_logic;
		aso_LINK1_O_data                                               : out std_logic_vector(83+RX_CODING*32 downto 0);	
		aso_LINK1_O_ready                                              : in std_logic;

		aso_LINK2_O_valid                                              : out std_logic;
		aso_LINK2_O_data                                               : out std_logic_vector(83+RX_CODING*32 downto 0);	
		aso_LINK2_O_ready                                              : in std_logic;		

		aso_LINK3_O_valid                                              : out std_logic;
		aso_LINK3_O_data                                               : out std_logic_vector(83+RX_CODING*32 downto 0);	
		aso_LINK3_O_ready                                              : in std_logic;
		
		aso_LINK4_O_valid                                              : out std_logic;
		aso_LINK4_O_data                                               : out std_logic_vector(83+RX_CODING*32 downto 0);	
		aso_LINK4_O_ready                                              : in std_logic;

		aso_LINK5_O_valid                                              : out std_logic;
		aso_LINK5_O_data                                               : out std_logic_vector(83+RX_CODING*32 downto 0);	
		aso_LINK5_O_ready                                              : in std_logic;

		aso_LINK6_O_valid                                              : out std_logic;
		aso_LINK6_O_data                                               : out std_logic_vector(83+RX_CODING*32 downto 0);	
		aso_LINK6_O_ready                                              : in std_logic;
		
	  --============================--
      -- AVALON Stream Sink (input) --
      --============================--			
		asi_LINK1_I_valid                                              : in std_logic;
		asi_LINK1_I_data                                               : in std_logic_vector(83+TX_CODING*32 downto 0);	
		asi_LINK1_I_ready                                              : out std_logic;
	
		asi_LINK2_I_valid                                              : in std_logic;
		asi_LINK2_I_data                                               : in std_logic_vector(83+TX_CODING*32 downto 0);	
		asi_LINK2_I_ready                                              : out std_logic;
		
		asi_LINK3_I_valid                                              : in std_logic;
		asi_LINK3_I_data                                               : in std_logic_vector(83+TX_CODING*32 downto 0);	
		asi_LINK3_I_ready                                              : out std_logic;

		asi_LINK4_I_valid                                              : in std_logic;
		asi_LINK4_I_data                                               : in std_logic_vector(83+TX_CODING*32 downto 0);	
		asi_LINK4_I_ready                                              : out std_logic;

		asi_LINK5_I_valid                                              : in std_logic;
		asi_LINK5_I_data                                               : in std_logic_vector(83+TX_CODING*32 downto 0);	
		asi_LINK5_I_ready                                              : out std_logic;

		asi_LINK6_I_valid                                              : in std_logic;
		asi_LINK6_I_data                                               : in std_logic_vector(83+TX_CODING*32 downto 0);	
		asi_LINK6_I_ready                                              : out std_logic;		

	  --===============--
      -- Serial Lanes  --
      --===============--  
		coe_tx_serial_data_LINK1_O									   : out std_logic;
		coe_tx_serial_data_LINK2_O									   : out std_logic;
		coe_tx_serial_data_LINK3_O									   : out std_logic;
		coe_tx_serial_data_LINK4_O									   : out std_logic;
		coe_tx_serial_data_LINK5_O									   : out std_logic;
		coe_tx_serial_data_LINK6_O									   : out std_logic;
		
		coe_rx_serial_data_LINK1_I									   : in  std_logic;
		coe_rx_serial_data_LINK2_I									   : in  std_logic;
		coe_rx_serial_data_LINK3_I									   : in  std_logic;
		coe_rx_serial_data_LINK4_I									   : in  std_logic;
		coe_rx_serial_data_LINK5_I									   : in  std_logic;
		coe_rx_serial_data_LINK6_I									   : in  std_logic;
		
		
	  --===============--
      -- General reset --
      --===============--      
     
      rsi_SYS_RESET_N_I                                                 : in  std_logic; 
	  
	  --================--
      -- GBT BANK RESET --
      --================--
	  rsi_MGT_TX_RESET_I												: in std_logic;
	  rsi_MGT_RX_RESET_I                                                : in std_logic;
	  rsi_GBT_TX_RESET_I												: in std_logic;
	  rsi_GBT_RX_RESET_I												: in std_logic;

	  --=======================--
      -- MGT REFERENCE CLK     --
      --=======================--		  
	  csi_MGT_REF_clk													: in std_logic;

	  --=========================--
      --   FRAME CLK (40 MHz)    --
      --=========================--	  
	  csi_TX_FRAME_CLK_LINK_1_I_clk										: in std_logic;
	  csi_TX_FRAME_CLK_LINK_2_I_clk										: in std_logic;
	  csi_TX_FRAME_CLK_LINK_3_I_clk										: in std_logic;
	  csi_TX_FRAME_CLK_LINK_4_I_clk										: in std_logic;
	  csi_TX_FRAME_CLK_LINK_5_I_clk										: in std_logic;
	  csi_TX_FRAME_CLK_LINK_6_I_clk										: in std_logic;
	  coe_TX_FRAME_CLK_PLL_LOCKED                                       : in std_logic;
	  
	  csi_RX_FRAME_CLK_LINK_1_I_clk										: in std_logic;
	  csi_RX_FRAME_CLK_LINK_2_I_clk										: in std_logic;
	  csi_RX_FRAME_CLK_LINK_3_I_clk										: in std_logic;
	  csi_RX_FRAME_CLK_LINK_4_I_clk										: in std_logic;
	  csi_RX_FRAME_CLK_LINK_5_I_clk										: in std_logic;
	  csi_RX_FRAME_CLK_LINK_6_I_clk										: in std_logic;
	  coe_RX_FRAME_CLK_PLL_LOCKED                                       : in std_logic;

	  --================--
      --   WORD CLK     --
      --================--	  	  
	  cso_TX_WORD_CLK_LINK_1_O_clk										: out std_logic;
	  cso_TX_WORD_CLK_LINK_2_O_clk										: out std_logic;
	  cso_TX_WORD_CLK_LINK_3_O_clk										: out std_logic;
	  cso_TX_WORD_CLK_LINK_4_O_clk										: out std_logic;
	  cso_TX_WORD_CLK_LINK_5_O_clk										: out std_logic;
	  cso_TX_WORD_CLK_LINK_6_O_clk										: out std_logic;

	  cso_RX_WORD_CLK_LINK_1_O_clk										: out std_logic;
	  cso_RX_WORD_CLK_LINK_2_O_clk										: out std_logic;
	  cso_RX_WORD_CLK_LINK_3_O_clk										: out std_logic;
	  cso_RX_WORD_CLK_LINK_4_O_clk										: out std_logic;
	  cso_RX_WORD_CLK_LINK_5_O_clk										: out std_logic;
	  cso_RX_WORD_CLK_LINK_6_O_clk										: out std_logic;
	  
	  --================--
      --   MGT CLK      --
      --================--	
	  csi_ATX_PLL_BONDED_clk_clk                                        : in std_logic_vector(5 downto 0);
	  coe_ATX_PLL_POWERDOWN                                             : in std_logic;
	  coe_ATX_PLL_LOCKED                                                : in std_logic;
	  coe_ATX_PLL_CAL_BUSY                                              : in std_logic;
	  
	  --=================================--
      -- AVALON MM SLAVE RECONFIGURATION --
      --=================================--
	  csi_reconfig_clk_clk						                  		: in  std_logic_vector( 0 downto 0);
	  rsi_reconfig_reset_reset				                   			: in  std_logic_vector( 0 downto 0);
	  avs_reconfig_write				                    			: in  std_logic_vector( 0 downto 0);
	  avs_reconfig_read					                    			: in  std_logic_vector( 0 downto 0);
	  avs_reconfig_address				                    			: in  std_logic_vector(12 downto 0);
	  avs_reconfig_writedata			                    			: in  std_logic_vector(31 downto 0);
	  avs_reconfig_readdata			                        			: out std_logic_vector(31 downto 0);
	  avs_reconfig_waitrequest			                    			: out std_logic_vector(0 downto 0)	  
      );
end gbt_qsys_wrapper;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of gbt_qsys_wrapper is 

--================================ Constant Declarations ================================--   

	-- !!     *************************************************************************************************************************************************************************         !!
	-- !!     ********************************************************###      GBT BANK SELECTION CALCULATION        ###****************************************************************        !!
	-- !!     **************************************************************************************************************************************************************************        !!

	-- !!  <No of Links(3 bits) - '1' >  <Tx Latency (std,lat)  (1 bit)> <Rx Latency (std,lat)  (1 bit)><Tx Coding (GBT Frame, Widebus) (1 bit)> <Rx Coding (GBT Frame, Widebus) (1 bit)> + '1' !!  
	----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     constant GBT_BANK_ID                                   : integer := (NUMBER_OF_LINKS-1)*16 + TX_LATENCY*8 + RX_LATENCY*4 + TX_CODING*1 + RX_CODING*2 + 1;
	 
	 
--================================ Signal Declarations ================================--   

	-------------
	-- GBT BANK :
	-------------
	signal to_gbtBank_clks                                 : gbtBankClks_i_R;                          
	signal from_gbtBank_clks                               : gbtBankClks_o_R;
	----------------------------------------------------        
	signal to_gbtBank_gbtTx                                : gbtTx_i_R_A(1 to NUMBER_OF_LINKS); 
	signal from_gbtBank_gbtTx                              : gbtTx_o_R_A(1 to NUMBER_OF_LINKS); 
	----------------------------------------------------        
	signal to_gbtBank_mgt                                  : mgt_i_R;
	signal from_gbtBank_mgt                                : mgt_o_R; 
	----------------------------------------------------        
	signal to_gbtBank_gbtRx                                : gbtRx_i_R_A(1 to NUMBER_OF_LINKS); 
	signal from_gbtBank_gbtRx                              : gbtRx_o_R_A(1 to NUMBER_OF_LINKS);

	--------------------
	-- MONITOR SIGNALS :
	--------------------
	-- PLL STATUS
	signal monitor_txFrameClk_pll_locked_r				   : std_logic; 
	signal monitor_rxFrameClk_pll_locked_r				   : std_logic; 
	signal monitor_atx_pll_locked_r						   : std_logic;
	signal monitor_atx_pll_cal_busy_r					   : std_logic;
	
	-- GBTBANK INFORMATION
	signal monitor_number_of_links_r					   : std_logic_vector(2 downto 0); 
	signal monitor_tx_coding_r							   : std_logic_vector(0 downto 0); 
	signal monitor_rx_coding_r							   : std_logic_vector(0 downto 0); 
	signal monitor_tx_latency_r							   : std_logic_vector(0 downto 0); 
	signal monitor_rx_latency_r							   : std_logic_vector(0 downto 0); 
	signal monitor_gbt_bank_id_r						   : std_logic_vector(6 downto 0); 

	--  MGT AND GBT RX STATUS
	signal monitor_gbtBank_mgt_ready_r					   : std_logic_vector(1 to 6);
	signal monitor_gbtBank_rx_wordclk_ready_r			   : std_logic_vector(1 to 6);
	signal monitor_rx_bislip_number_r					   : rxBitSlipNbr_mxnbit_A(1 to 6);
	signal monitor_gbtBank_rx_ready_r					   : std_logic_vector(1 to 6);
	
	--  CONTROL ASSIGNMENTS:
    signal monitor_enable_gbtbank_loopback_w			   : std_logic_vector(1 to 6):=(others=>'0');
	signal monitor_mgt_tx_polarity_w					   : std_logic_vector(1 to 6):=(others=>'0');
	signal monitor_mgt_rx_polarity_w					   : std_logic_vector(1 to 6):=(others=>'1');
	
	--------------------------------
	-- Rx WORD CLOCK BUFFER SIGNAL :
	--------------------------------
	signal  gbtBank_rx_wordClk_mux						   : std_logic_vector(1 to NUMBER_OF_LINKS);
	
   --==============================================================================================--   

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

		--============================================--
		--   MONITOR AND CONTROL SIGNAL ASSIGNMENT    --
		--============================================--
		
		----------------------
		--   READ SIGNALS   --
		----------------------
		
		-- GBTBANK INFORMATION
		monitor_number_of_links_r 												<= std_logic_vector(to_unsigned(NUMBER_OF_LINKS, monitor_number_of_links_r'length));
		monitor_tx_coding_r														<= std_logic_vector(to_unsigned(TX_CODING,monitor_tx_coding_r'length));
		monitor_rx_coding_r														<= std_logic_vector(to_unsigned(RX_CODING,monitor_rx_coding_r'length));
		monitor_tx_latency_r													<= std_logic_vector(to_unsigned(TX_LATENCY,monitor_tx_latency_r'length));
		monitor_rx_latency_r													<= std_logic_vector(to_unsigned(RX_LATENCY,monitor_rx_latency_r'length));
		monitor_gbt_bank_id_r													<= std_logic_vector(to_unsigned(GBT_BANK_ID,monitor_gbt_bank_id_r'length));
	
		--  MGT AND GBT RX STATUS
		gen_monitor_status_signals :
		for i in 1 to NUMBER_OF_LINKS generate
			monitor_gbtBank_mgt_ready_r(i)										<= from_gbtBank_mgt.mgtLink(i).ready;
			monitor_gbtBank_rx_wordclk_ready_r(i)								<= from_gbtBank_mgt.mgtLink(i).rxWordClkReady;
			monitor_rx_bislip_number_r(i)										<= from_gbtBank_gbtRx(i).bitSlipNbr;
			monitor_gbtBank_rx_ready_r(i)										<= from_gbtBank_gbtRx(i).ready;
		end generate gen_monitor_status_signals;
		
		---------------------------------------------------
		--   WRITE SIGNALS  & CONTROL SIGNALS ASSIGNMENT --
		---------------------------------------------------

		gen_control_signals :
		for i in 1 to NUMBER_OF_LINKS generate
			-- Comment: * The manual RX_WORDCLK phase alignment control of the MGT(GT) transceivers is not used in this 
			--            reference design (auto RX_WORDCLK phase alignment is used instead).		
			to_gbtBank_mgt.mgtLink(i).rxBitSlip_enable          					<= '1'; 
			to_gbtBank_mgt.mgtLink(i).rxBitSlip_ctrl            					<= '0'; 
			to_gbtBank_mgt.mgtLink(i).rxBitSlip_nbr            						<= "000000";
			to_gbtBank_mgt.mgtLink(i).rxBitSlip_run             					<= '0';
			
			-- Enable serial loopback
			to_gbtBank_mgt.mgtLink(i).loopBack 										<= monitor_enable_gbtbank_loopback_w(i);
			
			-- Polarity inversion in tx side is needed for PCI40	
			to_gbtBank_mgt.mgtLink(i).tx_polarity									<= monitor_mgt_tx_polarity_w(i); 
			to_gbtBank_mgt.mgtLink(i).rx_polarity									<= monitor_mgt_rx_polarity_w(i) and (not monitor_enable_gbtbank_loopback_w(i)); 
		end generate gen_control_signals;
--======================================================================================================================================================================--

		--==============================--
		--   MONITOR AVALON MM Slave    --
		--==============================-- 
		
		----------
		-- READ --
		----------
		process(rsi_monitor_reset_reset,csi_monitor_clk_clk,avs_monitor_read) 
		   begin
			if rsi_monitor_reset_reset = '1' then
				avs_monitor_readdata <= (others=>'0');
			else
				if rising_edge(csi_monitor_clk_clk) then
					if avs_monitor_read='1' then
						case avs_monitor_address is
							
							--PLL STATUS
							when x"00" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_txFrameClk_pll_locked_r;
							when x"01" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_rxFrameClk_pll_locked_r;
							when x"02" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_atx_pll_locked_r;
							when x"03"  => avs_monitor_readdata <=  x"0000000" & "000" & monitor_atx_pll_cal_busy_r;
							-- GBTBANK INFORMATION
							when x"04" 	=> avs_monitor_readdata <=  x"0000000" & "0"   & monitor_number_of_links_r;
							when x"05" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_tx_coding_r;
							when x"06" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_rx_coding_r;
							when x"07" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_tx_latency_r;
							when x"08" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_rx_latency_r;
							when x"09" 	=> avs_monitor_readdata <=  x"000000"  & "0"   & monitor_gbt_bank_id_r;
							
							--  MGT AND GBT RX STATUS
							-- LINK 1
							when x"10" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_mgt_ready_r(1);
							when x"11" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_wordclk_ready_r(1);
							when x"12" 	=> avs_monitor_readdata <=  x"000000"  & "00"  & monitor_rx_bislip_number_r(1);
							when x"13" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_ready_r(1);
							when x"14" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_enable_gbtbank_loopback_w(1);
							when x"15" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_tx_polarity_w(1);
							when x"16" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_rx_polarity_w(1);
							
							-- LINK 2
							when x"20" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_mgt_ready_r(2);
							when x"21" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_wordclk_ready_r(2);
							when x"22" 	=> avs_monitor_readdata <=  x"000000"  & "00"  & monitor_rx_bislip_number_r(2);
							when x"23" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_ready_r(2);
							when x"24" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_enable_gbtbank_loopback_w(2);
							when x"25" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_tx_polarity_w(2);
							when x"26" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_rx_polarity_w(2);
							
							-- LINK 3
							when x"30" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_mgt_ready_r(3);
							when x"31" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_wordclk_ready_r(3);
							when x"32" 	=> avs_monitor_readdata <=  x"000000"  & "00"  & monitor_rx_bislip_number_r(3);
							when x"33" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_ready_r(3);
							when x"34" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_enable_gbtbank_loopback_w(3);
							when x"35" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_tx_polarity_w(3);
							when x"36" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_rx_polarity_w(3);
							
							-- LINK 4
							when x"40" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_mgt_ready_r(4);
							when x"41" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_wordclk_ready_r(4);
							when x"42" 	=> avs_monitor_readdata <=  x"000000"  & "00"  & monitor_rx_bislip_number_r(4);
							when x"43" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_ready_r(4);
							when x"44" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_enable_gbtbank_loopback_w(4);
							when x"45" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_tx_polarity_w(4);
							when x"46" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_rx_polarity_w(4);
							
							-- LINK 5
							when x"50" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_mgt_ready_r(5);
							when x"51" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_wordclk_ready_r(5);
							when x"52" 	=> avs_monitor_readdata <=  x"000000"  & "00"  & monitor_rx_bislip_number_r(5);
							when x"53" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_ready_r(5);
							when x"54" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_enable_gbtbank_loopback_w(5);
							when x"55" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_tx_polarity_w(5);
							when x"56" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_rx_polarity_w(5);
							
							-- LINK 6
							when x"60" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_mgt_ready_r(6);
							when x"61" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_wordclk_ready_r(6);
							when x"62" 	=> avs_monitor_readdata <=  x"000000"  & "00"  & monitor_rx_bislip_number_r(6);
							when x"63" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_gbtBank_rx_ready_r(6);
							when x"64" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_enable_gbtbank_loopback_w(6);
							when x"65" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_tx_polarity_w(6);
							when x"66" 	=> avs_monitor_readdata <=  x"0000000" & "000" & monitor_mgt_rx_polarity_w(6);
							
							when others => avs_monitor_readdata <= (others => '0');
						end case;
					end if;
				end if;
			end if;
		end process;
   
		
		-----------
		-- WRITE --
		-----------
		process(rsi_monitor_reset_reset,csi_monitor_clk_clk,avs_monitor_write) 
		   begin
			if rsi_monitor_reset_reset = '1' then
				monitor_enable_gbtbank_loopback_w			   <=(others=>'0');
				monitor_mgt_tx_polarity_w					   <=(others=>'0');
				monitor_mgt_rx_polarity_w					   <=(others=>'1');
			else
				if rising_edge(csi_monitor_clk_clk) then
					if avs_monitor_write='1' then
						case avs_monitor_address is
							-- LINK 1
							when x"10" 	=> monitor_enable_gbtbank_loopback_w(1) 		<= avs_monitor_writedata(0);
							when x"11" 	=> monitor_mgt_tx_polarity_w(1)					<= avs_monitor_writedata(0);
							when x"12" 	=> monitor_mgt_rx_polarity_w(1) 				<= avs_monitor_writedata(0);

							-- LINK 2
							when x"20" 	=> monitor_enable_gbtbank_loopback_w(2) 		<= avs_monitor_writedata(0);
							when x"21" 	=> monitor_mgt_tx_polarity_w(2)					<= avs_monitor_writedata(0);
							when x"22" 	=> monitor_mgt_rx_polarity_w(2) 				<= avs_monitor_writedata(0);							

							-- LINK 3
							when x"30" 	=> monitor_enable_gbtbank_loopback_w(3) 		<= avs_monitor_writedata(0);
							when x"31" 	=> monitor_mgt_tx_polarity_w(3)					<= avs_monitor_writedata(0);
							when x"32" 	=> monitor_mgt_rx_polarity_w(3) 				<= avs_monitor_writedata(0);

							-- LINK 4
							when x"40" 	=> monitor_enable_gbtbank_loopback_w(4) 		<= avs_monitor_writedata(0);
							when x"41" 	=> monitor_mgt_tx_polarity_w(4)					<= avs_monitor_writedata(0);
							when x"42" 	=> monitor_mgt_rx_polarity_w(4) 				<= avs_monitor_writedata(0);

							-- LINK 5
							when x"50" 	=> monitor_enable_gbtbank_loopback_w(5) 		<= avs_monitor_writedata(0);
							when x"51" 	=> monitor_mgt_tx_polarity_w(5)					<= avs_monitor_writedata(0);
							when x"52" 	=> monitor_mgt_rx_polarity_w(5) 				<= avs_monitor_writedata(0);

							-- LINK 6
							when x"60" 	=> monitor_enable_gbtbank_loopback_w(6) 		<= avs_monitor_writedata(0);
							when x"61" 	=> monitor_mgt_tx_polarity_w(6)					<= avs_monitor_writedata(0);
							when x"62" 	=> monitor_mgt_rx_polarity_w(6) 				<= avs_monitor_writedata(0);
							
							when others => monitor_mgt_rx_polarity_w					<=(others=>'0');
						end case;
					end if;
				end if;
			end if;
		end process;   		
		--=====================--   
		-- AVALON STREAM SOURCE--   
		--=====================--
		--COMMENT: The role of ready signal is dummy in this design.
		
		link_x84:
		if TX_CODING = 0 generate
		
			-- LINK1
			gen_link1_stream_source:
			process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_1_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					asi_LINK1_I_ready 														<= '0';
					to_gbtBank_gbtTx(1).isDataSel											<= '0';
					to_gbtBank_gbtTx(1).data												<= (others=>'0');
					to_gbtBank_gbtTx(1).extraData_wideBus 									<= (others=>'0');
				elsif rising_edge(csi_TX_FRAME_CLK_LINK_1_I_clk) then
					asi_LINK1_I_ready 														<= '1';
					to_gbtBank_gbtTx(1).isDataSel											<= asi_LINK1_I_valid;
					to_gbtBank_gbtTx(1).data                            					<= asi_LINK1_I_data(83 downto 0); 
				end if;
			end process;
			
			-- LINK2
			gen_link2_stream_source:
			if NUMBER_OF_LINKS > 1 generate
				process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_2_I_clk)
				begin
					if rsi_LINK_reset = '1' then
						asi_LINK2_I_ready 														<= '0';
						to_gbtBank_gbtTx(2).isDataSel											<= '0';
						to_gbtBank_gbtTx(2).data												<= (others=>'0');
						to_gbtBank_gbtTx(2).extraData_wideBus 									<= (others=>'0');
					elsif rising_edge(csi_TX_FRAME_CLK_LINK_2_I_clk) then
						asi_LINK2_I_ready 														<= '1';
						to_gbtBank_gbtTx(2).isDataSel											<= asi_LINK2_I_valid;
						to_gbtBank_gbtTx(2).data                            					<= asi_LINK2_I_data(83 downto 0); 
					end if;
				end process;
			end generate gen_link2_stream_source;
			
			-- LINK3
			gen_link3_stream_source:
			if NUMBER_OF_LINKS > 2 generate
				process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_3_I_clk)
				begin
					if rsi_LINK_reset = '1' then
						asi_LINK3_I_ready 														<= '0';
						to_gbtBank_gbtTx(3).isDataSel											<= '0';
						to_gbtBank_gbtTx(3).data												<= (others=>'0');
						to_gbtBank_gbtTx(3).extraData_wideBus 									<= (others=>'0');
					elsif rising_edge(csi_TX_FRAME_CLK_LINK_3_I_clk) then
						asi_LINK3_I_ready 														<= '1';
						to_gbtBank_gbtTx(3).isDataSel											<= asi_LINK3_I_valid;
						to_gbtBank_gbtTx(3).data                            					<= asi_LINK3_I_data(83 downto 0); 
					end if;
				end process;
			end generate gen_link3_stream_source;
			
			-- LINK4
			gen_link4_stream_source:
			if NUMBER_OF_LINKS > 3 generate
				process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_4_I_clk)
				begin
					if rsi_LINK_reset = '1' then
						asi_LINK4_I_ready 														<= '0';
						to_gbtBank_gbtTx(4).isDataSel											<= '0';
						to_gbtBank_gbtTx(4).data												<= (others=>'0');
						to_gbtBank_gbtTx(4).extraData_wideBus 									<= (others=>'0');
					elsif rising_edge(csi_TX_FRAME_CLK_LINK_4_I_clk) then
						asi_LINK4_I_ready 														<= '1';
						to_gbtBank_gbtTx(4).isDataSel											<= asi_LINK4_I_valid;
						to_gbtBank_gbtTx(4).data                            					<= asi_LINK4_I_data(83 downto 0); 
					end if;
				end process;
			end generate gen_link4_stream_source;
			
			-- LINK5
			gen_link5_stream_source:
			if NUMBER_OF_LINKS > 4 generate
				process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_5_I_clk)
				begin
					if rsi_LINK_reset = '1' then
						asi_LINK5_I_ready 														<= '0';
						to_gbtBank_gbtTx(5).isDataSel											<= '0';
						to_gbtBank_gbtTx(5).data												<= (others=>'0');
						to_gbtBank_gbtTx(5).extraData_wideBus 									<= (others=>'0');
					elsif rising_edge(csi_TX_FRAME_CLK_LINK_5_I_clk) then
						asi_LINK5_I_ready 														<= '1';
						to_gbtBank_gbtTx(5).isDataSel											<= asi_LINK5_I_valid;
						to_gbtBank_gbtTx(5).data                            					<= asi_LINK5_I_data(83 downto 0); 
					end if;
				end process;
			end generate gen_link5_stream_source;
			
			-- LINK6
			gen_link6_stream_source:
			if NUMBER_OF_LINKS > 5 generate
				process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_6_I_clk)
				begin
					if rsi_LINK_reset = '1' then
						asi_LINK6_I_ready 														<= '0';
						to_gbtBank_gbtTx(6).isDataSel											<= '0';
						to_gbtBank_gbtTx(6).data												<= (others=>'0');
						to_gbtBank_gbtTx(6).extraData_wideBus 									<= (others=>'0');
					elsif rising_edge(csi_TX_FRAME_CLK_LINK_6_I_clk) then
						asi_LINK6_I_ready 														<= '1';
						to_gbtBank_gbtTx(6).isDataSel											<= asi_LINK6_I_valid;
						to_gbtBank_gbtTx(6).data                            					<= asi_LINK6_I_data(83 downto 0); 
					end if;
				end process;
			end generate gen_link6_stream_source;				
		
	end generate link_x84;
	
	link_x116:
	if TX_CODING = 1 generate
		
		-- LINK1
		gen_link1_stream_source:
		process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_1_I_clk)
		begin
			if rsi_LINK_reset = '1' then
				asi_LINK1_I_ready 														<= '0';
				to_gbtBank_gbtTx(1).isDataSel											<= '0';
				to_gbtBank_gbtTx(1).data												<= (others=>'0');
				to_gbtBank_gbtTx(1).extraData_wideBus 									<= (others=>'0');
			elsif rising_edge(csi_TX_FRAME_CLK_LINK_1_I_clk) then
				asi_LINK1_I_ready 														<= '1';
				to_gbtBank_gbtTx(1).isDataSel											<= asi_LINK1_I_valid;
				to_gbtBank_gbtTx(1).data                            					<= asi_LINK1_I_data(83 downto 0); 
				to_gbtBank_gbtTx(1).extraData_wideBus               					<= asi_LINK1_I_data(115 downto 84);
			end if;
		end process;
		
		-- LINK2
		gen_link2_stream_source:
		if NUMBER_OF_LINKS > 1 generate
			process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_2_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					asi_LINK2_I_ready 														<= '0';
					to_gbtBank_gbtTx(2).isDataSel											<= '0';
					to_gbtBank_gbtTx(2).data												<= (others=>'0');
					to_gbtBank_gbtTx(2).extraData_wideBus 									<= (others=>'0');
				elsif rising_edge(csi_TX_FRAME_CLK_LINK_2_I_clk) then
					asi_LINK2_I_ready 														<= '1';
					to_gbtBank_gbtTx(2).isDataSel											<= asi_LINK2_I_valid;
					to_gbtBank_gbtTx(2).data                            					<= asi_LINK2_I_data(83 downto 0); 
					to_gbtBank_gbtTx(2).extraData_wideBus               					<= asi_LINK2_I_data(115 downto 84);
				end if;
			end process;
		end generate gen_link2_stream_source;
		
		-- LINK3
		gen_link3_stream_source:
		if NUMBER_OF_LINKS > 2 generate
			process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_3_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					asi_LINK3_I_ready 														<= '0';
					to_gbtBank_gbtTx(3).isDataSel											<= '0';
					to_gbtBank_gbtTx(3).data												<= (others=>'0');
					to_gbtBank_gbtTx(3).extraData_wideBus 									<= (others=>'0');
				elsif rising_edge(csi_TX_FRAME_CLK_LINK_3_I_clk) then
					asi_LINK3_I_ready 														<= '1';
					to_gbtBank_gbtTx(3).isDataSel											<= asi_LINK3_I_valid;
					to_gbtBank_gbtTx(3).data                            					<= asi_LINK3_I_data(83 downto 0); 
					to_gbtBank_gbtTx(3).extraData_wideBus               					<= asi_LINK3_I_data(115 downto 84);
				end if;
			end process;
		end generate gen_link3_stream_source;
		
		-- LINK4
		gen_link4_stream_source:
		if NUMBER_OF_LINKS > 3 generate
			process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_4_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					asi_LINK4_I_ready 														<= '0';
					to_gbtBank_gbtTx(4).isDataSel											<= '0';
					to_gbtBank_gbtTx(4).data												<= (others=>'0');
					to_gbtBank_gbtTx(4).extraData_wideBus 									<= (others=>'0');
				elsif rising_edge(csi_TX_FRAME_CLK_LINK_4_I_clk) then
					asi_LINK4_I_ready 														<= '1';
					to_gbtBank_gbtTx(4).isDataSel											<= asi_LINK4_I_valid;
					to_gbtBank_gbtTx(4).data                            					<= asi_LINK4_I_data(83 downto 0); 
					to_gbtBank_gbtTx(4).extraData_wideBus               					<= asi_LINK4_I_data(115 downto 84);
				end if;
			end process;
		end generate gen_link4_stream_source;
		
		-- LINK5
		gen_link5_stream_source:
		if NUMBER_OF_LINKS > 4 generate
			process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_5_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					asi_LINK5_I_ready 														<= '0';
					to_gbtBank_gbtTx(5).isDataSel											<= '0';
					to_gbtBank_gbtTx(5).data												<= (others=>'0');
					to_gbtBank_gbtTx(5).extraData_wideBus 									<= (others=>'0');
				elsif rising_edge(csi_TX_FRAME_CLK_LINK_5_I_clk) then
					asi_LINK5_I_ready 														<= '1';
					to_gbtBank_gbtTx(5).isDataSel											<= asi_LINK5_I_valid;
					to_gbtBank_gbtTx(5).data                            					<= asi_LINK5_I_data(83 downto 0); 
					to_gbtBank_gbtTx(5).extraData_wideBus               					<= asi_LINK5_I_data(115 downto 84);
				end if;
			end process;
		end generate gen_link5_stream_source;
		
		-- LINK6
		gen_link6_stream_source:
		if NUMBER_OF_LINKS > 5 generate
			process(rsi_LINK_reset,csi_TX_FRAME_CLK_LINK_6_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					asi_LINK6_I_ready 														<= '0';
					to_gbtBank_gbtTx(6).isDataSel											<= '0';
					to_gbtBank_gbtTx(6).data												<= (others=>'0');
					to_gbtBank_gbtTx(6).extraData_wideBus 									<= (others=>'0');
				elsif rising_edge(csi_TX_FRAME_CLK_LINK_6_I_clk) then
					asi_LINK6_I_ready 														<= '1';
					to_gbtBank_gbtTx(6).isDataSel											<= asi_LINK6_I_valid;
					to_gbtBank_gbtTx(6).data                            					<= asi_LINK6_I_data(83 downto 0); 
					to_gbtBank_gbtTx(6).extraData_wideBus               					<= asi_LINK6_I_data(115 downto 84);
				end if;
			end process;
		end generate gen_link6_stream_source;				
		
	end generate link_x116;
 --======================================================================================================================================================================--	  	  

 		--=====================--   
		-- AVALON STREAM SINK--   
		--=====================--
		--COMMENT: The role of ready signal is dummy in this design.

	link_rx_x84:
	if RX_CODING = 0 generate
				
		-- LINK1
		gen_link1_stream_sink:
		process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_1_I_clk)
		begin
			if rsi_LINK_reset = '1' then
				aso_LINK1_O_data															<= (others=>'0');
			elsif rising_edge(csi_RX_FRAME_CLK_LINK_1_I_clk) then
				aso_LINK1_O_valid															<= from_gbtBank_gbtRx(1).isDataFlag;
				aso_LINK1_O_data(83 downto 0)												<= from_gbtBank_gbtRx(1).data;
			end if;
		end process;

		-- LINK2
		gen_link2_stream_sink:
		if NUMBER_OF_LINKS > 1 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_2_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK2_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_2_I_clk) then
					aso_LINK2_O_valid															<= from_gbtBank_gbtRx(2).isDataFlag;
					aso_LINK2_O_data(83 downto 0)												<= from_gbtBank_gbtRx(2).data;
				end if;
			end process;
		end generate gen_link2_stream_sink;

		-- LINK3
		gen_link3_stream_sink:
		if NUMBER_OF_LINKS > 2 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_3_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK3_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_3_I_clk) then
					aso_LINK3_O_valid															<= from_gbtBank_gbtRx(3).isDataFlag;
					aso_LINK3_O_data(83 downto 0)												<= from_gbtBank_gbtRx(3).data;
				end if;
			end process;
		end generate gen_link3_stream_sink;		

		-- LINK4
		gen_link4_stream_sink:
		if NUMBER_OF_LINKS > 3 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_4_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK4_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_4_I_clk) then
					aso_LINK4_O_valid															<= from_gbtBank_gbtRx(4).isDataFlag;
					aso_LINK4_O_data(83 downto 0)												<= from_gbtBank_gbtRx(4).data;
				end if;
			end process;
		end generate gen_link4_stream_sink;

		-- LINK5
		gen_link5_stream_sink:
		if NUMBER_OF_LINKS > 4 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_5_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK5_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_5_I_clk) then
					aso_LINK5_O_valid															<= from_gbtBank_gbtRx(5).isDataFlag;
					aso_LINK5_O_data(83 downto 0)												<= from_gbtBank_gbtRx(5).data;
				end if;
			end process;
		end generate gen_link5_stream_sink;

		-- LINK6
		gen_link6_stream_sink:
		if NUMBER_OF_LINKS > 5 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_6_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK6_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_6_I_clk) then
					aso_LINK6_O_valid															<= from_gbtBank_gbtRx(6).isDataFlag;
					aso_LINK6_O_data(83 downto 0)												<= from_gbtBank_gbtRx(6).data;
				end if;
			end process;
		end generate gen_link6_stream_sink;	
	end generate link_rx_x84;
	
	link_rx_x116:
	if RX_CODING = 1 generate
				
		-- LINK1
		gen_link1_stream_sink:
		process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_1_I_clk)
		begin
			if rsi_LINK_reset = '1' then
				aso_LINK1_O_data															<= (others=>'0');
			elsif rising_edge(csi_RX_FRAME_CLK_LINK_1_I_clk) then
				aso_LINK1_O_valid															<= from_gbtBank_gbtRx(1).isDataFlag;
				aso_LINK1_O_data(83 downto 0)												<= from_gbtBank_gbtRx(1).data;
				aso_LINK1_O_data(115 downto 84)												<= from_gbtBank_gbtRx(1).extraData_widebus;
			end if;
		end process;

		-- LINK2
		gen_link2_stream_sink:
		if NUMBER_OF_LINKS > 1 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_2_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK2_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_2_I_clk) then
					aso_LINK2_O_valid															<= from_gbtBank_gbtRx(2).isDataFlag;
					aso_LINK2_O_data(83 downto 0)												<= from_gbtBank_gbtRx(2).data;
					aso_LINK2_O_data(115 downto 84)												<= from_gbtBank_gbtRx(2).extraData_widebus;
				end if;
			end process;
		end generate gen_link2_stream_sink;

		-- LINK3
		gen_link3_stream_sink:
		if NUMBER_OF_LINKS > 2 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_3_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK3_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_3_I_clk) then
					aso_LINK3_O_valid															<= from_gbtBank_gbtRx(3).isDataFlag;
					aso_LINK3_O_data(83 downto 0)												<= from_gbtBank_gbtRx(3).data;
					aso_LINK3_O_data(115 downto 84)												<= from_gbtBank_gbtRx(3).extraData_widebus;
				end if;
			end process;
		end generate gen_link3_stream_sink;		

		-- LINK4
		gen_link4_stream_sink:
		if NUMBER_OF_LINKS > 3 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_4_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK4_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_4_I_clk) then
					aso_LINK4_O_valid															<= from_gbtBank_gbtRx(4).isDataFlag;
					aso_LINK4_O_data(83 downto 0)												<= from_gbtBank_gbtRx(4).data;
					aso_LINK4_O_data(115 downto 84)												<= from_gbtBank_gbtRx(4).extraData_widebus;
				end if;
			end process;
		end generate gen_link4_stream_sink;

		-- LINK5
		gen_link5_stream_sink:
		if NUMBER_OF_LINKS > 4 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_5_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK5_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_5_I_clk) then
					aso_LINK5_O_valid															<= from_gbtBank_gbtRx(5).isDataFlag;
					aso_LINK5_O_data(83 downto 0)												<= from_gbtBank_gbtRx(5).data;
					aso_LINK5_O_data(115 downto 84)												<= from_gbtBank_gbtRx(5).extraData_widebus;
				end if;
			end process;
		end generate gen_link5_stream_sink;

		-- LINK6
		gen_link6_stream_sink:
		if NUMBER_OF_LINKS > 5 generate
			process(rsi_LINK_reset,csi_RX_FRAME_CLK_LINK_6_I_clk)
			begin
				if rsi_LINK_reset = '1' then
					aso_LINK6_O_data															<= (others=>'0');
				elsif rising_edge(csi_RX_FRAME_CLK_LINK_6_I_clk) then
					aso_LINK6_O_valid															<= from_gbtBank_gbtRx(6).isDataFlag;
					aso_LINK6_O_data(83 downto 0)												<= from_gbtBank_gbtRx(6).data;
					aso_LINK6_O_data(115 downto 84)												<= from_gbtBank_gbtRx(6).extraData_widebus;
				end if;
			end process;
		end generate gen_link6_stream_sink;	
	end generate link_rx_x116;
 --======================================================================================================================================================================--
 
		--=====================--   
		-- GBT BANK CONNECTION --   
		--=====================--
   gbtBank: entity work.gbt_bank
      generic map (
         GBT_BANK_ID                                       => GBT_BANK_ID)
      port map (                       
         CLKS_I                                            => to_gbtBank_clks,                                  
         CLKS_O                                            => from_gbtBank_clks,               
         ----------------------------------------------               
         GBT_TX_I                                          => to_gbtBank_gbtTx,             
         GBT_TX_O                                          => from_gbtBank_gbtTx,         
         ----------------------------------------------               
         MGT_I                                             => to_gbtBank_mgt,              
         MGT_O                                             => from_gbtBank_mgt,              
         ----------------------------------------------               
         GBT_RX_I                                          => to_gbtBank_gbtRx,              
         GBT_RX_O                                          => from_gbtBank_gbtRx         
      );   
--======================================================================================================================================================================--	  	  
		
		--===============--
		-- Serial Lanes  --
		--===============--  

			coe_tx_serial_data_LINK1_O										<= from_gbtBank_mgt.mgtLink(1).txSerialData when NUMBER_OF_LINKS>=1;
			to_gbtBank_mgt.mgtLink(1).rxSerialData              			<= coe_rx_serial_data_LINK1_I when NUMBER_OF_LINKS>=1;
			
			coe_tx_serial_data_LINK2_O										<= from_gbtBank_mgt.mgtLink(2).txSerialData when NUMBER_OF_LINKS>=2;
			to_gbtBank_mgt.mgtLink(2).rxSerialData              			<= coe_rx_serial_data_LINK2_I when NUMBER_OF_LINKS>=2;
			
			coe_tx_serial_data_LINK3_O										<= from_gbtBank_mgt.mgtLink(3).txSerialData when NUMBER_OF_LINKS>=3;
			to_gbtBank_mgt.mgtLink(3).rxSerialData              			<= coe_rx_serial_data_LINK3_I when NUMBER_OF_LINKS>=3;
			
			coe_tx_serial_data_LINK4_O										<= from_gbtBank_mgt.mgtLink(4).txSerialData when NUMBER_OF_LINKS>=4;
			to_gbtBank_mgt.mgtLink(4).rxSerialData              			<= coe_rx_serial_data_LINK4_I when NUMBER_OF_LINKS>=4;
			
			coe_tx_serial_data_LINK5_O										<= from_gbtBank_mgt.mgtLink(5).txSerialData when NUMBER_OF_LINKS>=5;
			to_gbtBank_mgt.mgtLink(5).rxSerialData              			<= coe_rx_serial_data_LINK5_I when NUMBER_OF_LINKS>=5;
			
			coe_tx_serial_data_LINK6_O										<= from_gbtBank_mgt.mgtLink(6).txSerialData when NUMBER_OF_LINKS>=6;
			to_gbtBank_mgt.mgtLink(6).rxSerialData              			<= coe_rx_serial_data_LINK6_I when NUMBER_OF_LINKS>=6;
--======================================================================================================================================================================--	  	  

		--===============--
		-- General reset --
		--===============--      
			  
--======================================================================================================================================================================--	  	  

		--================--
		-- GBT BANK RESET --
		--================--
		gen_gbt_bank_reset :
		for i in 1 to NUMBER_OF_LINKS generate
		  to_gbtBank_gbtTx(i).reset                           				<= rsi_MGT_TX_RESET_I;
		  to_gbtBank_mgt.mgtLink(i).tx_reset								<= rsi_MGT_RX_RESET_I;
		  to_gbtBank_mgt.mgtLink(i).rx_reset								<= rsi_GBT_TX_RESET_I;
		  to_gbtBank_gbtRx(i).reset                           				<= rsi_GBT_RX_RESET_I;
		end generate gen_gbt_bank_reset;
--======================================================================================================================================================================--	  	  

		--======================--
		-- MGT Reference clock :
		--======================--
   
		to_gbtBank_clks.mgt_clks.mgtRxRefClk                  				<= csi_MGT_REF_clk; 
		to_gbtBank_clks.mgt_clks.tx_frameClk                    			<= csi_TX_FRAME_CLK_LINK_1_I_clk;	  
--======================================================================================================================================================================--	  

	  --================--
      --  FRAME CLK     --
      --================--	  	 
	  gen_frame_clk:
		for i in 1 to NUMBER_OF_LINKS generate
			to_gbtBank_gbtRx(i).rxFrameClkReady									<= coe_RX_FRAME_CLK_PLL_LOCKED;
		end generate gen_frame_clk;
		------------------
		-- TX FRAME CLOCK:
		------------------
		to_gbtBank_clks.tx_frameClk(1)										<= csi_TX_FRAME_CLK_LINK_1_I_clk when NUMBER_OF_LINKS>=1;
		to_gbtBank_clks.tx_frameClk(2)										<= csi_TX_FRAME_CLK_LINK_2_I_clk when NUMBER_OF_LINKS>=2;
		to_gbtBank_clks.tx_frameClk(3)										<= csi_TX_FRAME_CLK_LINK_3_I_clk when NUMBER_OF_LINKS>=3;
		to_gbtBank_clks.tx_frameClk(4)										<= csi_TX_FRAME_CLK_LINK_4_I_clk when NUMBER_OF_LINKS>=4;
		to_gbtBank_clks.tx_frameClk(5)										<= csi_TX_FRAME_CLK_LINK_5_I_clk when NUMBER_OF_LINKS>=5;
		to_gbtBank_clks.tx_frameClk(6)										<= csi_TX_FRAME_CLK_LINK_6_I_clk when NUMBER_OF_LINKS>=6;

		monitor_txFrameClk_pll_locked_r										<= coe_TX_FRAME_CLK_PLL_LOCKED;
		
		------------------
		-- RX FRAME CLOCK:
		------------------
		to_gbtBank_clks.rx_frameClk(1)										<= csi_RX_FRAME_CLK_LINK_1_I_clk when NUMBER_OF_LINKS>=1;
		to_gbtBank_clks.rx_frameClk(2)										<= csi_RX_FRAME_CLK_LINK_2_I_clk when NUMBER_OF_LINKS>=2;
		to_gbtBank_clks.rx_frameClk(3)										<= csi_RX_FRAME_CLK_LINK_3_I_clk when NUMBER_OF_LINKS>=3;
		to_gbtBank_clks.rx_frameClk(4)										<= csi_RX_FRAME_CLK_LINK_4_I_clk when NUMBER_OF_LINKS>=4;
		to_gbtBank_clks.rx_frameClk(5)										<= csi_RX_FRAME_CLK_LINK_5_I_clk when NUMBER_OF_LINKS>=5;
		to_gbtBank_clks.rx_frameClk(6)										<= csi_RX_FRAME_CLK_LINK_6_I_clk when NUMBER_OF_LINKS>=6;
		
		
		monitor_rxFrameClk_pll_locked_r										<= coe_RX_FRAME_CLK_PLL_LOCKED;
		
	  
	  
--======================================================================================================================================================================--	  

	  --================--
      --   WORD CLK     --
      --================--	  	  
	  gen_word_clk:
	  for i in 1 to NUMBER_OF_LINKS generate
	  
		to_gbtBank_clks.mgt_clks.tx_wordClk(i)								<= from_gbtBank_clks.mgt_clks.tx_wordClk(1);	
	
		to_gbtBank_clks.mgt_clks.rx_wordClk(i)								<= from_gbtBank_clks.mgt_clks.rx_wordClk(i);
		gbtBank_rx_wordClk_mux(i)											<= from_gbtBank_clks.mgt_clks.rx_wordClk(i) when ( from_gbtBank_gbtRx(i).header_lockedFlag='1') 
																				else 'Z';		
	  end generate gen_word_clk;
	  
		------------------
		--  TX WORD CLOCK:
		------------------
		cso_TX_WORD_CLK_LINK_1_O_clk										<= from_gbtBank_clks.mgt_clks.tx_wordClk(1) when NUMBER_OF_LINKS>=1;
		cso_TX_WORD_CLK_LINK_2_O_clk										<= from_gbtBank_clks.mgt_clks.tx_wordClk(2) when NUMBER_OF_LINKS>=2;
		cso_TX_WORD_CLK_LINK_3_O_clk										<= from_gbtBank_clks.mgt_clks.tx_wordClk(3) when NUMBER_OF_LINKS>=3;
		cso_TX_WORD_CLK_LINK_4_O_clk										<= from_gbtBank_clks.mgt_clks.tx_wordClk(4) when NUMBER_OF_LINKS>=4;
		cso_TX_WORD_CLK_LINK_5_O_clk										<= from_gbtBank_clks.mgt_clks.tx_wordClk(5) when NUMBER_OF_LINKS>=5;
		cso_TX_WORD_CLK_LINK_6_O_clk										<= from_gbtBank_clks.mgt_clks.tx_wordClk(6) when NUMBER_OF_LINKS>=6;

		------------------
		--  RX WORD CLOCK:
		------------------
		cso_RX_WORD_CLK_LINK_1_O_clk										<= gbtBank_rx_wordClk_mux(1) when NUMBER_OF_LINKS>=1;
		cso_RX_WORD_CLK_LINK_2_O_clk										<= gbtBank_rx_wordClk_mux(2) when NUMBER_OF_LINKS>=2;
		cso_RX_WORD_CLK_LINK_3_O_clk										<= gbtBank_rx_wordClk_mux(3) when NUMBER_OF_LINKS>=3;
		cso_RX_WORD_CLK_LINK_4_O_clk										<= gbtBank_rx_wordClk_mux(4) when NUMBER_OF_LINKS>=4;
		cso_RX_WORD_CLK_LINK_5_O_clk										<= gbtBank_rx_wordClk_mux(5) when NUMBER_OF_LINKS>=5;
		cso_RX_WORD_CLK_LINK_6_O_clk										<= gbtBank_rx_wordClk_mux(6) when NUMBER_OF_LINKS>=6;
--======================================================================================================================================================================--	  

	  --================--
      --   MGT CLK      --
      --================--		  
	  to_gbtBank_clks.mgt_clks.extGxTxPll_clk								<= csi_ATX_PLL_BONDED_clk_clk;
	  to_gbtBank_mgt.mgtCommon.extGxTxPll_powerDown							<= coe_ATX_PLL_POWERDOWN;
	  to_gbtBank_mgt.mgtCommon.extGxTxPll_locked             				<= coe_ATX_PLL_LOCKED;
	  monitor_atx_pll_locked_r												<= coe_ATX_PLL_LOCKED;
	  monitor_atx_pll_cal_busy_r											<= coe_ATX_PLL_CAL_BUSY;
--======================================================================================================================================================================--	 
 
	  --=================================--
      -- AVALON MM SLAVE RECONFIGURATION --
      --=================================--
		to_gbtBank_mgt.mgtLink(1).reconfig_clk								<= csi_reconfig_clk_clk;
		to_gbtBank_mgt.mgtLink(1).reconfig_reset							<= rsi_reconfig_reset_reset;
		to_gbtBank_mgt.mgtLink(1).reconfig_write							<= avs_reconfig_write;
		to_gbtBank_mgt.mgtLink(1).reconfig_read								<= avs_reconfig_read;
		to_gbtBank_mgt.mgtLink(1).reconfig_address							<= avs_reconfig_address;
		to_gbtBank_mgt.mgtLink(1).reconfig_writedata						<= avs_reconfig_writedata;
		avs_reconfig_readdata			                        			<= from_gbtBank_mgt.mgtLink(1).reconfig_readdata;	
		avs_reconfig_waitrequest		                    				<= from_gbtBank_mgt.mgtLink(1).reconfig_waitrequest;
	  
--=====================================================================================--   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--