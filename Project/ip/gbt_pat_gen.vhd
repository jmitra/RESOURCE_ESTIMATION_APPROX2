--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               VECC (INDIA)                                                         
-- Engineer:              Jubin MITRA (jubin.mitra@cern.ch) (jm61288@gmail.com)
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           Altera Arria 10 - GBT QSYS Wrapper                                         
--                                                                                                 
-- Language:              VHDL'08                                                                  
--                                                                                                   
-- Target Device:         Altera Arria 10                                                        
-- Tool version:          Quartus II 15.0                                                               
--                                                                                                   
-- Version:               1.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        22/06/2015   1.0       Jubin Mitra	   - GBT QSYS Patern Generator
--            
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity gbt_pat_gen is 
   generic(
			CODING                                             : integer := 0
		);
   port ( 
      --====================================================--
	  -- QSYS port defination style
	  --<interface type prefix>_<interface name>_<signal type>
	  --====================================================--
      
	  --======================--
      --   AVALON MM Slave    --
      --======================-- 
		rsi_monitor_reset_reset                                        : in std_logic;
		csi_monitor_clk_clk                                            : in std_logic;
		avs_monitor_read                                               : in std_logic;
		avs_monitor_readdata                                           : out std_logic_vector(31 downto 0);
		avs_monitor_write                                              : in std_logic;
		avs_monitor_writedata                                          : in std_logic_vector(31 downto 0);
		avs_monitor_address                                            : in std_logic_vector( 3 downto 0);
	  
	  --===============================--
      -- AVALON Stream Source (output) --
      --======================-========--		
			
		aso_O_valid                   		                           : out std_logic;
		aso_O_data                     		  	                       : out std_logic_vector(83+CODING*32 downto 0);	
		aso_O_ready                             	                   : in std_logic;
	 
	 --=========================--
      --   FRAME CLK (40 MHz)    --
      --=========================--	  
	  csi_TX_FRAME_clk			   									   : in std_logic
	  
	);
end entity gbt_pat_gen;



--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of gbt_pat_gen is 

--================================ Signal Declarations ================================-- 
		signal pattern_select														: std_logic_vector(  3 downto 0):=(others=>'0');
		signal pattern																: std_logic_vector(115 downto 0);
		signal static_pattern														: std_logic_vector(115 downto 0):=x"BEEFCAFEC000BABEAC1DACDCFFFFF";
--==============================================================================================--   

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

		--==============================--
		--   MONITOR AVALON MM Slave    --
		--==============================-- 
		
		----------
		-- READ --
		----------
		process(rsi_monitor_reset_reset,csi_monitor_clk_clk,avs_monitor_read) 
		   begin
			if rsi_monitor_reset_reset = '1' then
				avs_monitor_readdata <= (others=>'0');
			else
				if rising_edge(csi_monitor_clk_clk) then
					if avs_monitor_read='1' then
						case avs_monitor_address is
							
							when x"0" 	=> avs_monitor_readdata <=  std_logic_vector(to_unsigned(CODING,avs_monitor_readdata'length));
							when x"1" 	=> avs_monitor_readdata <=  x"0000000" & pattern_select;
							when x"2" 	=> avs_monitor_readdata <=  		pattern(  31 downto 00);
							when x"3"  => avs_monitor_readdata <=  		    pattern(  63 downto 32);
							when x"4"  => avs_monitor_readdata <=  		    pattern(  95 downto 64);
							when x"5"  => avs_monitor_readdata <=  x"000" & pattern( 115 downto 96);
														
							when others => avs_monitor_readdata <= (others => '0');
						end case;
					end if;
				end if;
			end if;
		end process;
   
		
		-----------
		-- WRITE --
		-----------
		process(rsi_monitor_reset_reset,csi_monitor_clk_clk,avs_monitor_write) 
		   begin
			if rsi_monitor_reset_reset = '1' then
				pattern_select			   <= (others=>'0');
				static_pattern			   <= x"BEEFCAFEC000BABEAC1DACDCFFFFF";
			else
				if rising_edge(csi_monitor_clk_clk) then
					if avs_monitor_write='1' then
						case avs_monitor_address is
						
							when x"0" 	=> pattern_select 								<= avs_monitor_writedata(3 downto 0);
							when x"1" 	=> static_pattern(  31 downto 00)				<= avs_monitor_writedata;
							when x"2" 	=> static_pattern(  63 downto 32)				<= avs_monitor_writedata;
							when x"3" 	=> static_pattern(  95 downto 64)				<= avs_monitor_writedata;
							when x"4" 	=> static_pattern( 115 downto 96)				<= avs_monitor_writedata(19 downto 0);
							
							when others => pattern_select								<=(others=>'0');
						end case;
					end if;
				end if;
			end if;
		end process;   	

		--=====================--   
		-- AVALON STREAM SOURCE--   
		--=====================--
		link_gen_x84:
		if CODING = 0 generate
		
			gen_stream_source:
			process(rsi_monitor_reset_reset,csi_TX_FRAME_clk)
			begin
				if rsi_monitor_reset_reset = '1' then
					aso_O_data 														    <= (others=>'0');
					aso_O_valid															<= '0';
				elsif rising_edge(csi_TX_FRAME_clk) then
						aso_O_data 														<= pattern(83 downto 0);
						aso_O_valid														<= '1';
				end if;
			end process;
	  
		end generate link_gen_x84;
		
		link_gen_x115:
		if CODING = 1 generate
		
			gen_stream_source:
			process(rsi_monitor_reset_reset,csi_TX_FRAME_clk)
			begin
				if rsi_monitor_reset_reset = '1' then
					aso_O_data 														    <= (others=>'0');
					aso_O_valid															<= '0';
				elsif rising_edge(csi_TX_FRAME_clk) then
						aso_O_data 														<= pattern;
						aso_O_valid														<= '1';
				end if;
			end process;
			
		end generate link_gen_x115;
  --=====================================================================================--   

  --==================================== User Logic =====================================--
   
   main: process(rsi_monitor_reset_reset, csi_TX_FRAME_clk) 

      variable WordCounter32bit                   : unsigned( 31 downto 0):=(others=>'0');
	  begin                                      
      if rsi_monitor_reset_reset = '1' then                          

      elsif rising_edge(csi_TX_FRAME_clk) then 
             
			case pattern_select is 
			
				when x"0" => 
							pattern <= static_pattern;
				
				when x"1" =>
							WordCounter32bit := WordCounter32bit + 1;
							pattern <= std_logic_vector(WordCounter32bit( 19 downto 0)) & std_logic_vector(WordCounter32bit) & std_logic_vector(WordCounter32bit) & std_logic_vector(WordCounter32bit);
				when others =>
							pattern <= (others=>'0');
			 
			end case;
			 
	  end if;
	end process;
  --=====================================================================================--   

end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
	