-- CREATED BY JUBIN MITRA FOR MODELSIM SIMULATION
-- IEEE VHDL 2008 standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gbt_qsys_tb is

end gbt_qsys_tb;

architecture structural of gbt_qsys_tb is


    component ref_design_x6 is
        port (
            clk_clk                                      : in  std_logic                     := 'X';             -- clk
            gbt_qsys_wrapper_rx_serial_data_link1_export : in  std_logic                     := 'X';             -- export
            gbt_qsys_wrapper_rx_serial_data_link2_export : in  std_logic                     := 'X';             -- export
            gbt_qsys_wrapper_rx_serial_data_link3_export : in  std_logic                     := 'X';             -- export
            gbt_qsys_wrapper_rx_serial_data_link4_export : in  std_logic                     := 'X';             -- export
            gbt_qsys_wrapper_rx_serial_data_link5_export : in  std_logic                     := 'X';             -- export
            gbt_qsys_wrapper_rx_serial_data_link6_export : in  std_logic                     := 'X';             -- export
            gbt_qsys_wrapper_tx_serial_data_link1_export : out std_logic;                                        -- export
            gbt_qsys_wrapper_tx_serial_data_link2_export : out std_logic;                                        -- export
            gbt_qsys_wrapper_tx_serial_data_link3_export : out std_logic;                                        -- export
            gbt_qsys_wrapper_tx_serial_data_link4_export : out std_logic;                                        -- export
            gbt_qsys_wrapper_tx_serial_data_link5_export : out std_logic;                                        -- export
            gbt_qsys_wrapper_tx_serial_data_link6_export : out std_logic;                                        -- export
            gbtbank_reset_busy_writeresponsevalid_n      : out std_logic;                                        -- writeresponsevalid_n
            gbtbank_reset_done_writeresponsevalid_n      : out std_logic;                                        -- writeresponsevalid_n
            gbtbank_reset_general_reset_reset            : in  std_logic                     := 'X';             -- reset
            mm_bridge_0_s0_waitrequest                   : out std_logic;                                        -- waitrequest
            mm_bridge_0_s0_readdata                      : out std_logic_vector(31 downto 0);                    -- readdata
            mm_bridge_0_s0_readdatavalid                 : out std_logic;                                        -- readdatavalid
            mm_bridge_0_s0_burstcount                    : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- burstcount
            mm_bridge_0_s0_writedata                     : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
            mm_bridge_0_s0_address                       : in  std_logic_vector(15 downto 0) := (others => 'X'); -- address
            mm_bridge_0_s0_write                         : in  std_logic                     := 'X';             -- write
            mm_bridge_0_s0_read                          : in  std_logic                     := 'X';             -- read
            mm_bridge_0_s0_byteenable                    : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
            mm_bridge_0_s0_debugaccess                   : in  std_logic                     := 'X';             -- debugaccess
            reset_reset_n                                : in  std_logic                     := 'X'              -- reset_n
        );
    end component ref_design_x6;

signal clk_clk: std_logic;
signal gbt_qsys_wrapper_tx_serial_data_link1_export: std_logic;
signal gbt_qsys_wrapper_rx_serial_data_link1_export: std_logic;
signal reset   : std_logic;
signal reset_n : std_logic;

--constant clk_period_ref : time := 8 ns;
constant clk_period_pll : time := 10 ns;

signal CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link1_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link2_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link3_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link4_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link5_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link6_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link1_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link2_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link3_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link4_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link5_export : std_logic;
signal CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link6_export : std_logic;
signal CONNECTED_TO_gbtbank_reset_busy_writeresponsevalid_n : std_logic;
signal CONNECTED_TO_gbtbank_reset_done_writeresponsevalid_n : std_logic;

signal CONNECTED_TO_mm_bridge_0_s0_waitrequest : std_logic;
signal CONNECTED_TO_mm_bridge_0_s0_readdata : std_logic_vector(31 downto 0);
signal CONNECTED_TO_mm_bridge_0_s0_readdatavalid : std_logic;
signal CONNECTED_TO_mm_bridge_0_s0_burstcount : std_logic_vector(0 downto 0);
signal CONNECTED_TO_mm_bridge_0_s0_writedata : std_logic_vector(31 downto 0);
signal CONNECTED_TO_mm_bridge_0_s0_address : std_logic_vector(15 downto 0);
signal CONNECTED_TO_mm_bridge_0_s0_write : std_logic;
signal CONNECTED_TO_mm_bridge_0_s0_read : std_logic;
signal CONNECTED_TO_mm_bridge_0_s0_byteenable : std_logic_vector(3 downto 0);
signal CONNECTED_TO_mm_bridge_0_s0_debugaccess : std_logic;


begin

	u0 : component ref_design_x6
        port map (
            clk_clk                                      => clk_clk,                                      			   --                                   clk.clk
            gbt_qsys_wrapper_rx_serial_data_link1_export => CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link1_export, -- gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_qsys_wrapper_rx_serial_data_link2_export => CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link2_export, -- gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_qsys_wrapper_rx_serial_data_link3_export => CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link3_export, -- gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_qsys_wrapper_rx_serial_data_link4_export => CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link4_export, -- gbt_qsys_wrapper_rx_serial_data_link4.export
            gbt_qsys_wrapper_rx_serial_data_link5_export => CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link5_export, -- gbt_qsys_wrapper_rx_serial_data_link5.export
            gbt_qsys_wrapper_rx_serial_data_link6_export => CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link6_export, -- gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_qsys_wrapper_tx_serial_data_link1_export => CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link1_export, -- gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_qsys_wrapper_tx_serial_data_link2_export => CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link2_export, -- gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_qsys_wrapper_tx_serial_data_link3_export => CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link3_export, -- gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_qsys_wrapper_tx_serial_data_link4_export => CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link4_export, -- gbt_qsys_wrapper_tx_serial_data_link4.export
            gbt_qsys_wrapper_tx_serial_data_link5_export => CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link5_export, -- gbt_qsys_wrapper_tx_serial_data_link5.export
            gbt_qsys_wrapper_tx_serial_data_link6_export => CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link6_export, -- gbt_qsys_wrapper_tx_serial_data_link6.export
            gbtbank_reset_busy_writeresponsevalid_n      => CONNECTED_TO_gbtbank_reset_busy_writeresponsevalid_n,      --                    gbtbank_reset_busy.writeresponsevalid_n
            gbtbank_reset_done_writeresponsevalid_n      => CONNECTED_TO_gbtbank_reset_done_writeresponsevalid_n,      --                    gbtbank_reset_done.writeresponsevalid_n
            gbtbank_reset_general_reset_reset            => reset,            										   --           gbtbank_reset_general_reset.reset
            mm_bridge_0_s0_waitrequest                   => CONNECTED_TO_mm_bridge_0_s0_waitrequest,                   --                        mm_bridge_0_s0.waitrequest
            mm_bridge_0_s0_readdata                      => CONNECTED_TO_mm_bridge_0_s0_readdata,                      --                                      .readdata
            mm_bridge_0_s0_readdatavalid                 => CONNECTED_TO_mm_bridge_0_s0_readdatavalid,                 --                                      .readdatavalid
            mm_bridge_0_s0_burstcount                    => CONNECTED_TO_mm_bridge_0_s0_burstcount,                    --                                      .burstcount
            mm_bridge_0_s0_writedata                     => CONNECTED_TO_mm_bridge_0_s0_writedata,                     --                                      .writedata
            mm_bridge_0_s0_address                       => CONNECTED_TO_mm_bridge_0_s0_address,                       --                                      .address
            mm_bridge_0_s0_write                         => CONNECTED_TO_mm_bridge_0_s0_write,                         --                                      .write
            mm_bridge_0_s0_read                          => CONNECTED_TO_mm_bridge_0_s0_read,                          --                                      .read
            mm_bridge_0_s0_byteenable                    => CONNECTED_TO_mm_bridge_0_s0_byteenable,                    --                                      .byteenable
            mm_bridge_0_s0_debugaccess                   => CONNECTED_TO_mm_bridge_0_s0_debugaccess,                   --                                      .debugaccess
            reset_reset_n                                => reset_n      					                           --                                 reset.reset_n
        );


		CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link1_export <= CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link1_export;
		CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link2_export <= CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link2_export;
		CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link3_export <= CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link3_export;
		CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link4_export <= CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link4_export;
		CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link5_export <= CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link5_export;
		CONNECTED_TO_gbt_qsys_wrapper_rx_serial_data_link6_export <= CONNECTED_TO_gbt_qsys_wrapper_tx_serial_data_link6_export;
	
  -- Clock process definitions for 120MHz	
   clk_process_pll :process
   begin
		clk_clk <= '0';
		wait for clk_period_pll/2;
		clk_clk <= '1';
		wait for clk_period_pll/2;
   end process;
	

	process
	begin
		wait until rising_edge(clk_clk);
		reset_n <= '0';
		reset   <= '1';
		
	for i in 0 to 100 loop
		wait until rising_edge(clk_clk);
	end loop;
		reset_n <= '1';
		reset   <= '0';

		-- end loop;
		wait;
		end process;
	
end structural;