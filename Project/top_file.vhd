----------------------------------------------------------------------------------------
-- Date of Design : 7th July 2015
-- Designed By : Jubin MITRA (jubin.mitra@cern.ch)
-- Purpose : For GBT Testing using QSYS wrapper
----------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library alt_a10_issp;

entity top_file is
port(
	A10_REFCLK_GBT_P					: in  std_logic_vector( 7 downto 0);
	
	MP0_TX_Resetl						: out std_logic;
	MP0_RX_Resetl						: out std_logic;
	MP0_TX_DATA_p						: out std_logic_vector(11 downto 0);
	MP0_RX_DATA_p						: in  std_logic_vector(11 downto 0);
	
	MP1_TX_Resetl						: out std_logic;
	MP1_RX_Resetl						: out std_logic;
	MP1_TX_DATA_p						: out std_logic_vector(11 downto 0);
	MP1_RX_DATA_p						: in  std_logic_vector(11 downto 0);
	
	MP2_TX_Resetl						: out std_logic;
	MP2_RX_Resetl						: out std_logic;
	MP2_TX_DATA_p						: out std_logic_vector(11 downto 0);
	MP2_RX_DATA_p						: in  std_logic_vector(11 downto 0);
	
	MP3_TX_Resetl						: out std_logic;
	MP3_RX_Resetl						: out std_logic;
	MP3_TX_DATA_p						: out std_logic_vector(11 downto 0);
	MP3_RX_DATA_p						: in  std_logic_vector(11 downto 0);

-- PCIe x16 gen3
	A10_CLK_PCIE_P									: in std_logic_vector  ( 1 downto 0);
	PLX_A10_PCIE_TX_P								: out std_logic_vector (15 downto 0);
	PLX_A10_PCIE_RX_P								: in  std_logic_vector (15 downto 0);
	pcie_8ch_lsb_pcie_npor_pin_perst				: in std_logic;                         		-- No Pin assigned (Default value should be '1')
	pcie_8ch_msb_pcie_npor_pin_perst				: in std_logic;									-- No Pin assigned (Default value should be '1')

	-- TEST INPUTS
	PCIe_LTSSM										: in std_logic_vector ( 4 downto 0);
	PCIe_rate										: in std_logic_vector ( 1 downto 0);
	PCIe_txdetectrx									: in std_logic;
	PCIe_txswing									: in std_logic;
	
-- SFP+ Connection TEST
	A10_REFCLK_TFC_P      : in  STD_LOGIC;
	A10_SFP_TFC_TX_P      : out STD_LOGIC;
	A10_SFP_TFC_RX_P      : in  STD_LOGIC;
	A10_SFP_TFC_TX_ENABLE : out  STD_LOGIC;
	
	A10_SI5315_BWSEL      : out STD_LOGIC_VECTOR(1 downto 0);  -- SI5315
    A10_SI5315_DBL2_BY    : out STD_LOGIC;
    A10_SI5315_FREQSEL    : out STD_LOGIC_VECTOR(3 downto 0);
    A10_SI5315_FRQTBL     : out STD_LOGIC;
    A10_SI5315_RESET_n    : out STD_LOGIC;
    A10_SI5315_SFOUT      : out STD_LOGIC_VECTOR(1 downto 0)		
	
);
end entity top_file;

architecture top_connection of top_file is

signal      system_reset0_cnt   : std_logic_vector(7 downto 0);
signal      system_reset1_cnt   : std_logic_vector(7 downto 0);
signal      system_reset2_cnt   : std_logic_vector(7 downto 0);
signal      system_reset3_cnt   : std_logic_vector(7 downto 0);
signal      system_reset4_cnt   : std_logic_vector(7 downto 0);
signal      system_reset5_cnt   : std_logic_vector(7 downto 0);
signal      system_reset6_cnt   : std_logic_vector(7 downto 0);
signal      system_reset7_cnt   : std_logic_vector(7 downto 0);
signal      reset0_n	       : std_logic:='0';
signal      reset1_n	       : std_logic:='0';
signal      reset2_n	       : std_logic:='0';
signal      reset3_n	       : std_logic:='0';
signal      reset4_n	       : std_logic:='0';
signal      reset5_n	       : std_logic:='0';
signal      reset6_n	       : std_logic:='0';
signal      reset7_n	       : std_logic:='0';

signal      source  	       : std_logic_vector(0 downto 0) :=(others=>'0');
signal      probe	    	   : std_logic_vector(0 downto 0) :=(others=>'0');

 component gbt_x2_top is
        port (
            fabric_clk_clk                                            : in  std_logic := 'X'; -- clk
            fabric_reset_reset_n                                      : in  std_logic := 'X'; -- reset_n
            mgt_clk_clk                                               : in  std_logic := 'X'; -- clk
            mgt_reset_reset_n                                         : in  std_logic := 'X'; -- reset_n
            gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export : in  std_logic := 'X'; -- export
            gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export : in  std_logic := 'X'; -- export
            gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export : out std_logic;        -- export
            gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export : out std_logic;        -- export
            gbt_x2_bank0_gbtbank_reset_busy_writeresponsevalid_n      : out std_logic;        -- writeresponsevalid_n
            gbt_x2_bank0_gbtbank_reset_done_writeresponsevalid_n      : out std_logic         -- writeresponsevalid_n
        );
    end component gbt_x2_top;

	 component gbt_x4_top is
        port (
            fabric_clk_clk                                            : in  std_logic := 'X'; -- clk
            fabric_reset_reset_n                                      : in  std_logic := 'X'; -- reset_n
            mgt_clk_clk                                               : in  std_logic := 'X'; -- clk
            mgt_reset_reset_n                                         : in  std_logic := 'X'; -- reset_n
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export : in  std_logic := 'X'; -- export
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export : in  std_logic := 'X'; -- export
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export : in  std_logic := 'X'; -- export
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export : in  std_logic := 'X'; -- export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export : out std_logic;        -- export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export : out std_logic;        -- export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export : out std_logic;        -- export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export : out std_logic;        -- export
            gbt_x4_bank0_gbtbank_reset_busy_writeresponsevalid_n      : out std_logic;        -- writeresponsevalid_n
            gbt_x4_bank0_gbtbank_reset_done_writeresponsevalid_n      : out std_logic         -- writeresponsevalid_n
        );
    end component gbt_x4_top;
	
 component gbt_x5_top is
        port (
            fabric_clk_clk                                            : in  std_logic := 'X'; -- clk
            fabric_reset_reset_n                                      : in  std_logic := 'X'; -- reset_n
            mgt_clk_clk                                               : in  std_logic := 'X'; -- clk
            mgt_reset_reset_n                                         : in  std_logic := 'X'; -- reset_n
            gbt_x5_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export : in  std_logic := 'X'; -- export
            gbt_x5_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export : in  std_logic := 'X'; -- export
            gbt_x5_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export : in  std_logic := 'X'; -- export
            gbt_x5_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export : in  std_logic := 'X'; -- export
            gbt_x5_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export : in  std_logic := 'X'; -- export
            gbt_x5_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export : out std_logic;        -- export
            gbt_x5_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export : out std_logic;        -- export
            gbt_x5_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export : out std_logic;        -- export
            gbt_x5_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export : out std_logic;        -- export
            gbt_x5_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export : out std_logic;        -- export
            gbt_x5_bank0_gbtbank_reset_busy_writeresponsevalid_n      : out std_logic;        -- writeresponsevalid_n
            gbt_x5_bank0_gbtbank_reset_done_writeresponsevalid_n      : out std_logic         -- writeresponsevalid_n
        );
    end component gbt_x5_top;

 component gbt_x6_top is
        port (
            fabric_clk_clk                                            : in  std_logic := 'X'; -- clk
            fabric_reset_reset_n                                      : in  std_logic := 'X'; -- reset_n
            mgt_clk_clk                                               : in  std_logic := 'X'; -- clk
            mgt_reset_reset_n                                         : in  std_logic := 'X'; -- reset_n
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export : in  std_logic := 'X'; -- export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export : in  std_logic := 'X'; -- export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export : in  std_logic := 'X'; -- export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export : in  std_logic := 'X'; -- export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export : in  std_logic := 'X'; -- export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export : in  std_logic := 'X'; -- export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export : out std_logic;        -- export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export : out std_logic;        -- export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export : out std_logic;        -- export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export : out std_logic;        -- export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export : out std_logic;        -- export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export : out std_logic;        -- export
            gbt_x6_bank0_gbtbank_reset_busy_writeresponsevalid_n      : out std_logic;        -- writeresponsevalid_n
            gbt_x6_bank0_gbtbank_reset_done_writeresponsevalid_n      : out std_logic         -- writeresponsevalid_n
        );
    end component gbt_x6_top;


	
	component counter_128
	PORT
	(
		clock		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
	);
	end component;

	component PCIe_test_top is
	port(
	A10_CLK_PCIE_P									: in std_logic_vector  ( 1 downto 0);
	PLX_A10_PCIE_TX_P								: out std_logic_vector (15 downto 0);
	PLX_A10_PCIE_RX_P								: in  std_logic_vector (15 downto 0);
	pcie_8ch_msb_pcie_npor_pin_perst				: in std_logic;
	pcie_8ch_lsb_pcie_npor_pin_perst				: in std_logic;
	--- TEST INPUTS
	PCIe_LTSSM										: in std_logic_vector ( 4 downto 0);
	PCIe_rate										: in std_logic_vector ( 1 downto 0);
	PCIe_txdetectrx									: in std_logic;
	PCIe_txswing									: in std_logic
	);
	end component PCIe_test_top;

	component sfp is 
	port(
		A10_REFCLK_TFC_P      : in  STD_LOGIC;
		A10_SFP_TFC_TX_P      : out STD_LOGIC;
		A10_SFP_TFC_RX_P      : in  STD_LOGIC;
		A10_SFP_TFC_TX_ENABLE : out  STD_LOGIC;
		
		A10_SI5315_BWSEL      : out STD_LOGIC_VECTOR(1 downto 0);  -- SI5315
		A10_SI5315_DBL2_BY    : out STD_LOGIC;
		A10_SI5315_FREQSEL    : out STD_LOGIC_VECTOR(3 downto 0);
		A10_SI5315_FRQTBL     : out STD_LOGIC;
		A10_SI5315_RESET_n    : out STD_LOGIC;
		A10_SI5315_SFOUT      : out STD_LOGIC_VECTOR(1 downto 0)
	);	
	end component sfp;

begin
	 counter0_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(0),
		q		=> system_reset0_cnt
	  );
	  
	 counter1_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(1),
		q		=> system_reset1_cnt
	  );
	  
	 counter2_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(2),
		q		=> system_reset2_cnt
	  );
	  
	 counter3_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(3),
		q		=> system_reset3_cnt
	  );
	  
	 counter4_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(4),
		q		=> system_reset4_cnt
	  );
	
	 counter5_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(5),
		q		=> system_reset5_cnt
	  );

	  counter6_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(6),
		q		=> system_reset6_cnt
	  );

	  counter7_L: component counter_128
	  port map(
		clock	=> A10_REFCLK_GBT_P(7),
		q		=> system_reset7_cnt
	  );
	  
	  reset0:process(A10_REFCLK_GBT_P(0))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(0)) then
			if (system_reset0_cnt = x"FF") then
           
              reset0_n <= '1';
           end if;
		end if;
	  end process;
	  
	  reset1:process(A10_REFCLK_GBT_P(1))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(1)) then
			if (system_reset1_cnt = x"FF") then
           
              reset1_n <= '1';
           end if;
		end if;
	  end process;	  
	  
	  reset2:process(A10_REFCLK_GBT_P(2))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(2)) then
			if (system_reset2_cnt = x"FF") then
           
              reset2_n <= '1';
           end if;
		end if;
	  end process;
	  
	  reset3:process(A10_REFCLK_GBT_P(3))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(3)) then
			if (system_reset3_cnt = x"FF") then
           
              reset3_n <= '1';
           end if;
		end if;
	  end process;	  
	  
	  reset4:process(A10_REFCLK_GBT_P(4))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(4)) then
			if (system_reset4_cnt = x"FF") then
           
              reset4_n <= '1';
           end if;
		end if;
	  end process;
	  
	  reset5:process(A10_REFCLK_GBT_P(5))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(5)) then
			if (system_reset5_cnt = x"FF") then
           
              reset5_n <= '1';
           end if;
		end if;
	  end process;	  
	  
	  reset6:process(A10_REFCLK_GBT_P(6))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(6)) then
			if (system_reset6_cnt = x"FF") then
           
              reset6_n <= '1';
           end if;
		end if;
	  end process;
	  
	  reset7:process(A10_REFCLK_GBT_P(7))
	  begin
		if rising_edge(A10_REFCLK_GBT_P(7)) then
			if (system_reset7_cnt = x"FF") then
           
              reset7_n <= '1';
           end if;
		end if;
	  end process;
	  
	    GBTx6_7 : component gbt_x6_top
        port map (
            fabric_clk_clk                                            => A10_REFCLK_GBT_P(7),       
            mgt_clk_clk												  => A10_REFCLK_GBT_P(6),	--                                              clk_0.clk
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP3_RX_DATA_p(0), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP3_RX_DATA_p(2), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP3_RX_DATA_p(4), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP3_RX_DATA_p(6), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP3_RX_DATA_p(8), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP3_RX_DATA_p(10), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP3_TX_DATA_p(0), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP3_TX_DATA_p(2), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP3_TX_DATA_p(4), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP3_TX_DATA_p(6), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP3_TX_DATA_p(8), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP3_TX_DATA_p(10), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            gbt_x6_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_busy.writeresponsevalid_n
            gbt_x6_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_done.writeresponsevalid_n
			mgt_reset_reset_n										  => '0',       -- disconnected
            fabric_reset_reset_n                                      => reset7_n                                            --                                            reset_0.reset_n
        );	  
	    GBTx6_6 : component gbt_x6_top
        port map (
            fabric_clk_clk                                            => A10_REFCLK_GBT_P(7),       
            mgt_clk_clk												  => A10_REFCLK_GBT_P(6),	--                                              clk_0.clk            
			gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP3_RX_DATA_p(1), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP3_RX_DATA_p(3), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP3_RX_DATA_p(5), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP3_RX_DATA_p(7), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP3_RX_DATA_p(9), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP3_RX_DATA_p(11), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP3_TX_DATA_p(1), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP3_TX_DATA_p(3), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP3_TX_DATA_p(5), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP3_TX_DATA_p(7), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP3_TX_DATA_p(9), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP3_TX_DATA_p(11), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            gbt_x6_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_busy.writeresponsevalid_n
            gbt_x6_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_done.writeresponsevalid_n
            mgt_reset_reset_n										  => '0',       -- disconnected
            fabric_reset_reset_n                                      => reset6_n                                                      --                                            reset_0.reset_n
        );	  
        MP3_TX_Resetl <= reset7_n or reset6_n;                                                                                        
        MP3_RX_Resetl <= reset7_n or reset6_n;                                                                                        

		GBTx6_5 : component gbt_x6_top
        port map (
            fabric_clk_clk                                            => A10_REFCLK_GBT_P(5),       
            mgt_clk_clk												  => A10_REFCLK_GBT_P(4),
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP2_RX_DATA_p(0), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP2_RX_DATA_p(2), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP2_RX_DATA_p(4), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP2_RX_DATA_p(6), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP2_RX_DATA_p(8), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP2_RX_DATA_p(10), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP2_TX_DATA_p(0), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP2_TX_DATA_p(2), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP2_TX_DATA_p(4), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP2_TX_DATA_p(6), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP2_TX_DATA_p(8), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP2_TX_DATA_p(10), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            gbt_x6_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_busy.writeresponsevalid_n
            gbt_x6_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_done.writeresponsevalid_n
            mgt_reset_reset_n										  => '0',       -- disconnected
            fabric_reset_reset_n                                      => reset5_n                                                       --                                            reset_0.reset_n
        );	  
	    GBTx6_4 : component gbt_x6_top
        port map (
            fabric_clk_clk                                            => A10_REFCLK_GBT_P(5),       
            mgt_clk_clk												  => A10_REFCLK_GBT_P(4),
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP2_RX_DATA_p(1), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP2_RX_DATA_p(3), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP2_RX_DATA_p(5), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP2_RX_DATA_p(7), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP2_RX_DATA_p(9), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP2_RX_DATA_p(11), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP2_TX_DATA_p(1), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP2_TX_DATA_p(3), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP2_TX_DATA_p(5), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP2_TX_DATA_p(7), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP2_TX_DATA_p(9), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP2_TX_DATA_p(11), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            gbt_x6_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_busy.writeresponsevalid_n
            gbt_x6_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_done.writeresponsevalid_n
			mgt_reset_reset_n										  => '0',       -- disconnected
            fabric_reset_reset_n                                      => reset4_n           
			);	  
        MP2_TX_Resetl <= reset5_n or reset4_n; 
        MP2_RX_Resetl <= reset5_n or reset4_n; 
		
		GBTx6_3 : component gbt_x6_top
        port map (
            fabric_clk_clk                                            => A10_REFCLK_GBT_P(3),       
            mgt_clk_clk												  => A10_REFCLK_GBT_P(2),
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP1_RX_DATA_p(0), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP1_RX_DATA_p(2), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP1_RX_DATA_p(4), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP1_RX_DATA_p(6), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP1_RX_DATA_p(8), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP1_RX_DATA_p(10), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP1_TX_DATA_p(0), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP1_TX_DATA_p(2), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP1_TX_DATA_p(4), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP1_TX_DATA_p(6), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP1_TX_DATA_p(8), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP1_TX_DATA_p(10), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            gbt_x6_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_busy.writeresponsevalid_n
            gbt_x6_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_done.writeresponsevalid_n
			mgt_reset_reset_n										  => '0',       -- disconnected
            fabric_reset_reset_n                                      => reset3_n           
			);	  

		GBTx6_2 : component gbt_x6_top
        port map (
            fabric_clk_clk                                            => A10_REFCLK_GBT_P(3),       
            mgt_clk_clk												  => A10_REFCLK_GBT_P(2),
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP1_RX_DATA_p(1), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP1_RX_DATA_p(3), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP1_RX_DATA_p(5), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP1_RX_DATA_p(7), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP1_RX_DATA_p(9), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP1_RX_DATA_p(11), -- gbt_x6_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP1_TX_DATA_p(1), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP1_TX_DATA_p(3), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP1_TX_DATA_p(5), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP1_TX_DATA_p(7), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP1_TX_DATA_p(9), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP1_TX_DATA_p(11), -- gbt_x6_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            gbt_x6_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_busy.writeresponsevalid_n
            gbt_x6_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x6_bank0_gbtbank_reset_done.writeresponsevalid_n
			mgt_reset_reset_n										  => '0',       -- disconnected
            fabric_reset_reset_n                                      => reset2_n           
			);	  

		MP1_TX_Resetl <= reset3_n or reset2_n; 
        MP1_RX_Resetl <= reset3_n or reset2_n; 
		
		GBTx4_1 : component gbt_x4_top
        port map (
            fabric_clk_clk                                            => A10_REFCLK_GBT_P(1),       
            mgt_clk_clk												  => A10_REFCLK_GBT_P(0),
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP0_RX_DATA_p(0), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP0_RX_DATA_p(2), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP0_RX_DATA_p(4), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            gbt_x4_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP0_RX_DATA_p(6), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP0_RX_DATA_p(8), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            --gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP0_RX_DATA_p(10), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP0_TX_DATA_p(0), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP0_TX_DATA_p(2), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP0_TX_DATA_p(4), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            gbt_x4_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP0_TX_DATA_p(6), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP0_TX_DATA_p(8), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            --gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP0_TX_DATA_p(10), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            gbt_x4_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x2_bank0_gbtbank_reset_busy.writeresponsevalid_n
            gbt_x4_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x2_bank0_gbtbank_reset_done.writeresponsevalid_n
			mgt_reset_reset_n										  => '0',       -- disconnected
            fabric_reset_reset_n                                      => reset1_n           
			);	  

		-- GBTx2_0 : component gbt_x2_top
        -- port map (
            -- fabric_clk_clk                                            => A10_REFCLK_GBT_P(1),       
            -- mgt_clk_clk												  => A10_REFCLK_GBT_P(0),
            -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link1_export => MP0_RX_DATA_p(1), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link1.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link2_export => MP0_RX_DATA_p(3), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link2.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link3_export => MP0_RX_DATA_p(5), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link3.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link4_export => MP0_RX_DATA_p(7), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link4.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link5_export => MP0_RX_DATA_p(9), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link5.export
            --gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link6_export => MP0_RX_DATA_p(11), -- gbt_x2_bank0_gbt_qsys_wrapper_rx_serial_data_link6.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link1_export => MP0_TX_DATA_p(1), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link1.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link2_export => MP0_TX_DATA_p(3), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link2.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link3_export => MP0_TX_DATA_p(5), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link3.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link4_export => MP0_TX_DATA_p(7), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link4.export
            -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link5_export => MP0_TX_DATA_p(9), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link5.export
            --gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link6_export => MP0_TX_DATA_p(11), -- gbt_x2_bank0_gbt_qsys_wrapper_tx_serial_data_link6.export
            -- gbt_x2_bank0_gbtbank_reset_busy_writeresponsevalid_n      => open,      --                    gbt_x2_bank0_gbtbank_reset_busy.writeresponsevalid_n
            -- gbt_x2_bank0_gbtbank_reset_done_writeresponsevalid_n      => open,      --                    gbt_x2_bank0_gbtbank_reset_done.writeresponsevalid_n
			-- mgt_reset_reset_n										  => '0',       -- disconnected
            -- fabric_reset_reset_n                                      => reset0_n           
			-- );	  
        -- MP0_TX_Resetl <= reset1_n or reset0_n; 
        -- MP0_RX_Resetl <= reset1_n or reset0_n; 		
		
		pcie_comp: component  PCIe_test_top
		port map(
			A10_CLK_PCIE_P									=> A10_CLK_PCIE_P,
			PLX_A10_PCIE_TX_P								=> PLX_A10_PCIE_TX_P,
			PLX_A10_PCIE_RX_P								=> PLX_A10_PCIE_RX_P,
			pcie_8ch_msb_pcie_npor_pin_perst				=> pcie_8ch_msb_pcie_npor_pin_perst,
			pcie_8ch_lsb_pcie_npor_pin_perst				=> pcie_8ch_lsb_pcie_npor_pin_perst,
			-- TEST INPUTS
			PCIe_LTSSM										=> PCIe_LTSSM,
			PCIe_rate										=> PCIe_rate,
			PCIe_txdetectrx									=> PCIe_txdetectrx,
			PCIe_txswing									=> PCIe_txswing
		);
		
		sfp_comp: component sfp
		port map(
			A10_REFCLK_TFC_P      	                        => A10_REFCLK_TFC_P,
			A10_SFP_TFC_TX_P                                => A10_SFP_TFC_TX_P, 
			A10_SFP_TFC_RX_P                                => A10_SFP_TFC_RX_P,
			A10_SFP_TFC_TX_ENABLE                           => A10_SFP_TFC_TX_ENABLE,
			
			A10_SI5315_BWSEL                                => A10_SI5315_BWSEL,
			A10_SI5315_DBL2_BY                              => A10_SI5315_DBL2_BY,
			A10_SI5315_FREQSEL                              => A10_SI5315_FREQSEL,
			A10_SI5315_FRQTBL                               => A10_SI5315_FRQTBL,
			A10_SI5315_RESET_n                              => A10_SI5315_RESET_n,
			A10_SI5315_SFOUT                                => A10_SI5315_SFOUT
		);

end architecture top_connection;