# Created by Jubin Mitra on 9th MArch 2016
echo "Cleaning Project Directory"
rm -r ip/*/
rm -r ip/.qsys_edit/
rm -r ./.qsys_edit/
rm -r ip/*.sopcinfo
mv -f ip ../ip
rm -r ./*/
rm *.sopcinfo
rm *.*~
rm *.rpt
rm *.qar
rm *.qarlog
rm *.txt
mv -f ../ip ip

rm -r ../Interfaces/PCIe_x16_mm_DMA_Q15.0/*/
rm ../Interfaces/PCIe_x16_mm_DMA_Q15.0/*.sopcinfo
rm ../Interfaces/PCIe_x16_mm_DMA_Q15.0/*.BAK.qsys

rm -r ../Interfaces/sfp/*/
rm ../Interfaces/sfp/*.sopcinfo
rm ../Interfaces/sfp/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x1/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x1/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x1/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x2/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x2/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x2/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x3/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x3/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x3/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x4/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x4/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x4/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x5/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x5/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x5/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x6/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x6/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x6/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x1/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x1/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x1/*.BAK.qsys


rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x2/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x2/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x2/*.BAK.qsys


rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x3/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x3/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x3/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x4/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x4/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x4/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x5/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x5/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x5/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x6/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x6/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x6/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_rx/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_rx/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_rx/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_tx/*/
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_tx/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_tx/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/gbt_tx/tx_dpram/*/
rm ../Interfaces/gbt_bank/altera_a10/gbt_tx/tx_dpram/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/gbt_tx/tx_dpram/*.BAK.qsys

rm -r ../Interfaces/gbt_bank/altera_a10/gbt_rx/rx_dpram/*/
rm ../Interfaces/gbt_bank/altera_a10/gbt_rx/rx_dpram/*.sopcinfo
rm ../Interfaces/gbt_bank/altera_a10/gbt_rx/rx_dpram/*.BAK.qsys
