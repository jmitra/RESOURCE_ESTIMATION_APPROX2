# Created by Jubin Mitra on 9th MArch 2016
quartus_map="/opt/altera/15.1/quartus/bin/quartus_map"
modelsim_script_gen="/opt/altera/15.1/quartus/sopc_builder/bin/ip-make-simscript"
quartus_sh="/opt/altera/15.1/quartus/bin/quartus_sh"
qnativesim="/opt/altera/15.1/quartus/common/tcl/internal/nativelink/qnativesim.tcl"

qpf_file="Common_interface"
qsf_file="Common_interface"

#time $quartus_map --read_settings_files=on --write_settings_files=off $qpf_file -c $qsf_file --analysis_and_elaboration

$quartus_sh -t $qnativesim --rtl_sim $qpf_file $qsf_file

$modelsim_script_gen --nativelink-mode --output-directory=simulation --spd=../Interfaces/gbt_bank/altera_a10/gbt_rx/rx_dpram/alt_a10_rx_dpram/alt_a10_rx_dpram.spd --spd=../Interfaces/gbt_bank/altera_a10/gbt_tx/tx_dpram/alt_a10_tx_dpram/alt_a10_tx_dpram.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_rx/alt_a10_gx_reset_rx/alt_a10_gx_reset_rx.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_tx/alt_a10_gx_reset_tx/alt_a10_gx_reset_tx.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x1/alt_a10_gx_std_x1/alt_a10_gx_std_x1.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x2/alt_a10_gx_std_x2/alt_a10_gx_std_x2.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x3/alt_a10_gx_std_x3/alt_a10_gx_std_x3.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x4/alt_a10_gx_std_x4/alt_a10_gx_std_x4.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x5/alt_a10_gx_std_x5/alt_a10_gx_std_x5.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x6/alt_a10_gx_std_x6/alt_a10_gx_std_x6.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x1/alt_a10_gx_latopt_x1/alt_a10_gx_latopt_x1.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x2/alt_a10_gx_latopt_x2/alt_a10_gx_latopt_x2.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x3/alt_a10_gx_latopt_x3/alt_a10_gx_latopt_x3.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x4/alt_a10_gx_latopt_x4/alt_a10_gx_latopt_x4.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x5/alt_a10_gx_latopt_x5/alt_a10_gx_latopt_x5.spd --spd=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x6/alt_a10_gx_latopt_x6/alt_a10_gx_latopt_x6.spd --spd=./ip/counter_128/counter_128.spd --spd=./ip/gbt_x1_top/gbt_x1_top.spd --spd=./ip/gbt_x2_top/gbt_x2_top.spd --spd=./ip/gbt_x3_top/gbt_x3_top.spd --spd=./ip/gbt_x4_top/gbt_x4_top.spd --spd=./ip/gbt_x5_top/gbt_x5_top.spd --spd=./ip/gbt_x6_top/gbt_x6_top.spd
