# Created by Jubin Mitra on 9th MArch 2016
qsys_gen="/opt/altera/15.1/quartus/sopc_builder/bin/qsys-generate"

$qsys_gen ../Interfaces/PCIe_x16_mm_DMA_Q15.0/ep_g3x8_avmm256.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/PCIe_x16_mm_DMA_Q15.0/ep_g3x8_avmm256 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/sfp/ttk.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/sfp/ttk --family="Arria 10" --part=10AX115S4F45I3SGE2

$qsys_gen ../Interfaces/gbt_bank/altera_a10/gbt_rx/rx_dpram/alt_a10_rx_dpram.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/gbt_rx/rx_dpram/alt_a10_rx_dpram --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/gbt_tx/tx_dpram/alt_a10_tx_dpram.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/gbt_tx/tx_dpram/alt_a10_tx_dpram --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_rx/alt_a10_gx_reset_rx.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_rx/alt_a10_gx_reset_rx --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_tx/alt_a10_gx_reset_tx.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_reset_tx/alt_a10_gx_reset_tx --family="Arria 10" --part=10AX115S4F45I3SGE2

$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x1/alt_a10_gx_latopt_x1.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x1/alt_a10_gx_latopt_x1 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x2/alt_a10_gx_latopt_x2.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x2/alt_a10_gx_latopt_x2 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x3/alt_a10_gx_latopt_x3.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x3/alt_a10_gx_latopt_x3 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x4/alt_a10_gx_latopt_x4.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x4/alt_a10_gx_latopt_x4 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x5/alt_a10_gx_latopt_x5.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x5/alt_a10_gx_latopt_x5 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x6/alt_a10_gx_latopt_x6.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_latopt_x6/alt_a10_gx_latopt_x6 --family="Arria 10" --part=10AX115S4F45I3SGE2

$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x1/alt_a10_gx_std_x1.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x1/alt_a10_gx_std_x1 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x2/alt_a10_gx_std_x2.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x2/alt_a10_gx_std_x2 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x3/alt_a10_gx_std_x3.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x3/alt_a10_gx_std_x3 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x4/alt_a10_gx_std_x4.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x4/alt_a10_gx_std_x4 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x5/alt_a10_gx_std_x5.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x5/alt_a10_gx_std_x5 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x6/alt_a10_gx_std_x6.qsys --synthesis=VHDL --simulation=VHDL --output-directory=../Interfaces/gbt_bank/altera_a10/mgt/gx_std_x6/alt_a10_gx_std_x6 --family="Arria 10" --part=10AX115S4F45I3SGE2

$qsys_gen ./ip/counter_128.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/counter_128 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/gbt_x1_top.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/gbt_x1_top --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/gbt_x2_top.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/gbt_x2_top --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/gbt_x3_top.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/gbt_x3_top --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/gbt_x4_top.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/gbt_x4_top --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/gbt_x5_top.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/gbt_x5_top --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/gbt_x6_top.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/gbt_x6_top --family="Arria 10" --part=10AX115S4F45I3SGE2

$qsys_gen ./ip/ref_design_x1.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/ref_design_x1 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/ref_design_x2.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/ref_design_x2 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/ref_design_x3.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/ref_design_x3 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/ref_design_x4.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/ref_design_x4 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/ref_design_x5.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/ref_design_x5 --family="Arria 10" --part=10AX115S4F45I3SGE2
$qsys_gen ./ip/ref_design_x6.qsys --synthesis=VHDL --simulation=VHDL --output-directory=./ip/ref_design_x6 --family="Arria 10" --part=10AX115S4F45I3SGE2


